/**
 * Created by Mithun on 12/15/16.
 */
angular.module('docketApp.controllers',[]).controller('UserLogin',function($scope,$stateParams,UserLogin,$window,$store){
	$scope.user=new UserLogin();
	$scope.user.key = "docketpay";
	$scope.error = "";
	$scope.promise = null;
	$scope.validation =  function() {
		var x = $scope.user.email;
		if(x === undefined){
			x="";
		}
		var atpos = x.indexOf("@");
		var dotpos = x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
			$scope.error = "Enter Valid Email"
		}else{
			$scope.login();
		}
	}
	$scope.login =function(){
		$scope.promise =  UserLogin.get({email:$scope.user.email,password:$scope.user.password,key:$scope.user.key});
		$scope.promise.$promise.then(function(data) {
			$scope.error = data.message; 
			if(data.status == "OK"){
				$store.set("id",data.result.id);
				$store.set("name",data.result.name);
				$store.set("email",data.result.email);
				$store.set("mobile",data.result.mobile);
				$store.set("statesId",data.result.states);
				var landingUrl = "http://" + $window.location.host + "/dashboard/#/allproperty";
				$window.location.href = landingUrl;
			}else{
				$scope.error = data.message; 
			}
		}, function(error) {
			$scope.error = error.data.message;
		});
	}

}).controller('Activation',function($scope,$stateParams, $location, $store, UserOTP){
	$scope.key = "docketpay";
	$scope.code_type = $store.get("code_type");
	$scope.code = $store.get("code");

}).controller('Reset',function($scope,$stateParams, $location, $store, UserReset){
	$scope.key = "docketpay";
	$scope.code_type = $store.get("code_type");
	$scope.code = $store.get("code");
})


.controller('PaymentResult',function($scope,$stateParams, $location, $store, Receipt){
	$scope.key = "docketpay";
	$scope.user_id =  $store.get("id");
	$scope.invoice_no = $store.get("invoice_no").split('=')[1].split('#')[0];
	$scope.data = null;
	
	$scope.promise =  Receipt.get({user_id:$scope.user_id,invoice_no:$scope.invoice_no,key:$scope.key});
	$scope.promise.$promise.then(function(data) {
		$scope.data = data.data;
		$scope.error = data.message; 
		if(data.status == "OK"){
			$scope.error = data.message; 
		}else{
			$scope.error = data.message; 
		}
	}, function(error) {
		$scope.error = error.data.message;
	});

}).controller('bill',function($scope,$stateParams,Bill,$store){
	$scope.key = "docketpay";
	$scope.user_id =  $store.get("id");
	$scope.bills = null;
	$scope.promise =  Bill.get({usepror_id:$scope.user_id,invoice_no:$scope.invoice_no,key:$scope.key});
	$scope.promise.$promise.then(function(data) {
		$scope.bills = data.data.bills;
			$scope.error = data.message; 
			if(data.status == "OK"){
		}else{
			$scope.error = data.message; 
		}
	}, function(error) {
		$scope.error = error.data.message;
	});

}).controller('forgot',function($scope,$stateParams,UserForgot,$state){
	$scope.response = null;
	$scope.email = "";
	$scope.forgot=new UserForgot();
	$scope.forgot.key = "docketpay";
	//$scope.forgot.mobile =$scope.user.mobile
	$scope.error = "";
	$scope.validation =  function() {
		var x = $scope.email;
		if(x === undefined){
			x="";
		}
		var atpos = x.indexOf("@");
		var dotpos = x.lastIndexOf(".");
	
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
			$scope.error = "Enter Valid Email"
		}else{
			$scope.addForgot();
		}
	}
	$scope.addForgot = function(){
		$scope.forgot.email = $scope.email;
		UserForgot.save($scope.forgot, function(data){
			if(data.status == "OK"){
				$scope.error = data.message;
				$state.go('login');
			}else{
				$scope.error = data.message;
			}
		}, function(error) {
			  $scope.error = error.data.message;
		});
	}
})
.controller('dashboard',function($scope,$stateParams, b64DecodeEncode){
	// console.log("dashboard b64DecodeEncode.b64EncodeUnicode('json'):;" + b64DecodeEncode.b64EncodeUnicode('{ id: 2, name: "Mithun", email: "mithunmaddur@gmail.com", mobile: "9964999882", profile_img: "", gender: "", birthday: "", id_proof_url: "", pancard_url: "", referral_code: "Wav71bbf"  } '));
	// console.log("dashboard b64DecodeEncode b64EncodeUnicode('\n'):;" +b64DecodeEncode.b64EncodeUnicode('\n'));
	// console.log("dashboard b64DecodeEncode b64DecodeUnicode('json'):;" +b64DecodeEncode.b64DecodeUnicode('eyBpZDogMiwgbmFtZTogIk1pdGh1biIsIGVtYWlsOiAibWl0aHVubWFkZHVyQGdtYWlsLmNvbSIsIG1vYmlsZTogIjk5NjQ5OTk4ODIiLCBwcm9maWxlX2ltZzogIiIsIGdlbmRlcjogIiIsIGJpcnRoZGF5OiAiIiwgaWRfcHJvb2ZfdXJsOiAiIiwgcGFuY2FyZF91cmw6ICIiLCByZWZlcnJhbF9jb2RlOiAiV2F2NzFiYmYiICB9IA=='));
	// console.log("b64DecodeEncode  b64DecodeUnicode('Cg=='):;" +b64DecodeEncode.b64DecodeUnicode('Cg=='));
})
.controller('GetAllProperty',function($scope,$state,Property, $store,Pay, payService){
	$scope.property=new Property();
	$scope.property.key = "docketpay";
	$scope.property.user_id =  $store.get("id");
	$scope.error = "mesage no error";
	$scope.promise = null;
	$scope.promisePay = null;
	$scope.states = $store.get("statesId");
	$scope.status = true;
	$scope.propertyDetails = null;
	
	$scope.showPropertySociety = function(id,society) {
		$store.set("propertyId",id);
		$store.set("societyPropertyId",society);
		 $state.go('propertysociety');
	}
	$scope.showProperty = function(id) {
		angular.forEach( $scope.properties ,function(value,index){
			if(value.id == id){
				$scope.propertyDetails = value;
				$scope.status =false;
			}
		})
	}
	$scope.showBack = function() {
		$scope.status = true;
	}
	
	$scope.stateById = function(id) {
		$scope.value;
		angular.forEach( $scope.states ,function(value,index){
			if(value.id == id){
				$scope.value = value.name;
			}
		})
		return 	 $scope.value;
	}
	$scope.payTask = function(id) {
		$scope.promisePay =  Pay.get({user_id:$scope.property.user_id,invoice_no:id,key:$scope.property.key});
		$scope.promisePay.$promise.then(function(data) {
			if(data.status == "OK"){
				// $scope.payUrl = data.data.bill.url;
				// payService.addPay(data.data.bill.options);
				payService.addPay(data.data.bill);
				$state.go('payICCI');
			}else{
				 $scope.error = data.message; 
			}
		}, function(error) {
			$scope.error = error.data.message;
		});
	}

	$scope.promise =  Property.get({user_id:$scope.property.user_id,key:$scope.property.key});
	$scope.promise.$promise.then(function(data) {
		$scope.properties =data.data.property;
		if(data.status == "OK"){
			$scope.propertyDetails =data.data.property[0];
		}else{
			$scope.error = data.message; 
		}
	}, function(error) {
		$scope.error = error.data.message;
	});
})
.controller('home',function($scope,$stateParams,Query,$store){
	
	$scope.query=new Query();
	$scope.query.key = "docketpay";
	$scope.query.user_id = $store.get("id");
	//$scope.query.name = $store.get("name");
	//$scope.query.email = $store.get("email");
	//$scope.query.mobile = $store.get("mobile");
	$scope.error = "";
	$scope.addQuery=function(){
		Query.save($scope.query, function(data){
			if(data.status == "OK"){
				$scope.error =  "Successfully sent";
				$scope.query.name = "";
				$scope.query.email = "";
				$scope.query.mobile = "";
				$scope.query.query = "";
			} else{
				$scope.error = data.message;
			}
		}, function(error) {
			  $scope.error = error.data.message;
		});
	}
}).controller('profile',function($scope,$stateParams, $store, UserDetails, Profile,$state){
	$scope.details= new Profile();
	$scope.mobile= new Profile();
	$scope.password= new Profile();
	$scope.key = "docketpay";
	$scope.user_id =  $store.get("id");
	$scope.UserDetails = null; 
	$scope.upload_type ="profile";
	$scope.cancel =function(){
		$state.go('dashboard');
	}
	$scope.promise =  UserDetails.get({user_id:$scope.user_id,key:$scope.key});
	$scope.promise.$promise.then(function(data) {
		$scope.error = data.message; 
		if(data.status == "OK"){
			$scope.UserDetails =  data.data;
		}else{
			$scope.error = data.message; 
		}
	}, function(error) {
		$scope.error = error.data.message;
	});
	$scope.updateMobile =function(){
		$scope.mobile.new_mobile  = $scope.UserDetails.new_mobile ;
		$scope.mobile.user_id = $scope.user_id;
		$scope.mobile.key = $scope.key ;
		$scope.mobile.type = "Mobile";
		Profile.update( $scope.mobile, function(data){
			if(data.status == "OK"){
				$scope.error = data.message;
				$state.go('dashboard');
			} else{
				$scope.error = data.message;
			}
		}, function(error) {
			  $scope.error = error.data.message;
		});
	}
	$scope.updatePassword =function(){
		$scope.password.new_password  = $scope.UserDetails.new_password ;
		$scope.password.old_password  = $scope.UserDetails.old_password ;
		$scope.password.user_id = $scope.user_id;
		$scope.password.key = $scope.key ;
		$scope.password.type = "Password";
		Profile.update( $scope.password, function(data){
			if(data.status == "OK"){
				$scope.error = data.message;
				$state.go('dashboard');
			} else{
				$scope.error = data.message;
			}
		}, function(error) {
			$scope.error = error.data.message;
		});
	}
	$scope.updateUser = function() {
		$scope.details.name = $scope.UserDetails.name;
		$scope.details.birthday = $scope.UserDetails.birthday;
		$scope.details.gender = $scope.UserDetails.gender;
		$scope.details.city = $scope.UserDetails.city;
		$scope.details.profile_img = $scope.UserDetails.profile_img;
		$scope.details.id_proof_url = $scope.UserDetails.id_proof_url;
		$scope.details.user_id = $scope.user_id;
		$scope.details.key = $scope.key ;
		$scope.details.type = "Details";
		Profile.update( $scope.details, function(data){
			if(data.status == "INVALID_REQUEST"){
				$scope.error = data.message;
			}else if(data.status == "OK"){
				$scope.error = data.message;
				$state.go('dashboard');
			} else{
				$scope.error = data.message;
			}
		}, function(error) {
			$scope.error = error.data.message;
		});
	}
	$scope.sizeLimit      = 5292880; // 5MB in Bytes
	$scope.uploadProgress = 0;
	$scope.creds          = {};
	$scope.creds.access_key ="AKIAJFCCMRGDEU5HE5YA";
	$scope.creds.secret_key ="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";
	$scope.creds.bucket ="docketpay-test";
	
	$scope.date = null;
	$scope.upload = function() {
		$scope.date =  new Date();
		AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
		AWS.config.region = 'us-east-1';
		var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
		if($scope.file) {
			// Perform File Size Check First
			var fileSize = Math.round(parseInt($scope.file.size));
			if (fileSize > $scope.sizeLimit) {
				toastr.error('Sorry, your attachment is too big. <br/> Maximum '  + $scope.fileSizeLabel() + ' file attachment allowed','File Too Large');
				return false;
			}
			// Prepend Unique String To Prevent Overwrites
			var uniqueFileName = $scope.date.getTime() + '-' + $scope.file.name;
			var params = { Key: uniqueFileName, ContentType: $scope.file.type, Body: $scope.file, ServerSideEncryption: 'AES256' };

			bucket.putObject(params, function(err, data) {
				if(err) {
					toastr.error(err.message,err.code);
					return false;
				}
				else {
					// Upload Successfully Finished
					toastr.success('File Uploaded Successfully', 'Done');
					if($scope.upload_type == "profile"){
						$scope.UserDetails.profile_img=  "https://" +  $scope.creds.bucket + ".s3.amazonaws.com/" + uniqueFileName;
					}else{
						$scope.UserDetails.id_proof_url=  "https://" +  $scope.creds.bucket + ".s3.amazonaws.com/" + uniqueFileName;				
					}
				// Reset The Progress Bar
				setTimeout(function() {
				  $scope.uploadProgress = 0;
				  $scope.$digest();
				}, 4000);
			  }
			})
			.on('httpUploadProgress',function(progress) {
				$scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
				$scope.$digest();
			});
		}else {
			// No File Selected
			toastr.error('Please select a file to upload');
	  	}
	}

	$scope.fileSizeLabel = function() {
		// Convert Bytes To MB
		return Math.round($scope.sizeLimit / 1024 / 1024) + 'MB';
	  };
}).controller('queries',function($scope,$stateParams,Queries, $store){
	$scope.key = "docketpay";
	$scope.user_id =  $store.get("id");
	$scope.queries = null; 
	$scope.promise =  Queries.get({user_id:$scope.user_id,key:$scope.key});
	 $scope.promise.$promise.then(function(data) {
		 $scope.data = data.data;
		 // console.log(data);
		 $scope.error = data.message; 
		 // console.log( $scope.error );
		 if(data.status == "OK"){
			$scope.queries =  data.data.queries;
			
		 }else{
			$scope.error = data.message; 
		 }
		
		}, function(error) {
				// console.log("ERORR");
				// console.log(error);
			  $scope.error = error.data.message;
			  
		
		});

}).controller('invitations',function($scope,$stateParams,Invitations, $store){
	$scope.key = "docketpay";
	$scope.user_id =  $store.get("id");
	$scope.invitations = null; 
	$scope.promise =  Invitations.get({user_id:$scope.user_id,key:$scope.key});
	 $scope.promise.$promise.then(function(data) {
		 $scope.data = data.data;
		 // console.log( data.data.invites);
		 $scope.error = data.message; 
		 // console.log( $scope.error );
		 if(data.status == "OK"){
			$scope.invitations =  data.data.invites;
			
		 }else{
			$scope.error = data.message; 
		 }
		
		}, function(error) {
				// console.log("ERORR");
				// console.log(error);
			  $scope.error = error.data.message;
			  
		
		});

}).controller('propertysociety',function($scope,$stateParams,PropertySociety, SocietyQuery,$store,$state){
	$scope.propertySociety=new PropertySociety();
	$scope.propertySociety.user_id = $store.get("id");
	$scope.query = "";
	$scope.societyPropertyId ="";
	// console.log("dfdfdf societysocietysocietyPropertyId"+ $store.get("societyPropertyId"));
	if(($store.get("societyPropertyId")!== undefined) && ($store.get("societyPropertyId") !== null)){
		$scope.societyPropertyId = $store.get("societyPropertyId");
	}
	
	$scope.propertySociety.key = "docketpay";
	$scope.error = "mesage no error";
	 
	$scope.societyQueryRes ="";
	 $scope.addPropertySociety=function(){
		 if(! $scope.query ) {
			$state.go('allproperty');
		}else{
			// console.log($scope.query);
			
			 $scope.propertySociety.society_id  = parseInt($scope.query[0]);
			 $scope.propertySociety.property_id   = $store.get("propertyId");
		 PropertySociety.save($scope.propertySociety, function(data){
			 $store.remove("propertyId");
			 $store.remove("societyPropertyId");
			 // console.log("SUCCESS");
			 // console.log(data);
			  // console.log(data.status );
			  // console.log(data.message);
			  if(data.status == "INVALID_REQUEST"){
				 $scope.error = data.message;
				  
			  }else if(data.status == "OK"){
				  $scope.error = data.message;
				  $state.go('allproperty');
				
			  } else{
				  $scope.error = data.message;
			  }
			 
			}, function(error) {
			  $scope.error = error.data.message;
		});
			
		}
		
			 
			
	}
	
	

		$scope.promise =  SocietyQuery.get({user_id: $store.get("id"),query:"", key:"docketpay"});
		$scope.promise.$promise.then(function(data) {
		
		  $scope.error = data.message; 
		 // console.log( $scope.error );
		 if(data.status == "OK"){
			$scope.societyQueryRes =  data.data.societies;
			 // console.log( "$scope.societyQueryRes ");
			  // console.log( $scope.societyQueryRes );
		 }else{
			$scope.error = data.message; 
		 }
		
		
		}, function(error) {
				// console.log("ERORR");
				// console.log(error);
			  $scope.error = error.data.message;
			  
		
		});
	

}).controller('invite',function($scope,$stateParams,Invite, $store,$state){
	$scope.invite=new Invite();
	$scope.invite.user_id = $store.get("id");
	$scope.invite.key = "docketpay";
	$scope.error = "mesage no error";
	$scope.cancel =function(){
		$state.go('dashboard');
	}
	 $scope.addInvite=function(){

		 Invite.save($scope.invite, function(data){
			 // console.log("SUCCESS");
			 // console.log(data);
			  // console.log(data.status );
			  // console.log(data.message);
			  if(data.status == "INVALID_REQUEST"){
				 $scope.error = data.message;
				  
			  }else if(data.status == "OK"){
				  $scope.error = data.message;
				  $state.go('invitations');
				
			  } else{
				  $scope.error = data.message;
			  }
			 
			}, function(error) {
			  $scope.error = error.data.message;
		});
	}
	


}).controller('society',function($scope,$stateParams,Society, $store, $state){
	$scope.society=new Society();
	$scope.society.user_id = $store.get("id");
	$scope.society.key = "docketpay";
	$scope.error = "mesage no error";
	$scope.cancel =function(){
			$state.go('dashboard');
		}
	 $scope.addSociety=function(){

		 Society.save($scope.society, function(data){
			 // console.log("SUCCESS");
			 // console.log(data);
			  // console.log(data.status );
			  // console.log(data.message);
			  if(data.status == "INVALID_REQUEST"){
				 $scope.error = data.message;
				  
			  }else if(data.status == "OK"){
				  $scope.error = data.message;
				  $state.go('dashboard');
				
			  } else{
				  $scope.error = data.message;
			  }
			 
			}, function(error) {
			  $scope.error = error.data.message;
		});
	}

}).controller('enquiry',function($scope,$stateParams, Query,  $store,$state){
	$scope.query=new Query();
	$scope.query.key = "docketpay";
	
	$scope.query.user_id = $store.get("id");
	$scope.query.name = $store.get("name");
	$scope.query.email = $store.get("email");
	$scope.query.mobile = $store.get("mobile");
	$scope.error = "mesage no error";
	$scope.cancel =function(){
		$state.go('dashboard');
	}
	 $scope.addQuery=function(){

		 Query.save($scope.query, function(data){
			 // console.log("SUCCESS");
			 // console.log(data);
			  // console.log(data.status );
			  // console.log(data.message);
			  if(data.status == "INVALID_REQUEST"){
				 $scope.error = data.message;
				  
			  }else if(data.status == "OK"){
				  $scope.error = data.message;
				  $state.go('queries');
				
			  } else{
				  $scope.error = data.message;
			  }
			 
			}, function(error) {
			  $scope.error = error.data.message;
		});
	}

}).controller('notification',function($scope,$stateParams,Notification, $store){
	$scope.key = "docketpay";
	$scope.user_id =  $store.get("id");
	$scope.notifications = null; 
	$scope.promise =  Notification.get({user_id:$scope.user_id,key:$scope.key});
	 $scope.promise.$promise.then(function(data) {
		 $scope.data = data.data;
		 // console.log(data);
		 $scope.error = data.message; 
		 // console.log( $scope.error );
		 if(data.status == "OK"){
			$scope.notifications =  data.result.notification;
			
			
		 }else{
			$scope.error = data.message; 
		 }
		
		}, function(error) {
				// console.log("ERORR");
				// console.log(error);
			  $scope.error = error.data.message;
		});
}).controller('PayICCI',function($scope,$stateParams, $sce, $store, payService){
	$scope.payment = payService.getPay();
	$scope.payment_url = $sce.trustAsResourceUrl($scope.payment.url);
	$scope.key = "docketpay";
	$scope.user_id =  $store.get("id");
})
.controller('UserController',function($scope,$state,User){
	$scope.user=new User();
	$scope.user.key = "docketpay";

	$scope.error = "";
	$scope.validation =  function() {  	  
		var x = $scope.user.email;
		if(x === undefined){
			x="";
		}
		var atpos = x.indexOf("@");
		var dotpos = x.lastIndexOf(".");
		var y = $scope.user.mobile;
		if(y === undefined){
			y="";
		}
		if(y.length  < 10){
			$scope.error = "Enter Valid Mobile"
		}else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
			
			$scope.error = "Enter Valid Email"
			
		}else{
			 $scope.addUser();
		}
	}
	$scope.addUser=function(){
		User.save($scope.user, function(data){
			if(data.status == "INVALID_REQUEST"){
				$scope.error = data.message;
			}else if(data.status == "OK"){
				$scope.error = data.message;
				$state.go('login');
			}else{
				$scope.error = data.message;
			}
			},function(error) {
				$scope.error = error.data.message;
		});
	}
}).controller('Property',function($scope,$state,Property,$store){
	$scope.property =new Property();
	$scope.property.is_owner = 'true';
	$scope.property.society_available = 'false';
	$scope.property.key = "docketpay";
	$scope.property.user_id = $store.get("id");
	$scope.states = $store.get("statesId");
	$scope.error = "mesage no error";
	$scope.isOwnerChanged = function(value) {
		if(value == "true"){
			$scope.property.tenant_name ="";
			$scope.property.tenant_email = "";
			$scope.property.tenant_mobile = "";
			$scope.property.owner_name = $store.get("name");
			$scope.property.owner_email = $store.get("email");
			$scope.property.owner_mobile = $store.get("mobile");
			// $scope.property.society_available = function(value){
	
		   	//  var valueToEdit = value;
	
			//  $dialog.dialog(angular.extend(dialogOptions, {resolve: {item: angular.copy(valueToEdit)}}))
			//  .open()
			//  .then(function(result) {
			//  if(result) {
			//  	angular.copy(result, valueToEdit);                
			//  }
			//     valueToEdit = undefined;
			// 	});
			// 	};
			// 	}
		}else{
			$scope.property.owner_name = "";
			$scope.property.owner_email = "";
			$scope.property.owner_mobile = "";
			$scope.property.tenant_name = $store.get("name");
			$scope.property.tenant_email = $store.get("email");
			$scope.property.tenant_mobile = $store.get("mobile");
		}
	}
	$scope.isOwnerChanged($scope.property.is_owner);
	$scope.addProperty=function(){
		Property.save($scope.property, function(data){
			if(data.status == "OK"){
				$scope.error = data.message;
				$state.go('allproperty');
			}else{
				$scope.error = data.message;
			}
			},function(error) {
				$scope.error = error.data.message;
		});
	}

	$scope.sizeLimit      = 5292880; // 5MB in Bytes
	$scope.uploadProgress = 0;
	$scope.creds          = {};
	$scope.creds.access_key ="AKIAJFCCMRGDEU5HE5YA";
	$scope.creds.secret_key ="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";
	$scope.creds.bucket ="docketpay-test";
 	
 	$scope.date = new Date();
  	$scope.upload = function() {
		AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
		AWS.config.region = 'us-east-1';
		var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
		
		if($scope.file) {
			// Perform File Size Check First
			var fileSize = Math.round(parseInt($scope.file.size));
			if (fileSize > $scope.sizeLimit) {
				toastr.error('Sorry, your attachment is too big. <br/> Maximum '  + $scope.fileSizeLabel() + ' file attachment allowed','File Too Large');
				return false;
			}
			// Prepend Unique String To Prevent Overwrites
			var uniqueFileName = $scope.date.getTime() + '-' + $scope.file.name;
			var params = { Key: uniqueFileName, ContentType: $scope.file.type, Body: $scope.file, ServerSideEncryption: 'AES256' };
			bucket.putObject(params, function(err, data) {
			if(err) {
				toastr.error(err.message,err.code);
				return false;
			}else{
				// Upload Successfully Finished
				toastr.success('File Uploaded Successfully', 'Done');
				$scope.property.rent_agreement_url =  "https://" +  $scope.creds.bucket + ".s3.amazonaws.com/" + uniqueFileName;
				// Reset The Progress Bar
				setTimeout(function() {
					$scope.uploadProgress = 0;
					$scope.$digest();
				}, 4000);
			}
		}).on('httpUploadProgress',function(progress) {
			$scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
			$scope.$digest();
		});
		}else {
			// No File Selected
			toastr.error('Please select a file to upload');
		}
	}
	$scope.fileSizeLabel = function() {
		// Convert Bytes To MB
		return Math.round($scope.sizeLimit / 1024 / 1024) + 'MB';
	};
  
 
 
}).controller('Tenant', function($scope){
	$scope.sizeLimit      = 10585760; // 10MB in Bytes
	$scope.uploadProgress = 0;
	$scope.creds          = {};
	$scope.creds.access_key ="AKIAJFCCMRGDEU5HE5YA";
	$scope.creds.secret_key ="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";
	$scope.creds.bucket ="docketpay-test";
	$scope.bucketUrl;
	$scope.date = new Date();
	$scope.upload = function() {
		AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
		AWS.config.region = 'us-east-1';
		var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
		
		if($scope.file) {
			// Perform File Size Check First
			var fileSize = Math.round(parseInt($scope.file.size));
			if (fileSize > $scope.sizeLimit) {
			  toastr.error('Sorry, your attachment is too big. <br/> Maximum '  + $scope.fileSizeLabel() + ' file attachment allowed','File Too Large');
			  return false;
			}
			// Prepend Unique String To Prevent Overwrites
			var uniqueFileName = $scope.date.getTime() + '-' + $scope.file.name;
			var params = { Key: uniqueFileName, ContentType: $scope.file.type, Body: $scope.file, ServerSideEncryption: 'AES256' };
			bucket.putObject(params, function(err, data) {
				if(err) {
					toastr.error(err.message,err.code);
					return false;
				}
				else {
					// Upload Successfully Finished
					toastr.success('File Uploaded Successfully', 'Done');
					$scope.bucketUrl =  "https://" +  $scope.creds.bucket + ".s3.amazonaws.com/" + uniqueFileName;
					// Reset The Progress Bar
					setTimeout(function() {
						$scope.uploadProgress = 0;
						$scope.$digest();
					}, 4000);
				}
			})
			.on('httpUploadProgress',function(progress) {
				$scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
				$scope.$digest();
			});
		}else {
			// No File Selected
			toastr.error('Please select a file to upload');
		}
	}

	$scope.fileSizeLabel = function() {
		// Convert Bytes To MB
		return Math.round($scope.sizeLimit / 1024 / 1024) + 'MB';
	};
}).controller('AdminController',function($scope,$state,User){

});