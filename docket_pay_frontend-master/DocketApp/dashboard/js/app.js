/**
 * Created by Mithun on 12/15/16.
 */
angular.module('docketApp',['ui.router','ngMessages','ngResource','cgBusy','docketApp.directives','docketApp.controllers','docketApp.services']);
angular.module('docketApp').config(function($stateProvider,$httpProvider){
	$stateProvider.state('home',{
		url:'/home',
		templateUrl:'/dashboard/partials/homepage.html',
		controller:'home'
	})

	// User Routes
	.state('user',{
		url:'/user',
		templateUrl:'dashboard/partials/user-add.html',
		controller:'UserController'
	})
	.state('login',{
		url:'/login',
		templateUrl:'dashboard/partials/login.html',
		controller:'UserLogin'
	})
	.state('forgot',{
		url:'/forgot',
		templateUrl:'dashboard/partials/forgot.html',
		controller:'forgot'
	})
	.state('profile',{
		url:'/profile',
		templateUrl:'adminPartials/profile.html',
		controller:'profile'    
	})
	
	// Dashboard
	.state('dashboard',{
		url:'/dashboard',
		templateUrl:'adminPartials/dashboard.html',
		controller:'GetAllProperty' 
	})

	// Query Route
	.state('queries',{
		url:'/queries',
		templateUrl:'adminPartials/enqueryList.html',
		controller:'queries'
	})
	.state('enquiry',{
		url:'/enquiry',
		templateUrl:'adminPartials/enquery.html',
		controller:'enquiry'        
	})

	// Invitation Routes
	.state('invitations',{
		url:'/invitations',
		templateUrl:'adminPartials/inviteList.html',
		controller:'invitations'
	})
	.state('invite',{
		url:'/invite',
		templateUrl:'adminPartials/invite.html',
		controller:'invite'
	})

	// SOciety routes
	.state('society',{
		url:'/society',
		templateUrl:'adminPartials/society.html',
		controller:'society'
	})
	.state('propertysociety',{
		url:'/propertysociety',
		templateUrl:'adminPartials/propertysociety.html',
		controller:'propertysociety'
	})

	//  Property Routes
	.state('property',{
		url:'/property',
		templateUrl:'adminPartials/addproperty.html',
		controller:'Property'
	})
	.state('tenant',{
		url:'/tenant',
		templateUrl:'adminPartials/tenant.html',
		controller:'Tenant'
	})
	.state('allproperty',{
		url:'/allproperty',
		templateUrl:'adminPartials/propertyall.html',
		controller:'GetAllProperty'
	}).state('testui',{
		url:'/testui',
		templateUrl:'adminPartials/addproperty.html',
		controller:'Tenant'
		
	})

	// Payment routes
	.state('payICCI',{
		url:'/payICCI',
		templateUrl:'adminPartials/payicci.html',
		controller:'PayICCI'
	}).state('bill',{
		url:'/bill',
		templateUrl:'adminPartials/bill.html',
		controller:'bill'   
	})
	.state('paymentSuccess',{
		url:'/payment/success',
		templateUrl:'adminPartials/invoice.html',
		controller:'PaymentResult'
	})
	.state('paymentFailed',{
		url:'/payment/fail',
		templateUrl:'adminPartials/paymentfail.html',
		controller:'PaymentResult'
	})
	.state('paymentError',{
		url:'/payment/error',
		templateUrl:'adminPartials/paymenterror.html',
		controller:'PaymentResult'
	})

	// Notification Routes
	.state('notification',{
		url:'/notification',
		templateUrl:'adminPartials/notification.html',
		controller:'notification'           
	})
	// Static routes
	.state('terms',{
		url:'/terms',
		templateUrl:'dashboard/partials/termscondition.html',
		controller:'AdminController'
	})
	.state('privacy',{
		url:'/privacy',
		templateUrl:'dashboard/partials/privacy.html',
		controller:'AdminController'
	})
	.state('disclaimerprivacy',{
		url:'/disclaimerprivacy',
		templateUrl:'dashboard/partials/disclaimerprivacy.html',
		controller:'AdminController'
	})
	.state('refundpolicy',{
		url:'/refundpolicy',
		templateUrl:'dashboard/partials/refundpolicy.html',
		controller:'AdminController'
	})
	.state('reset',{
		url:'/reset',
		templateUrl:'dashboard/partials/reset.html',
		controller:'Reset'
	})
	.state('activation',{
		url:'/activation',
		templateUrl:'dashboard/partials/activation.html',
		controller:'Activation'
	});
}).run(function($rootScope,$state,$store,$location,$window,Notification){
	
	$rootScope.userName = $store.get("name");
	var absurl = $location.absUrl();
	var landingUrl = "http://" + $window.location.host + "/#/home";
	if (absurl.indexOf('code') > -1) { 
		$store.set("code",$location.absUrl().split(':')[1]);
		if (absurl.indexOf('activation') > -1) { 
			$store.set("code_type","activation");
			landingUrl = "http://" + $window.location.host + "/#/activation";
		}else if (absurl.indexOf('reset') > -1) { 
			$store.set("code_type","reset");
			landingUrl = "http://" + $window.location.host + "/#/reset";
		}
		$window.location.href = landingUrl;
	}else if(absurl.indexOf('/dashboard/#') > -1 &&   ($store.get("id") == "" ||  $store.get("id") == null)){
			$window.location.href = landingUrl;
	}else if (absurl.indexOf('invoice_no') > -1) { 
		$store.set("invoice_no",$location.absUrl().split('?')[1]);
		landingUrl = "http://" + $window.location.host + "/dashboard/#/paymentresult";
		$window.location.href = landingUrl;
	}else {
		$state.go('home');
	}
	
	if(!($store.get("id") == "" ||  $store.get("id") == null)){
		$rootScope.notifications = null; 
		$rootScope.promise =  Notification.get({user_id: $store.get("id"),key:"docketpay"});
		$rootScope.promise.$promise.then(function(data) {
		$rootScope.data = data.data;
		$rootScope.error = data.message; 
		if(data.status == "OK"){
			$rootScope.notifications =  data.result.notification;
			$rootScope.notificationsCount = function() {
				var count = 0;
				angular.forEach($rootScope.notifications, function(notification){
					count ++;
				});
				return count; 
			}
			
		}else{
			$rootScope.error = data.message; 
		}
	}, function(error) {
		$rootScope.error = error.data.message;
	});

	$rootScope.logout = function() {
		$store.remove("id");
		$store.remove("name");
		$store.remove("email");
		$store.remove("mobile");
		$store.remove("statesId");
		$window.location.href = landingUrl;
		}
	}
}); 