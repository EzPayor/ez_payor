/**
 *  Created by Mithun on 12/15/16.
 */

angular.module('docketApp.services',[])
.factory('User',function($resource,Environment){
    return $resource(Environment.getApiRoute()+'/user/signup/:id',{id:'@_id'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('UserDetails',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/details',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Profile',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/profile',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('UserLogin',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/signin',{email:'@_email',password:'@_password',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('UserOTP',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/verify/otp/',{code:'@_code',otp:'@_otp',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('UserReset',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/verify/rest/',{code:'@_code',otp:'@_otp',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('UserForgot',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/password/forgot',{email:'@_email',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Pay',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/bill/pay',{user_id:'@_user_id',invoice_no:'@_invoice_no',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Notification',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/notifications',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Query',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/query',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Queries',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/queries',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Property',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/property',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Invoice',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/bill/details',{invoice_no:'@_invoice_no',user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Verify',function($resource,Environment){
    return $resource(Environment.getApiRoute() +' /verify/authorize/',{type:'@_type',code:'@_code',invite_code:'@_invite_code',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Receipt',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/bill/receipt',{invoice_no:'@_invoice_no',user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Bill',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/bill',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Society',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/society',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('PropertySociety',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/property/society',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('SocietyQuery',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/society/search',{user_id:'@_user_id',query:'@_query',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Invite',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/invite',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.factory('Invitations',function($resource,Environment){
    return $resource(Environment.getApiRoute() +'/user/invitations',{user_id:'@_user_id',key:'@_key'},{
        update: {
            method: 'PUT'
        }
    });
})
.service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
})
.service('payService', function() {
  var payList = null;
  var addPay = function(newObj) {
      payList = newObj;
  };
  var getPay = function(){
      return payList;
  };
  return {
    addPay: addPay,
    getPay: getPay
  };
})
.factory("$store",function($parse){
  /**
	 * Global Vars
	 */
	var storage = (typeof window.localStorage === 'undefined') ? undefined : window.localStorage,
		supported = !(typeof storage == 'undefined' || typeof window.JSON == 'undefined');
	var privateMethods = {
		/** 
		 * Pass any type of a string from the localStorage to be parsed so it returns a usable version (like an Object)
		 * @param res - a string that will be parsed for type
		 * @returns {*} - whatever the real type of stored value was
		 */
		parseValue: function(res) {
			var val;
			try {
				val = JSON.parse(res);
				if (typeof val == 'undefined'){
					val = res;
				}
				if (val == 'true'){
					val = true;
				}
				if (val == 'false'){
					val = false;
				}
				if (parseFloat(val) == val && !angular.isObject(val) ){
					val = parseFloat(val);
				}
			} catch(e){
				val = res;
			}
			return val;
		}
	};
	var publicMethods = {
		/**
		 * Set - let's you set a new localStorage key pair set
		 * @param key - a string that will be used as the accessor for the pair
		 * @param value - the value of the localStorage item
		 * @returns {*} - will return whatever it is you've stored in the local storage
		 */
		set: function(key,value){
			if (!supported){
				try {
					$.cookie(key, value);
					return value;
				} catch(e){
					console.log('Local Storage not supported, make sure you have the $.cookie supported.');
				}
			}
			var saver = JSON.stringify(value);
			storage.setItem(key, saver);
			return privateMethods.parseValue(saver);
		},
		/**
		 * Get - let's you get the value of any pair you've stored
		 * @param key - the string that you set as accessor for the pair
		 * @returns {*} - Object,String,Float,Boolean depending on what you stored
		 */
		get: function(key){
			if (!supported){
				try {
					return privateMethods.parseValue($.cookie(key));
				} catch(e){
					return null;
				}
			}
			var item = storage.getItem(key);
			return privateMethods.parseValue(item);
		},
		/**
		 * Remove - let's you nuke a value from localStorage
		 * @param key - the accessor value
		 * @returns {boolean} - if everything went as planned
		 */
		remove: function(key) {
			if (!supported){
				try {
					$.cookie(key, null);
					return true;
				} catch(e){
					return false;
				}
			}
			storage.removeItem(key);
			return true;
		},
		/**
	         * Bind - let's you directly bind a localStorage value to a $scope variable
	         * @param $scope - the current scope you want the variable available in
	         * @param key - the name of the variable you are binding
	         * @param def - the default value (OPTIONAL)
	         * @returns {*} - returns whatever the stored value is
	         */
	        bind: function ($scope, key, def) {
	            def = def || '';
	            if (!publicMethods.get(key)) {
	                publicMethods.set(key, def);
	            }
	            $parse(key).assign($scope, publicMethods.get(key));
	            $scope.$watch(key, function (val) {
	                publicMethods.set(key, val);
	            }, true);
	            return publicMethods.get(key);
	        }
	};
	return publicMethods;
})
.filter('range', function() {
    return function(input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i=min; i<max; i++)
          input.push(i);
        return input;
    };
})
.factory("b64DecodeEncode", function() {                                                                                                                                                   
    return {                                                                                                                                                                                                              
       b64EncodeUnicode: function(str) {   
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
		}));
       },
       b64DecodeUnicode: function(str) {
        return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
       }
	   
	 }
    }).provider('Environment', function () {
    var environments = {
        dev: {
            root: 'http://localhost',
            port: 3000,
            api: '/api',
            version: 'v1'
        }
    };
    var selectedEnv = 'dev';
    var self = this;
    this.setEnvironments = function (envs) {
        if (!Object.keys(envs).length)
            throw new Error('At least one environment is required!');

        environments = envs;
    };
    this.setActive = function (env) {
        if (!environments[env])
            throw new Error('No such environment present: ' + env);

        selectedEnv = env;
        return self.getActive();
    };
    this.getEnvironment = function (env) {
        if (!env)
            throw new Error('No such environment present: ' + env);
        return environments[env];
    };
    this.getActive = function () {
        if (!selectedEnv)
            throw new Error('You must configure at least one environment');

        return environments[selectedEnv];
    };
    this.getApiRoute = function () {
        var active = self.getActive();
        return active.root + (active.port ? ':' + active.port : '') +
            active.api + (active.version ? '/' + active.version : '');
    };
    this.$get = [function () {
        return self;
    }];
});