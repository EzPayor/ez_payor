require 'test_helper'

class Users::Invites::StatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_invites_status = users_invites_statuses(:one)
  end

  test "should get index" do
    get users_invites_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_users_invites_status_url
    assert_response :success
  end

  test "should create users_invites_status" do
    assert_difference('Users::Invites::Status.count') do
      post users_invites_statuses_url, params: { users_invites_status: { name: @users_invites_status.name } }
    end

    assert_redirected_to users_invites_status_url(Users::Invites::Status.last)
  end

  test "should show users_invites_status" do
    get users_invites_status_url(@users_invites_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_invites_status_url(@users_invites_status)
    assert_response :success
  end

  test "should update users_invites_status" do
    patch users_invites_status_url(@users_invites_status), params: { users_invites_status: { name: @users_invites_status.name } }
    assert_redirected_to users_invites_status_url(@users_invites_status)
  end

  test "should destroy users_invites_status" do
    assert_difference('Users::Invites::Status.count', -1) do
      delete users_invites_status_url(@users_invites_status)
    end

    assert_redirected_to users_invites_statuses_url
  end
end
