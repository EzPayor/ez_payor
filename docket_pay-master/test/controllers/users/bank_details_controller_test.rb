require 'test_helper'

class Users::BankDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_bank_detail = users_bank_details(:one)
  end

  test "should get index" do
    get users_bank_details_url
    assert_response :success
  end

  test "should get new" do
    get new_users_bank_detail_url
    assert_response :success
  end

  test "should create users_bank_detail" do
    assert_difference('Users::BankDetail.count') do
      post users_bank_details_url, params: { users_bank_detail: { acc_number: @users_bank_detail.acc_number, acc_type: @users_bank_detail.acc_type, account_holder_name: @users_bank_detail.account_holder_name, account_id: @users_bank_detail.account_id, account_type: @users_bank_detail.account_type, bank_name: @users_bank_detail.bank_name, ifsc: @users_bank_detail.ifsc, user_id: @users_bank_detail.user_id } }
    end

    assert_redirected_to users_bank_detail_url(Users::BankDetail.last)
  end

  test "should show users_bank_detail" do
    get users_bank_detail_url(@users_bank_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_bank_detail_url(@users_bank_detail)
    assert_response :success
  end

  test "should update users_bank_detail" do
    patch users_bank_detail_url(@users_bank_detail), params: { users_bank_detail: { acc_number: @users_bank_detail.acc_number, acc_type: @users_bank_detail.acc_type, account_holder_name: @users_bank_detail.account_holder_name, account_id: @users_bank_detail.account_id, account_type: @users_bank_detail.account_type, bank_name: @users_bank_detail.bank_name, ifsc: @users_bank_detail.ifsc, user_id: @users_bank_detail.user_id } }
    assert_redirected_to users_bank_detail_url(@users_bank_detail)
  end

  test "should destroy users_bank_detail" do
    assert_difference('Users::BankDetail.count', -1) do
      delete users_bank_detail_url(@users_bank_detail)
    end

    assert_redirected_to users_bank_details_url
  end
end
