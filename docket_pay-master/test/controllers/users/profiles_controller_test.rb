require 'test_helper'

class Users::ProfilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_profile = users_profiles(:one)
  end

  test "should get index" do
    get users_profiles_url
    assert_response :success
  end

  test "should get new" do
    get new_users_profile_url
    assert_response :success
  end

  test "should create users_profile" do
    assert_difference('Users::Profile.count') do
      post users_profiles_url, params: { users_profile: { city: @users_profile.city, current_login: @users_profile.current_login, current_login_ip: @users_profile.current_login_ip, dob: @users_profile.dob, gender: @users_profile.gender, id_proof_url: @users_profile.id_proof_url, invite_code: @users_profile.invite_code, last_login: @users_profile.last_login, last_login_ip: @users_profile.last_login_ip, last_pwd: @users_profile.last_pwd, last_pwd_update: @users_profile.last_pwd_update, last_pwd_update_ip: @users_profile.last_pwd_update_ip, pancard_url: @users_profile.pancard_url, profile_img: @users_profile.profile_img, referral_code: @users_profile.referral_code, referred_by: @users_profile.referred_by, state_id: @users_profile.state_id } }
    end

    assert_redirected_to users_profile_url(Users::Profile.last)
  end

  test "should show users_profile" do
    get users_profile_url(@users_profile)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_profile_url(@users_profile)
    assert_response :success
  end

  test "should update users_profile" do
    patch users_profile_url(@users_profile), params: { users_profile: { city: @users_profile.city, current_login: @users_profile.current_login, current_login_ip: @users_profile.current_login_ip, dob: @users_profile.dob, gender: @users_profile.gender, id_proof_url: @users_profile.id_proof_url, invite_code: @users_profile.invite_code, last_login: @users_profile.last_login, last_login_ip: @users_profile.last_login_ip, last_pwd: @users_profile.last_pwd, last_pwd_update: @users_profile.last_pwd_update, last_pwd_update_ip: @users_profile.last_pwd_update_ip, pancard_url: @users_profile.pancard_url, profile_img: @users_profile.profile_img, referral_code: @users_profile.referral_code, referred_by: @users_profile.referred_by, state_id: @users_profile.state_id } }
    assert_redirected_to users_profile_url(@users_profile)
  end

  test "should destroy users_profile" do
    assert_difference('Users::Profile.count', -1) do
      delete users_profile_url(@users_profile)
    end

    assert_redirected_to users_profiles_url
  end
end
