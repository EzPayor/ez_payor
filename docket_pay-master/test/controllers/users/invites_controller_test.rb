require 'test_helper'

class Users::InvitesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_invite = users_invites(:one)
  end

  test "should get index" do
    get users_invites_url
    assert_response :success
  end

  test "should get new" do
    get new_users_invite_url
    assert_response :success
  end

  test "should create users_invite" do
    assert_difference('Users::Invite.count') do
      post users_invites_url, params: { users_invite: { email: @users_invite.email, invited_by: @users_invite.invited_by, mobile: @users_invite.mobile, name: @users_invite.name, referral_code: @users_invite.referral_code } }
    end

    assert_redirected_to users_invite_url(Users::Invite.last)
  end

  test "should show users_invite" do
    get users_invite_url(@users_invite)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_invite_url(@users_invite)
    assert_response :success
  end

  test "should update users_invite" do
    patch users_invite_url(@users_invite), params: { users_invite: { email: @users_invite.email, invited_by: @users_invite.invited_by, mobile: @users_invite.mobile, name: @users_invite.name, referral_code: @users_invite.referral_code } }
    assert_redirected_to users_invite_url(@users_invite)
  end

  test "should destroy users_invite" do
    assert_difference('Users::Invite.count', -1) do
      delete users_invite_url(@users_invite)
    end

    assert_redirected_to users_invites_url
  end
end
