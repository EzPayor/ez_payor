require 'test_helper'

class Users::QueriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_query = users_queries(:one)
  end

  test "should get index" do
    get users_queries_url
    assert_response :success
  end

  test "should get new" do
    get new_users_query_url
    assert_response :success
  end

  test "should create users_query" do
    assert_difference('Users::Query.count') do
      post users_queries_url, params: { users_query: { assigned_to: @users_query.assigned_to, name: @users_query.name, notes: @users_query.notes, query: @users_query.query, status_id: @users_query.status_id, subject: @users_query.subject, user_id: @users_query.user_id } }
    end

    assert_redirected_to users_query_url(Users::Query.last)
  end

  test "should show users_query" do
    get users_query_url(@users_query)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_query_url(@users_query)
    assert_response :success
  end

  test "should update users_query" do
    patch users_query_url(@users_query), params: { users_query: { assigned_to: @users_query.assigned_to, name: @users_query.name, notes: @users_query.notes, query: @users_query.query, status_id: @users_query.status_id, subject: @users_query.subject, user_id: @users_query.user_id } }
    assert_redirected_to users_query_url(@users_query)
  end

  test "should destroy users_query" do
    assert_difference('Users::Query.count', -1) do
      delete users_query_url(@users_query)
    end

    assert_redirected_to users_queries_url
  end
end
