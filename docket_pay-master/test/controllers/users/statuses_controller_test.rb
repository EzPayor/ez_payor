require 'test_helper'

class Users::StatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_status = users_statuses(:one)
  end

  test "should get index" do
    get users_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_users_status_url
    assert_response :success
  end

  test "should create users_status" do
    assert_difference('Users::Status.count') do
      post users_statuses_url, params: { users_status: { name: @users_status.name } }
    end

    assert_redirected_to users_status_url(Users::Status.last)
  end

  test "should show users_status" do
    get users_status_url(@users_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_status_url(@users_status)
    assert_response :success
  end

  test "should update users_status" do
    patch users_status_url(@users_status), params: { users_status: { name: @users_status.name } }
    assert_redirected_to users_status_url(@users_status)
  end

  test "should destroy users_status" do
    assert_difference('Users::Status.count', -1) do
      delete users_status_url(@users_status)
    end

    assert_redirected_to users_statuses_url
  end
end
