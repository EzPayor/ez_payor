require 'test_helper'

class Users::AuthenticationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_authentication = users_authentications(:one)
  end

  test "should get index" do
    get users_authentications_url
    assert_response :success
  end

  test "should get new" do
    get new_users_authentication_url
    assert_response :success
  end

  test "should create users_authentication" do
    assert_difference('Users::Authentication.count') do
      post users_authentications_url, params: { users_authentication: { authentication_type: @users_authentication.authentication_type, code: @users_authentication.code, status_id: @users_authentication.status_id, user_id: @users_authentication.user_id, validity: @users_authentication.validity } }
    end

    assert_redirected_to users_authentication_url(Users::Authentication.last)
  end

  test "should show users_authentication" do
    get users_authentication_url(@users_authentication)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_authentication_url(@users_authentication)
    assert_response :success
  end

  test "should update users_authentication" do
    patch users_authentication_url(@users_authentication), params: { users_authentication: { authentication_type: @users_authentication.authentication_type, code: @users_authentication.code, status_id: @users_authentication.status_id, user_id: @users_authentication.user_id, validity: @users_authentication.validity } }
    assert_redirected_to users_authentication_url(@users_authentication)
  end

  test "should destroy users_authentication" do
    assert_difference('Users::Authentication.count', -1) do
      delete users_authentication_url(@users_authentication)
    end

    assert_redirected_to users_authentications_url
  end
end
