require 'test_helper'

class EbProvidersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @eb_provider = eb_providers(:one)
  end

  test "should get index" do
    get eb_providers_url
    assert_response :success
  end

  test "should get new" do
    get new_eb_provider_url
    assert_response :success
  end

  test "should create eb_provider" do
    assert_difference('EbProvider.count') do
      post eb_providers_url, params: { eb_provider: { name: @eb_provider.name, state_id: @eb_provider.state_id, status_id: @eb_provider.status_id } }
    end

    assert_redirected_to eb_provider_url(EbProvider.last)
  end

  test "should show eb_provider" do
    get eb_provider_url(@eb_provider)
    assert_response :success
  end

  test "should get edit" do
    get edit_eb_provider_url(@eb_provider)
    assert_response :success
  end

  test "should update eb_provider" do
    patch eb_provider_url(@eb_provider), params: { eb_provider: { name: @eb_provider.name, state_id: @eb_provider.state_id, status_id: @eb_provider.status_id } }
    assert_redirected_to eb_provider_url(@eb_provider)
  end

  test "should destroy eb_provider" do
    assert_difference('EbProvider.count', -1) do
      delete eb_provider_url(@eb_provider)
    end

    assert_redirected_to eb_providers_url
  end
end
