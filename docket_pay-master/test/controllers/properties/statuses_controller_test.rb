require 'test_helper'

class Properties::StatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @properties_status = properties_statuses(:one)
  end

  test "should get index" do
    get properties_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_properties_status_url
    assert_response :success
  end

  test "should create properties_status" do
    assert_difference('Properties::Status.count') do
      post properties_statuses_url, params: { properties_status: { name: @properties_status.name } }
    end

    assert_redirected_to properties_status_url(Properties::Status.last)
  end

  test "should show properties_status" do
    get properties_status_url(@properties_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_properties_status_url(@properties_status)
    assert_response :success
  end

  test "should update properties_status" do
    patch properties_status_url(@properties_status), params: { properties_status: { name: @properties_status.name } }
    assert_redirected_to properties_status_url(@properties_status)
  end

  test "should destroy properties_status" do
    assert_difference('Properties::Status.count', -1) do
      delete properties_status_url(@properties_status)
    end

    assert_redirected_to properties_statuses_url
  end
end
