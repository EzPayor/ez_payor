require 'test_helper'

class Properties::TenantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @properties_tenant = properties_tenants(:one)
  end

  test "should get index" do
    get properties_tenants_url
    assert_response :success
  end

  test "should get new" do
    get new_properties_tenant_url
    assert_response :success
  end

  test "should create properties_tenant" do
    assert_difference('Properties::Tenant.count') do
      post properties_tenants_url, params: { properties_tenant: { amount: @properties_tenant.amount, end_date: @properties_tenant.end_date, invited_by: @properties_tenant.invited_by, property_id: @properties_tenant.property_id, removed_by: @properties_tenant.removed_by, staying_since: @properties_tenant.staying_since, user_id: @properties_tenant.user_id } }
    end

    assert_redirected_to properties_tenant_url(Properties::Tenant.last)
  end

  test "should show properties_tenant" do
    get properties_tenant_url(@properties_tenant)
    assert_response :success
  end

  test "should get edit" do
    get edit_properties_tenant_url(@properties_tenant)
    assert_response :success
  end

  test "should update properties_tenant" do
    patch properties_tenant_url(@properties_tenant), params: { properties_tenant: { amount: @properties_tenant.amount, end_date: @properties_tenant.end_date, invited_by: @properties_tenant.invited_by, property_id: @properties_tenant.property_id, removed_by: @properties_tenant.removed_by, staying_since: @properties_tenant.staying_since, user_id: @properties_tenant.user_id } }
    assert_redirected_to properties_tenant_url(@properties_tenant)
  end

  test "should destroy properties_tenant" do
    assert_difference('Properties::Tenant.count', -1) do
      delete properties_tenant_url(@properties_tenant)
    end

    assert_redirected_to properties_tenants_url
  end
end
