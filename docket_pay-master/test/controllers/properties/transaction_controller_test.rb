require 'test_helper'

class Properties::TransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bills_payment = bills_payments(:one)
  end

  test "should get index" do
    get bills_payments_url
    assert_response :success
  end

  test "should get new" do
    get new_bills_payment_url
    assert_response :success
  end

  test "should create bills_payment" do
    assert_difference('Properties::Transaction.count') do
      post bills_payments_url, params: { bills_payment: { amount: @bills_payment.amount, approval_code: @bills_payment.approval_code, bank_ref_num: @bills_payment.bank_ref_num, bankcode: @bills_payment.bankcode, banktxnid: @bills_payment.banktxnid, bname: @bills_payment.bname, cardnum: @bills_payment.cardnum, ccbin: @bills_payment.ccbin, ccbrand: @bills_payment.ccbrand, cccountry: @bills_payment.cccountry, currency: @bills_payment.currency, discount: @bills_payment.discount, email: @bills_payment.email, expmonth: @bills_payment.expmonth, expyear: @bills_payment.expyear, first_name: @bills_payment.first_name, last_name: @bills_payment.last_name, terminal_id: @bills_payment.terminal_id, ipgTransactionId: @bills_payment.ipgTransactionId, mode: @bills_payment.mode, name_on_card: @bills_payment.name_on_card, oid: @bills_payment.oid, order_id: @bills_payment.order_id, pg_type: @bills_payment.pg_type, phone: @bills_payment.phone, respmsg: @bills_payment.respmsg, responseHash_validationFrom: @bills_payment.responseHash_validationFrom, response_code: @bills_payment.response_code, response_code_3dsecure: @bills_payment.response_code_3dsecure, response_dump: @bills_payment.response_dump, response_hash_validation: @bills_payment.response_hash_validation, secret_hash: @bills_payment.secret_hash, status: @bills_payment.status, tdate: @bills_payment.tdate, timezone: @bills_payment.timezone, transactionNotificationURL: @bills_payment.transactionNotificationURL, txndate_processed: @bills_payment.txndate_processed, txndatetime: @bills_payment.txndatetime, txnid: @bills_payment.txnid, txntype: @bills_payment.txntype } }
    end

    assert_redirected_to bills_payment_url(Properties::Transaction.last)
  end

  test "should show bills_payment" do
    get bills_payment_url(@bills_payment)
    assert_response :success
  end

  test "should get edit" do
    get edit_bills_payment_url(@bills_payment)
    assert_response :success
  end

  test "should update bills_payment" do
    patch bills_payment_url(@bills_payment), params: { bills_payment: { amount: @bills_payment.amount, approval_code: @bills_payment.approval_code, bank_ref_num: @bills_payment.bank_ref_num, bankcode: @bills_payment.bankcode, banktxnid: @bills_payment.banktxnid, bname: @bills_payment.bname, cardnum: @bills_payment.cardnum, ccbin: @bills_payment.ccbin, ccbrand: @bills_payment.ccbrand, cccountry: @bills_payment.cccountry, currency: @bills_payment.currency, discount: @bills_payment.discount, email: @bills_payment.email, expmonth: @bills_payment.expmonth, expyear: @bills_payment.expyear, first_name: @bills_payment.first_name, last_name: @bills_payment.last_name, mid: @bills_payment.mid, mihpayid: @bills_payment.mihpayid, mode: @bills_payment.mode, name_on_card: @bills_payment.name_on_card, oid: @bills_payment.oid, order_id: @bills_payment.order_id, pg_type: @bills_payment.pg_type, phone: @bills_payment.phone, respmsg: @bills_payment.respmsg, responseHash_validationFrom: @bills_payment.responseHash_validationFrom, response_code: @bills_payment.response_code, response_code_3dsecure: @bills_payment.response_code_3dsecure, response_dump: @bills_payment.response_dump, response_hash_validation: @bills_payment.response_hash_validation, secret_hash: @bills_payment.secret_hash, status_id: @bills_payment.status_id, tdate: @bills_payment.tdate, timezone: @bills_payment.timezone, transactionNotificationURL: @bills_payment.transactionNotificationURL, txndate_processed: @bills_payment.txndate_processed, txndatetime: @bills_payment.txndatetime, txnid: @bills_payment.txnid, txntype: @bills_payment.txntype } }
    assert_redirected_to bills_payment_url(@bills_payment)
  end

  test "should destroy bills_payment" do
    assert_difference('Properties::Transaction.count', -1) do
      delete bills_payment_url(@bills_payment)
    end

    assert_redirected_to bills_payments_url
  end
end
