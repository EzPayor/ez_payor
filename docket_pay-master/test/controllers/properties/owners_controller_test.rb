require 'test_helper'

class Properties::OwnersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @properties_owner = properties_owners(:one)
  end

  test "should get index" do
    get properties_owners_url
    assert_response :success
  end

  test "should get new" do
    get new_properties_owner_url
    assert_response :success
  end

  test "should create properties_owner" do
    assert_difference('Properties::Owner.count') do
      post properties_owners_url, params: { properties_owner: { amount: @properties_owner.amount, property_id: @properties_owner.property_id, user_id: @properties_owner.user_id } }
    end

    assert_redirected_to properties_owner_url(Properties::Owner.last)
  end

  test "should show properties_owner" do
    get properties_owner_url(@properties_owner)
    assert_response :success
  end

  test "should get edit" do
    get edit_properties_owner_url(@properties_owner)
    assert_response :success
  end

  test "should update properties_owner" do
    patch properties_owner_url(@properties_owner), params: { properties_owner: { amount: @properties_owner.amount, property_id: @properties_owner.property_id, user_id: @properties_owner.user_id } }
    assert_redirected_to properties_owner_url(@properties_owner)
  end

  test "should destroy properties_owner" do
    assert_difference('Properties::Owner.count', -1) do
      delete properties_owner_url(@properties_owner)
    end

    assert_redirected_to properties_owners_url
  end
end
