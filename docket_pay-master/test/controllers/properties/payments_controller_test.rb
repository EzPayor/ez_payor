require 'test_helper'

class Properties::PaymentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @properties_payment = properties_payments(:one)
  end

  test "should get index" do
    get properties_payments_url
    assert_response :success
  end

  test "should get new" do
    get new_properties_payment_url
    assert_response :success
  end

  test "should create properties_payment" do
    assert_difference('Properties::Payment.count') do
      post properties_payments_url, params: { properties_payment: { amount: @properties_payment.amount, bank_detail_id: @properties_payment.bank_detail_id, bill_id: @properties_payment.bill_id, employee_id: @properties_payment.employee_id, property_id: @properties_payment.property_id, status_id: @properties_payment.status_id, type: @properties_payment.type } }
    end

    assert_redirected_to properties_payment_url(Properties::Payment.last)
  end

  test "should show properties_payment" do
    get properties_payment_url(@properties_payment)
    assert_response :success
  end

  test "should get edit" do
    get edit_properties_payment_url(@properties_payment)
    assert_response :success
  end

  test "should update properties_payment" do
    patch properties_payment_url(@properties_payment), params: { properties_payment: { amount: @properties_payment.amount, bank_detail_id: @properties_payment.bank_detail_id, bill_id: @properties_payment.bill_id, employee_id: @properties_payment.employee_id, property_id: @properties_payment.property_id, status_id: @properties_payment.status_id, type: @properties_payment.type } }
    assert_redirected_to properties_payment_url(@properties_payment)
  end

  test "should destroy properties_payment" do
    assert_difference('Properties::Payment.count', -1) do
      delete properties_payment_url(@properties_payment)
    end

    assert_redirected_to properties_payments_url
  end
end
