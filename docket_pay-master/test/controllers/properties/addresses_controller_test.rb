require 'test_helper'

class Properties::AddressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @properties_address = properties_addresses(:one)
  end

  test "should get index" do
    get properties_addresses_url
    assert_response :success
  end

  test "should get new" do
    get new_properties_address_url
    assert_response :success
  end

  test "should create properties_address" do
    assert_difference('Properties::Address.count') do
      post properties_addresses_url, params: { properties_address: { building: @properties_address.building, city: @properties_address.city, flat_no: @properties_address.flat_no, floor: @properties_address.floor, locality: @properties_address.locality, pincode: @properties_address.pincode, property_id: @properties_address.property_id, state_id: @properties_address.state_id } }
    end

    assert_redirected_to properties_address_url(Properties::Address.last)
  end

  test "should show properties_address" do
    get properties_address_url(@properties_address)
    assert_response :success
  end

  test "should get edit" do
    get edit_properties_address_url(@properties_address)
    assert_response :success
  end

  test "should update properties_address" do
    patch properties_address_url(@properties_address), params: { properties_address: { building: @properties_address.building, city: @properties_address.city, flat_no: @properties_address.flat_no, floor: @properties_address.floor, locality: @properties_address.locality, pincode: @properties_address.pincode, property_id: @properties_address.property_id, state_id: @properties_address.state_id } }
    assert_redirected_to properties_address_url(@properties_address)
  end

  test "should destroy properties_address" do
    assert_difference('Properties::Address.count', -1) do
      delete properties_address_url(@properties_address)
    end

    assert_redirected_to properties_addresses_url
  end
end
