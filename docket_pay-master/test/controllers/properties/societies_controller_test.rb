require 'test_helper'

class Properties::SocietiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @properties_society = properties_societies(:one)
  end

  test "should get index" do
    get properties_societies_url
    assert_response :success
  end

  test "should get new" do
    get new_properties_society_url
    assert_response :success
  end

  test "should create properties_society" do
    assert_difference('Properties::Society.count') do
      post properties_societies_url, params: { properties_society: { amount: @properties_society.amount, property_id: @properties_society.property_id, society_id: @properties_society.society_id } }
    end

    assert_redirected_to properties_society_url(Properties::Society.last)
  end

  test "should show properties_society" do
    get properties_society_url(@properties_society)
    assert_response :success
  end

  test "should get edit" do
    get edit_properties_society_url(@properties_society)
    assert_response :success
  end

  test "should update properties_society" do
    patch properties_society_url(@properties_society), params: { properties_society: { amount: @properties_society.amount, property_id: @properties_society.property_id, society_id: @properties_society.society_id } }
    assert_redirected_to properties_society_url(@properties_society)
  end

  test "should destroy properties_society" do
    assert_difference('Properties::Society.count', -1) do
      delete properties_society_url(@properties_society)
    end

    assert_redirected_to properties_societies_url
  end
end
