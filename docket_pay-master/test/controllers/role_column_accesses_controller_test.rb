require 'test_helper'

class RoleColumnAccessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @role_column_access = role_column_accesses(:one)
  end

  test "should get index" do
    get role_column_accesses_url
    assert_response :success
  end

  test "should get new" do
    get new_role_column_access_url
    assert_response :success
  end

  test "should create role_column_access" do
    assert_difference('RoleColumnAccess.count') do
      post role_column_accesses_url, params: { role_column_access: { column_name: @role_column_access.column_name, role_id: @role_column_access.role_id, table_name: @role_column_access.table_name } }
    end

    assert_redirected_to role_column_access_url(RoleColumnAccess.last)
  end

  test "should show role_column_access" do
    get role_column_access_url(@role_column_access)
    assert_response :success
  end

  test "should get edit" do
    get edit_role_column_access_url(@role_column_access)
    assert_response :success
  end

  test "should update role_column_access" do
    patch role_column_access_url(@role_column_access), params: { role_column_access: { column_name: @role_column_access.column_name, role_id: @role_column_access.role_id, table_name: @role_column_access.table_name } }
    assert_redirected_to role_column_access_url(@role_column_access)
  end

  test "should destroy role_column_access" do
    assert_difference('RoleColumnAccess.count', -1) do
      delete role_column_access_url(@role_column_access)
    end

    assert_redirected_to role_column_accesses_url
  end
end
