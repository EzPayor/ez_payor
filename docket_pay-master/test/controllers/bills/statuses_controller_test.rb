require 'test_helper'

class Bills::StatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bills_status = bills_statuses(:one)
  end

  test "should get index" do
    get bills_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_bills_status_url
    assert_response :success
  end

  test "should create bills_status" do
    assert_difference('Bills::Status.count') do
      post bills_statuses_url, params: { bills_status: { name: @bills_status.name } }
    end

    assert_redirected_to bills_status_url(Bills::Status.last)
  end

  test "should show bills_status" do
    get bills_status_url(@bills_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_bills_status_url(@bills_status)
    assert_response :success
  end

  test "should update bills_status" do
    patch bills_status_url(@bills_status), params: { bills_status: { name: @bills_status.name } }
    assert_redirected_to bills_status_url(@bills_status)
  end

  test "should destroy bills_status" do
    assert_difference('Bills::Status.count', -1) do
      delete bills_status_url(@bills_status)
    end

    assert_redirected_to bills_statuses_url
  end
end
