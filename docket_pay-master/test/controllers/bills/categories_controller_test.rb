require 'test_helper'

class Bills::CategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bills_category = bills_categories(:one)
  end

  test "should get index" do
    get bills_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_bills_category_url
    assert_response :success
  end

  test "should create bills_category" do
    assert_difference('Bills::Category.count') do
      post bills_categories_url, params: { bills_category: { name: @bills_category.name } }
    end

    assert_redirected_to bills_category_url(Bills::Category.last)
  end

  test "should show bills_category" do
    get bills_category_url(@bills_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_bills_category_url(@bills_category)
    assert_response :success
  end

  test "should update bills_category" do
    patch bills_category_url(@bills_category), params: { bills_category: { name: @bills_category.name } }
    assert_redirected_to bills_category_url(@bills_category)
  end

  test "should destroy bills_category" do
    assert_difference('Bills::Category.count', -1) do
      delete bills_category_url(@bills_category)
    end

    assert_redirected_to bills_categories_url
  end
end
