require 'test_helper'

class Banks::EmisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @banks_emi = banks_emis(:one)
  end

  test "should get index" do
    get banks_emis_url
    assert_response :success
  end

  test "should get new" do
    get new_banks_emi_url
    assert_response :success
  end

  test "should create banks_emi" do
    assert_difference('Banks::Emi.count') do
      post banks_emis_url, params: { banks_emi: { bank_id: @banks_emi.bank_id, roi: @banks_emi.roi, status_id: @banks_emi.status_id, tenure: @banks_emi.tenure } }
    end

    assert_redirected_to banks_emi_url(Banks::Emi.last)
  end

  test "should show banks_emi" do
    get banks_emi_url(@banks_emi)
    assert_response :success
  end

  test "should get edit" do
    get edit_banks_emi_url(@banks_emi)
    assert_response :success
  end

  test "should update banks_emi" do
    patch banks_emi_url(@banks_emi), params: { banks_emi: { bank_id: @banks_emi.bank_id, roi: @banks_emi.roi, status_id: @banks_emi.status_id, tenure: @banks_emi.tenure } }
    assert_redirected_to banks_emi_url(@banks_emi)
  end

  test "should destroy banks_emi" do
    assert_difference('Banks::Emi.count', -1) do
      delete banks_emi_url(@banks_emi)
    end

    assert_redirected_to banks_emis_url
  end
end
