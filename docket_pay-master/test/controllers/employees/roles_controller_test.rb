require 'test_helper'

class Employees::RolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employees_role = employees_roles(:one)
  end

  test "should get index" do
    get employees_roles_url
    assert_response :success
  end

  test "should get new" do
    get new_employees_role_url
    assert_response :success
  end

  test "should create employees_role" do
    assert_difference('Employees::Role.count') do
      post employees_roles_url, params: { employees_role: { employee_id: @employees_role.employee_id, role_id: @employees_role.role_id } }
    end

    assert_redirected_to employees_role_url(Employees::Role.last)
  end

  test "should show employees_role" do
    get employees_role_url(@employees_role)
    assert_response :success
  end

  test "should get edit" do
    get edit_employees_role_url(@employees_role)
    assert_response :success
  end

  test "should update employees_role" do
    patch employees_role_url(@employees_role), params: { employees_role: { employee_id: @employees_role.employee_id, role_id: @employees_role.role_id } }
    assert_redirected_to employees_role_url(@employees_role)
  end

  test "should destroy employees_role" do
    assert_difference('Employees::Role.count', -1) do
      delete employees_role_url(@employees_role)
    end

    assert_redirected_to employees_roles_url
  end
end
