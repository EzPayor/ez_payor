ActionMailer::Base.default_url_options = {:host => "docketpay.com"}
ActionMailer::Base.delivery_method = :smtp
# ActionMailer::Base.delivery_method = :ses
ActionMailer::Base.perform_deliveries = true
ActionMailer::Base.raise_delivery_errors = true
ActionMailer::Base.logger = Logger.new('log/mailer.log')
ActionMailer::Base.default :charset => "utf-8"
# ActionMailer::Base.add_delivery_method :ses, AWS::SES::Base,
  # access_key_id: ENV['AMAZON_ACCESS_KEY'],
  # secret_access_key: ENV['AMAZON_SECRET_KEY']
ActionMailer::Base.smtp_settings = {
  :address              => "smtp.gmail.com",
  :port                 => "587",
  :domain               => 'docketpay.com',
  :user_name            => ENV["sender_email"],
  :password             => ENV["sender_email_password"],
  :authentication       => :login,
  :security_type 		    => 'none',
  :enable_starttls_auto => true ,
  :openssl_verify_mode  => 'none' 
}