DocketPay::Application.routes.draw do
	namespace :api do
		namespace :v0 do
			############# 		User API 	     ###############
			match 'user/signup' 	 	 	 	=> 'user_api#signup',						via: [:post] 
			match 'user/signin' 	 	 		=> 'user_api#login',						via: [:get] 
			match 'user/details' 	 	 		=> 'user_api#user_details',					via: [:get] 
			match 'user/profile'  				=> 'user_api#update_profile',				via: [:put,:get] 
			match 'user/mobile' 	 			=> 'user_api#change_mobile',				via: [:put] 
			match 'user/password'		 		=> 'user_api#change_password',				via: [:put] 
			match 'user/password/forgot'		=> 'user_api#forgot_password',				via: [:post,:get] 
			match 'user/password/reset'			=> 'user_api#reset_password',				via: [:put]
			match 'user/generate_otp' 			=> 'user_api#generate_otp', 				via: [:post,:get] 
			match 'user/generate_email' 		=> 'user_api#generate_email', 				via: [:post,:get] 

			############# 		Bill API   	     ###############
			match 'user/bill' 	 	 			=> 'bill_api#create_bill',					via: [:post] 
			match 'user/bill' 	 	 			=> 'bill_api#update_bill',					via: [:put] 
			match 'user/bill' 	 	 			=> 'bill_api#delete_bill',					via: [:delete] 
			match 'user/bill' 	 	 			=> 'bill_api#user_bills',					via: [:get] 
			match 'user/bill/search'  			=> 'bill_api#search_bill',					via: [:get] 
			match 'user/bill/details' 			=> 'bill_api#bill_details',					via: [:get]
			match 'user/bill/pay' 				=> 'bill_api#payment_url',					via: [:get]
			match 'user/bill/receipt' 			=> 'bill_api#rent_receipt', 				via: [:post,:get]
			match 'user/bill/calculate' 		=> 'bill_api#bill_calculation', 			via: [:get]

			############# 		Property API 	 ###############
			match 'user/property'  		 		=> 'property_api#all_properties', 			via: [:get]
			match 'user/property'  		 		=> 'property_api#create_property', 			via: [:post]
			match 'user/property'		 		=> 'property_api#update_property', 			via: [:put]
			match 'user/property'		 		=> 'property_api#delete_property', 			via: [:delete]
			match 'user/property/address'		=> 'property_api#create_address', 			via: [:post]
			match 'user/property/owner'			=> 'property_api#add_owner', 				via: [:post]
			match 'user/property/tenant' 		=> 'property_api#add_tenant', 				via: [:post]
			match 'user/property/society'		=> 'property_api#add_society', 				via: [:get,:post]
			match 'user/property/owner'			=> 'property_api#invite_owner', 			via: [:put]
			match 'user/property/tenant' 		=> 'property_api#invite_tenant', 			via: [:post]
			match 'user/property/status' 		=> 'property_api#property_status', 			via: [:post]
			match 'user/property/activate'		=> 'property_api#activate_property_invite_user', 	via: [:post]
			match 'user/property/validate'		=> 'property_api#create_property_invite_user', via: [:get]
			match 'user/property/associate'		=> 'property_api#remove_associate', 		via: [:put]
			match 'user/property/notification'	=> 'property_api#send_notification', 		via: [:post]

			#############    	Society Api      ############ 
			match 'society'						=> 'society_api#create_society', 			via: [:post]
			match 'society/search'				=> 'society_api#search_society', 			via: [:get]
			match 'user/:profile/bank_details'  => 'society_api#add_bank_detail', 			via: [:post]

			############# 		Notification API ###############
			match 'user/notifications'			=>	'notification_api#notifications', 		via: [:get]
			match 'user/notifications/read'		=> 	'notification_api#read_notification', 	via: [:put]

			# match 'user/notifications/test'	=> 	'notification_api#dummy_notification', 	via: [:get]
			############# 		Invite API 		 ###############
			match 'user/invite'					=>	'invite_api#invite', 					via: [:post]
			match 'user/invitations'			=> 	'invite_api#invite_list', 				via: [:get]

			############# 		Query API  		 ###############
			match 'user/query'					=>	'query_api#query', 						via: [:post]
			match 'user/queries'				=> 	'query_api#queries', 					via: [:get]
			
			############# 		Verify API  		 ###############
			match 'verify/otp'					=>	'user_api#verify_otp', 					via: [:post]
			match 'verify/authorize'			=>	'user_api#validate_url', 				via: [:post]

			match 'upload/s3_url'		 		=> 'file_upload_api#presigned_s3_url',		via: [:get]
			match 'user/file/:type'		 		=> 'file_upload_api#get_invoice',			via: [:get]
			match 'data/static'		 			=> 'user_api#static_data',					via: [:get]
		end
	end
end