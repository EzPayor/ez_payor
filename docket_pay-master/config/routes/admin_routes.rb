DocketPay::Application.routes.draw do
	scope "/admin" do
		match '/' 							=> 'employees#index', 										via: [:post, :get]
		match "properties/payments/update_transaction" 	=> 'properties/payments#update_transaction', 	via: [:put,:post,:get]
		match "employees/logout" 			=> "employees#employee_logout", 	as: "logout_employee", 	via: [:post, :get]
		match "employees/sign_in" 			=> "employees#employee_sign_in", 	as: "employee_sign_in", via: [:post, :get]
		match "employees/login" 			=> "employees#employee_login", 								via: [:post]
		match "employees/:id/edit_role" 	=> "employees#edit_role", 									via: [:post, :get]
		match "employees/:id/add_role" 		=> "employees#add_role", 									via: [:put]
		match "employees/:id/remove_role" 	=> "employees#remove_role", 								via: [:put]
		# namespace :employees do
		match "roles/:id/edit_column_access"              => "roles#edit_column_access", :as => 'edit_column_access', via: [:post, :get]
		match "roles/:id/toggle_column_access"            => "roles#toggle_column_access", 				via: [:put]
		match "roles/:id/edit_permission"                 => "roles#edit_permission", 					via: [:post, :get]
		match "roles/:id/update_permission"               => "roles#update_permission", 				via: [:post, :get]
		post  "roles/:id/add_permission/:resource_id"     => "roles#add_permission", 					via: [:post, :get]
		post  "roles/:id/delete_permission/:resource_id"  => "roles#delete_permission", 				via: [:post, :get]
		match 'properties/:id/details'					  => 'properties#details', as: 'properties_details', via: [:post, :get]
		match 'download_excel'					  		  => 'properties/payments#download_excel',		via: [:post, :get]
		match 'create_excel'							  => 'properties/payments#create_excel',        via: [:post, :get]
		match 'excel'									  => 'properties/payments#excel',				via: [:post, :get]
		resources :roles
		namespace :employees do
			match 'payment_verification/index'				=> 'payment_verification#index',  			via: [:put,:post,:get]
			match 'payment_verification/verify_payment'		=> 'payment_verification#verify_payment',  	via: [:put,:post,:get]
			match 'property_verification/index'				=> 'property_verification#index',  			via: [:put,:post,:get]
			match 'property_verification/verify_property'	=> 'property_verification#verify_property', via: [:put,:post,:get]
			match 'property_verification/report_issue'	=> 'property_verification#report_issue',  via: [:put,:post,:get]
			match 'user_notification/index'					=> 'user_notification#index',  				via: [:put,:post,:get]
			match 'user_notification/send_notification'		=> 'user_notification#send_notification',  	via: [:put,:post,:get]
			match 'user_verification/index'					=> 'user_verification#index',  				via: [:put,:post,:get]
			match 'user_verification/verify_user'			=> 'user_verification#verify_user', 	 	via: [:put,:post,:get]
			match 'properties/search'						=> 'search#filter_property', 				via: [:put,:post,:get]
			match 'properties/search_by'					=> 'search#search_by_property', 			via: [:put,:post,:get]
			match 'properties/property_list'				=> 'search#property_list',					via: [:put,:post,:get]
			match 'properties/society_search'				=> 'search#society_search', 				via: [:put,:post,:get]
			match 'properties/society_list'					=> 'search#society_list',					via: [:put,:post,:get]
			match 'bills/search'            				=> 'payment_verification#search', 			via: [:put,:post,:get]
			match 'users/search'            				=> 'user_verification#search', 				via: [:put,:post,:get]
			match 'users/search_by'         				=> 'user_verification#search_by', 			via: [:put,:post,:get]
			match 'dbmetrics'         						=> 'metrics#index', 						via: [:put,:post,:get]
			match 'dbmetrics/properties'         			=> 'metrics#compact_data', 					via: [:put,:post,:get]
			match 'tenant_verification/index'				=> 'tenant_verification#index',  			via: [:put,:post,:get]
			match 'tenant_verification/verify_tenant'		=> 'tenant_verification#verify_tenant',  	via: [:put,:post,:get]
			match 'tenant_verification/report_issue'		=> 'tenant_verification#report_issue',  	via: [:put,:post,:get]
		end
		resources :employees
		resources :roles_column_accesses
		# resources :roles do
		# end
		resources :users_roles
		resources :permissions
		resources :resources
		resources :role_column_accesses

		namespace :bills do
			resources :statuses
			resources :categories
		end
		resources :bills
		namespace :users do
			namespace :invites do
				resources :statuses
			end
			resources :invites
			resources :queries
			resources :bank_details
			resources :profiles
			resources :authentications
    		resources :statuses
		end
  		namespace :properties do
			resources :tenants
			resources :transactions
			resources :addresses
			resources :owners
			resources :societies
			resources :statuses
    		resources :payments
		end
		match '/users/:id/user_ownership'                 => 'users#list_ownership', as: 'user_ownerships', via: [:post, :get]
		match '/users/:id/user_tenancy'               	  => 'users#list_tenancy', as: 'user_tenancy', via: [:post, :get]
		resources :societies
		resources :statuses
		resources :states
		resources :properties
		resources :users
		resources :notifications
		
		resources :eb_providers
		resources :apps
		namespace :banks do
		    resources :emis
		end
		resources :banks
	end
end
