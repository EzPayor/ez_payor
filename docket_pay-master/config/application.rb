require_relative 'boot'
require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DocketPay
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.encoding = "utf-8"
    # config.assets.paths << Rails.root.join("app", "assets", "fonts",'images')
    config.filter_parameters += [:password,:pin]
    config.assets.paths << Rails.root.join("app", "assets", 'images', 'fonts')
    config.assets.precompile += %w( employees.css application.css employees.js application.js)
    config.time_zone = 'Mumbai'
    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.active_job.queue_adapter = :resque
    # Do not swallow errors in after_commit/after_rollback callbacks.
    # config.active_record.raise_in_transactional_callbacks = true
    config.paths["config/routes.rb"].concat(Dir[Rails.root.join("config/routes/*.rb")])
  end
end
