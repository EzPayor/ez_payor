# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
every 1.days, :at => '10:00 am' do # 1.minute 1.day 1.week 1.month 1.year is also supported
	runner "::Property.generate_recurring_bills"
end
  # runner "MyModel.some_process"
  # rake "my:rake:task"
  # command "/usr/bin/my_great_command"

# export RAILS_DIR=${RAILS_DIR:-/home/ubuntu/docket}
# export RAILS_ENV=${RAILS_ENV:-production}
# export GIT_BRANCH=${GIT_BRANCH:-master}
# set -e
# cd $RAILS_DIR
# mkdir -p tmp/{pids,sockets}

# rake resque:work QUEUE=* BACKGROUND=yes

# echo "== git pull"
# git checkout "$GIT_BRANCH"
# git pull origin "$GIT_BRANCH"
# echo "== bundle install"
# bundle install -j6 --without development test
# echo "== rake assets:precompile"
# bundle exec rake assets:precompile
# echo "== rake db:migrate"
# bundle exec rake db:migrate
# echo "== rake db:seed"
# bundle exec rake db:seed
# echo "== rake admin:populate_resources"
# bundle exec rake admin:populate_resources > log/deployment.log 2>&1
# echo "== Restarting nginx server"
# service nginx restart
# echo "== rails cache clear"
# bundle exec rails runner "Rails.cache.clear"
