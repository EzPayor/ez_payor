Rails.application.routes.draw do
  root to: 'website#index'
  match "/payment/:status/callback" => "bills#icici_payment", :via => [:post,:put,:get]
end
