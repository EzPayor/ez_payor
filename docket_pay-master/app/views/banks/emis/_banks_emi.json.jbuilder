json.extract! banks_emi, :id, :bank_id, :status_id, :tenure, :roi, :created_at, :updated_at
json.url banks_emi_url(banks_emi, format: :json)
