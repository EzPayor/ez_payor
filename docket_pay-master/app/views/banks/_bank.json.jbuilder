json.extract! bank, :id, :name, :status_id, :created_at, :updated_at
json.url bank_url(bank, format: :json)
