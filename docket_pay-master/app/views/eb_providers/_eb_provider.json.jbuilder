json.extract! eb_provider, :id, :state_id, :name, :status_id, :created_at, :updated_at
json.url eb_provider_url(eb_provider, format: :json)
