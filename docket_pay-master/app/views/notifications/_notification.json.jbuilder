json.extract! notification, :id, :user_id, :url, :notification_type, :message, :read, :created_at, :updated_at
json.url notification_url(notification, format: :json)