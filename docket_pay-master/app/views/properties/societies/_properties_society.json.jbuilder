json.extract! properties_society, :id, :property_id, :society_id, :amount, :created_at, :updated_at
json.url properties_society_url(properties_society, format: :json)