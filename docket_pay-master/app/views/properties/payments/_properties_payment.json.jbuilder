json.extract! properties_payment, :id, :bank_detail_id, :bill_id, :property_id, :type, :status_id, :employee_id, :amount, :created_at, :updated_at
json.url properties_payment_url(properties_payment, format: :json)
