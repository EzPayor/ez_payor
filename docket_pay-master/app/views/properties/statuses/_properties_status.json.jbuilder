json.extract! properties_status, :id, :name, :created_at, :updated_at
json.url properties_status_url(properties_status, format: :json)