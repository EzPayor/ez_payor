json.extract! properties_owner, :id, :property_id, :user_id, :amount, :created_at, :updated_at
json.url properties_owner_url(properties_owner, format: :json)