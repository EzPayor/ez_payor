json.extract! users_invite, :id, :name, :email, :mobile, :referral_code, :user_id, :status_id, :created_at, :updated_at
json.url users_invite_url(users_invite, format: :json)