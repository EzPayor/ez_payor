json.extract! users_invites_status, :id, :name, :created_at, :updated_at
json.url users_invites_status_url(users_invites_status, format: :json)