json.extract! user, :id, :name, :email, :mobile, :status_id, :profile_type, :profile_id, :mobile_verified, :email_verified, :verified, :created_at, :updated_at
json.url user_url(user, format: :json)