json.extract! users_authentication, :id, :code, :authentication_type, :user_id, :status_id, :validity, :created_at, :updated_at
json.url users_authentication_url(users_authentication, format: :json)