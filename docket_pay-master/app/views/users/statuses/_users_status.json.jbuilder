json.extract! users_status, :id, :name, :created_at, :updated_at
json.url users_status_url(users_status, format: :json)