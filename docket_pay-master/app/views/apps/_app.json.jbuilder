json.extract! app, :id, :name, :value, :created_at, :updated_at
json.url app_url(app, format: :json)
