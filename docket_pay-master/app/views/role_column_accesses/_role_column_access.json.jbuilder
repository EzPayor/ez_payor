json.extract! role_column_access, :id, :role_id, :table_name, :column_name, :created_at, :updated_at
json.url role_column_access_url(role_column_access, format: :json)