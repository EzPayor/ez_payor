json.extract! role, :id, :name, :role_type, :parent_id, :created_at, :updated_at
json.url role_url(role, format: :json)