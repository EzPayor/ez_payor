json.extract! society, :id, :name, :email, :contact, :registration_no, :created_at, :updated_at
json.url society_url(society, format: :json)