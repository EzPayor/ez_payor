json.extract! bills_status, :id, :name, :created_at, :updated_at
json.url bills_status_url(bills_status, format: :json)