json.extract! bills_category, :id, :name, :created_at, :updated_at
json.url bills_category_url(bills_category, format: :json)