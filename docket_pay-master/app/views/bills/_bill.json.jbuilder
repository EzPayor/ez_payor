json.extract! bill, :id, :date_paid, :category_id, :amount, :status_id, :user_id, :invoice_no, :verified_by, :created_at, :updated_at
json.url bill_url(bill, format: :json)