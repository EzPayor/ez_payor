json.extract! permission, :id, :role_id, :resource_id, :created_at, :updated_at
json.url permission_url(permission, format: :json)