json.extract! employee, :id, :name, :email, :mobile, :password, :parent_id, :city, :created_at, :updated_at
json.url employee_url(employee, format: :json)