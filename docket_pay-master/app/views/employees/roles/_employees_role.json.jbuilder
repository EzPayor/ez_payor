json.extract! employees_role, :id, :role_id, :employee_id, :created_at, :updated_at
json.url employees_role_url(employees_role, format: :json)