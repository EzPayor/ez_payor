require "render_anywhere"
 
class InvoicePdf
  include RenderAnywhere
 
  def initialize(bill_id)
    @bill = Bill.find(bill_id)
    @tenant = @bill.property.active_tenants_details.first
    @owner = @bill.property.active_owners_details.first
    @address = @bill.property.get_address
    @calculation = Bill.invoice_calculation(@bill.id)
    @base_url = ENV['email_base_url']
    @user = User.find(@bill.user_id)
  end
 
  def to_pdf
    receipt_path = "receipt_#{@bill.invoice_no}.pdf"
    invoice_path = "invoice_#{@bill.invoice_no}.pdf"
    new_receipt = PDFKit.new(receipt, page_size: 'A4')
    new_receipt.to_file("#{Rails.root}/tmp/#{receipt_path}")
    new_invoice = PDFKit.new(invoice, page_size: 'A4')
    new_invoice.to_file("#{Rails.root}/tmp/#{invoice_path}")
    return paths = {invoice: invoice_path, receipt: receipt_path}
  end
 
  def filename
    "Invoice #{invoice.id}.pdf"
  end
 
  private
    attr_reader :bill, :tenant, :owner, :address, :calculation, :base_url, :user
    
    def as_html
      render template: "bill_mailer/new_invoice", layout: false, locals: { bill: bill, tenant: tenant, 
                                                    owner: owner, address: address, calculation: calculation, base_url: base_url, user: user }
      # render template: "bill_mailer/receipt", layout: false, locals: { bill: bill, tenant: tenant, owner: owner, address: address}
    end

    def receipt
      render template: "bill_mailer/receipt", layout: false, locals: { bill: bill, tenant: tenant,
                                                    owner: owner, address: address}
    end

    def invoice
      render template: "bill_mailer/new_invoice", layout: "invoice_pdf", locals: { bill: bill, tenant: tenant, 
                                                    owner: owner, address: address, calculation: calculation, base_url: base_url, user: user }
    end
end