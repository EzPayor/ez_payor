class PropertyMailer < ApplicationMailer

	def added property_id
		
	end

	def activated property_id
		@property = Property.find(property_id)
		@base_url = ENV['email_base_url']
		mail( :to => @property.user.email, bcc: ENV['email_bcc'], :subject => 'DocketPay - Your Property has been activated' )
	  	puts 'Invite Property verified'
	end

	def dectivated property_id
		@property = Property.find(property_id)
		@base_url = ENV['email_base_url']
		mail( :to => @property.user.email, bcc: ENV['email_bcc'], :subject => 'DocketPay - Your Property has been activated' )
	  	puts 'Invite Property verified'
	end

	def tenant_added property_id,tenant_id
		@property = Property.find(property_id)
		@base_url = ENV['email_base_url']
		mail( :to => @property.user.email, bcc: ENV['email_bcc'], :subject => 'DocketPay - Your Property has been activated' )
	  	puts 'Invite Property verified'
	end

	def tenant_removed property_id,tenant_id
		@property = Property.find(property_id)
		@base_url = ENV['email_base_url']
		mail( :to => @property.user.email, bcc: ENV['email_bcc'], :subject => 'DocketPay - Your Property has been activated' )
	  	puts 'Invite Property verified'
	end

	def owner property_id
		
	end
end
