class UserMailer < ApplicationMailer

	def welcome_email(user_id)
	  	@user = User.find(user_id)
	   	@base_url = ENV['email_base_url']
	  	mail( :to => @user.email, bcc: ENV['email_bcc'], :subject => 'Welcome to DocketPay' )
	  	puts 'Welcome mail sent'
	end
	
	def password_reset(user_id,activation_link)
	   @base_url = ENV['email_base_url']
	   @user = User.find(user_id)
	   @url = activation_link
	   mail(to: @user.email, bcc: ENV['email_bcc'], :subject => "DocketPay Account Password Reset")
	   puts 'Email password reset mail sent'
	end

	def activation_email(user_id)
	   @base_url = ENV['email_base_url']
	   @user = User.find(user_id)
	   mail(to: @user.email, bcc: ENV['email_bcc'], :subject => "DocketPay Email Verified")
	   puts 'Email Activated mail sent'

	end

	def activate_email(user_id,url)
	   @base_url = ENV['email_base_url']
	   @user = User.find(user_id)
	   @url = url
	   mail(to: @user.email, bcc: ENV['email_bcc'], :subject => "Verify Your DocketPay Email")
	   puts 'Email verification mail sent'

	end

	def verify_account(user_id)
	   @base_url = ENV['email_base_url']
	   @user = User.find(user_id)
	   mail(to: @user.email, bcc: ENV['email_bcc'], :subject => "Verify Your DocketPay Account")
		puts 'Verify mail sent'
	end
	
	def verify_otp user_id,code
		@code = code
		@user = User.find(user_id)
		mail(to: @user.email, bcc: ENV['email_bcc'], :subject => "Your OTP For DocketPay")
		puts 'otp mail sent'
	end

	def property_activated property_id
		@code = code
		@user = User.find(user_id)
		mail(to: @user.email, bcc: ENV['email_bcc'], :subject => "Your OTP For DocketPay")
		puts 'otp mail sent'
	end
end