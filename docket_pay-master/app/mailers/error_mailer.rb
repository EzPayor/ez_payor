class ErrorMailer < ApplicationMailer

	def payment_pending id
		@bill = Bill.find(id)
	   	@base_url = ENV['email_base_url']
	  	mail( :to => 'mahesh@dockettech.com', bcc: ENV['email_bcc'], :subject => 'Payment Pending' )
	  	puts 'Invite mail sent'
	end

	def acc_not_found id
		@bill = Properties::Payment.find(id)
	   	@base_url = ENV['email_base_url']
	  	mail( :to => 'mahesh@dockettech.com', bcc: ENV['email_bcc'], :subject => 'Account Not Found' )
	  	puts 'Invite mail sent'
	end

	def property_not_verified id
		@property = Property.find(id)
	   	@base_url = ENV['email_base_url']
	  	mail( :to => 'mahesh@dockettech.com', bcc: ENV['email_bcc'], :subject => 'Property Not Verified' )
	  	puts 'Property not Verified mail sent'
	end

	def bill_mismatch id
		payment = Properties::Payment.find(id)
	   	@base_url = ENV['email_base_url']
	  	mail( :to => 'mahesh@dockettech.com', bcc: ENV['email_bcc'], :subject => 'Bill Mismatch' )
	  	puts 'Bill Mismatch mail sent'
	end
	
	def verify_new_tenant id
		@tenant = Properties::Tenant.find(id)
	   	@base_url = ENV['email_base_url']
	  	mail( :to => 'mahesh@dockettech.com', bcc: ENV['email_bcc'], :subject => 'Verify New Tenant' )
	  	puts 'Verify New Tenant mail sent'
	end
end
