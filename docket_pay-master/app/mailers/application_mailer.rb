class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@docketpay.com'
  layout 'mailer'
end
