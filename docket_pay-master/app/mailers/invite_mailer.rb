class InviteMailer < ApplicationMailer
	
	def invite id,url
		@invite = Users::Invite.find(id)
	   	@base_url = ENV['email_base_url']
	   	@url = url
	  	mail( :to => @invite.email, bcc: ENV['email_bcc'], :subject => 'Invitation For DocketPay - Your Online Rental Payment Solution' )
	  	puts 'Invite mail sent'
	end

	def property_tenant_invite id,url,property_id
		@invite = Users::Invite.find(id)
		@landlord = Properties::Owner.where(user_id: @invite.user_id, property_id: property_id).first
		@property = Property.find(property_id)
	   	@base_url = ENV['email_base_url']
	   	@url = url
	  	mail( :to => @invite.email, bcc: ENV['email_bcc'], :subject => 'Invitation For DocketPay - Your Online Rental Payment Solution' )
	  	puts 'Invite mail sent'
	end

	def property_owner_invite id,url,property_id
		@invite = Users::Invite.find(id)
		@tenant = Properties::Tenant.where(user_id: @invite.user_id, property_id: property_id).first
		@property = Property.find(property_id)
	   	@base_url = ENV['email_base_url']
	   	@url = url
	  	mail( :to => @invite.email, bcc: ENV['email_bcc'], :subject => 'Invitation For DocketPay - Your Online Rental Payment Solution' )
	  	puts 'Invite mail sent'
	end

	def property_society_invite id,url
		@invite = Users::Invite.find(id)
	   	@base_url = ENV['email_base_url']
	   	@url = url
	  	mail( :to => @invite.email, bcc: ENV['email_bcc'], :subject => 'Invitation For DocketPay - Your Online Rental Payment Solution' )
	  	puts 'Invite mail sent'
	end
end
