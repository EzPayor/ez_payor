class BillMailer < ApplicationMailer

	def generate_reciept
		
	end

	def generate_invoice bill_id
		@bill = Bill.find(bill_id)
		@tenant = @bill.property.active_tenants.first
		@owner = @bill.property.active_owners.first
		mail( :to => @bill.user.email, bcc: ENV['email_bcc'], :subject => "Invoice for #{@bill.invoice_no} | DocketPay - Your Online Rental Payment Solution" )
	  	puts 'Bill paid mail sent'
	end

	def notify_bill
		
	end

	def bill_paid bill_id
		@bill = Bill.where(id: bill_id).first
		@base_url = ENV['email_base_url']
		mail( :to => @bill.user.email, bcc: ENV['email_bcc'], 
			:subject => "Payment Received For #{@bill.property.uid} | DocketPay - Your Online Rental Payment Solution" )
	  	puts 'Bill paid mail sent'
	end

	def rent_due associate_id,associate_type
		case associate_type.downcase
		when 'tenant' 
			@associate = Properties::Tenant.where(id: associate_id).first
		else
			puts 'some error in due calculation'
		end
		@base_url = ENV['email_base_url']
		if @associate
			mail( :to => @associate.email, bcc: ENV['email_bcc'], 
			:subject => "Rent Payment Due For #{@associate.property.uid} | DocketPay - Your Online Rental Payment Solution" )
		end
	  	puts 'rent due mail sent'

	end

	def maintenance_due associate_id,associate_type
		case associate_type.downcase
		when 'tenant' 
			@associate = Properties::Tenant.where(id: associate_id).first
		when 'landlord'
			@associate = Properties::Owner.where(id: associate_id).first
		else
			puts 'some error in due calculation'
		end
		@base_url = ENV['email_base_url']
		if @associate
			mail( :to => @associate.email, bcc: ENV['email_bcc'], 
			:subject => "Maintenance Payment Due For #{@associate.property.uid} | DocketPay - Your Online Rental Payment Solution" )
		end
	  	puts 'maintenance due mail sent'
	end
end
