class Employee < ApplicationRecord
	before_create :create_and_update_password
	before_update :update_password
	#TODO : can't validate password right now because of hash
	validates :email, :presence => true, :uniqueness => true#, :email => true
	validates :mobile, :presence => true
	
	has_and_belongs_to_many :roles, :join_table => "employees_roles"
	has_many :children, :class_name => "Employee", :foreign_key => "parent_id"
	belongs_to :parent, :class_name => "Employee", optional: true
	has_many :queries, class_name: "::Users::Query"


	def self.authenticate(email, password)
		@employee = Employee.where('email' => email)
		if @employee.count == 1 and Password::check(password, @employee[0].password)
			@employee[0]
		else
			nil
		end
	end

	def get_children
		employees = Employee.find_by_sql('WITH RECURSIVE children AS ( select * from employees where id = '+self.id.to_s+' UNION ALL select employees.* from employees,children where employees.parent_id = children.id) select * from children')
		return employees - [self]
	end

	def get_children_roles
		my_roles = self.roles.collect { |role| role.id  }.join(",")
		roles =  Role.find_by_sql('WITH RECURSIVE children AS ( select * from roles where id in ('+my_roles+') UNION ALL select roles.* from roles,children where roles.parent_id = children.id) select * from children')
		unless self.roles.include?(Role.where(:name => 'admin')[0])
			roles = roles - self.roles
		end
		return roles
	end

	def check_parent parent_id
		if self.roles.include?(Role.where(:name => 'admin').first)
			return true
		end
		if Employee.find(parent_id).get_children.include?(self)
			return true
		else
			return false
		end
	end

	def check_child_role role_id
		if get_children_roles.include?(Role.find(role_id))
			return true
		else
			return false
		end
	end

	def self.employee_predictions(query_string)
		keyword = "%#{query_string.upcase}%"
		employees = Employee.where("UPPER(employees.email) LIKE ? OR employees.mobile LIKE ?", keyword,keyword)
							.limit(20)
							.select(["employees.id as id",
									"CONCAT(employees.email,'-',employees.mobile,'-',employees.profile_id) as name"])
	end

	def self.employee_predictions_by_role(query_string,role_name)
		keyword = "%#{query_string.upcase}%"
		role_name = role_name.to_s.strip.downcase
		data_collectors = Employee.joins(:roles)
								.where('lower(roles.name) = ?', role_name)
								.select(["CONCAT(employees.email,'-',employees.mobile,'-',employees.id) as name",
	  								 "employees.id as id"])
	end

	def self.ids_from_tokens(tokens)
		tokens.split(",")
	end

	def tokenize
		{:id => self.id, :name => "#{self.email}-#{self.mobile}-#{self.id}"}
	end


	protected 
	def update_password
		self.password = Password::update(self.password) if self.password_changed?
	end

	def create_and_update_password
		self.roles.push(Role.where(:name => "open_user", :role_type => "U").first_or_create)
		self.password = SecureRandom.base64(6).gsub(/[$=+\/]/,65.+(rand(25)).chr) if self.password.nil?
		self.password = Password::update(self.password)
	end
end
