class Property < ApplicationRecord
	before_create :set_initials
	# after_create :create_first_bill
	
	has_one :properties_societies, class_name: "::Properties::Society"
	has_one :address, 	class_name: "::Properties::Address"
	
	has_many :tenants, 	class_name: "::Properties::Tenant"
	has_many :owners, 	class_name: "::Properties::Owner"
	has_many :bills, 	class_name: "::Bill"
	has_many :payments, class_name: "::Properties::Payment"
	has_many :queries, class_name: "Users::Query"
	
	belongs_to :user, 	class_name: "::User", foreign_key: :added_by
	belongs_to :status, class_name: "::Properties::Status", optional: true
	has_one :society, 	through: :properties_societies
	
	validates :eb_account, uniqueness: {scope: :eb_provider}
	# after_save :send_verified_email
	after_create :set_uid

	def get_address type='compact'
		address = self.address
		if type != 'compact'
			adr = (address.flat_no.to_s + address.floor.to_s + ", " + address.building.to_s + ", " + address.locality.to_s + ", " + address.city.to_s + ", " + address.state.name.to_s)
		else
			adr = (address.flat_no.to_s + address.floor.to_s + ", " + address.building.to_s + ", " + address.locality.to_s + ", " + address.city.to_s)
		end
		return (adr.split(',').reject { |c| (c.strip.empty? || c.strip!.empty?) rescue nil }).join(', ')
	end

	def set_uid
		self.uid = "P" + Date.today.strftime('%Y%m%d') + "#{self.id}"
		self.save!
	end

	def last_paid_rent
		bills = self.bills.where(status_id: Bills::Status.where(name: ['PAID', 'VERIFIED']))
				.where(category_id: Bills::Category.where(name: ['RENT', 'RENT + MAINTENANCE']))
					.order('date_paid DESC')
		if bills.length > 0
			last_date = bills.first.date_paid.to_date.mjd
			date_today = Date.today.mjd
			return ((date_today - last_date) >= 25) ? true : false
		else
			return true
		end
	end

	def last_paid_maintenance
		bills = self.bills.where(status_id: Bills::Status.where(name: ['PAID', 'VERIFIED']))
				.where(category_id: Bills::Category.where(name: ['MAINTENANCE', 'RENT + MAINTENANCE']))
					.order('date_paid DESC')
		if bills.length > 0
			last_date = bills.first.date_paid.to_date.mjd
			date_today = Date.today.mjd
			return ((date_today - last_date) >= 25) ? true : false
		else
			return true
		end
	end

	def active_tenants_details
		self.tenants.where('properties_tenants.status_id =?', Properties::Status.where(name: "ACTIVE").first.id)
	end

	def active_owners_details
		self.owners.where('properties_owners.status_id =?', Properties::Status.where(name: "ACTIVE").first.id)
	end

	# def active_society_details
	# 	if self.society_available && (self.maintenance_paid_to == 'Society')
	# 		self.society.joins(:bank_details)
	# 		.select("societies.id, societies.name, societies.email, bank_details.ifsc, bank_details.acc_number, 
	# 				bank_details.acc_type,bank_details.account_holder_name, bank_details.bank_name ")
	# 	end
	# end

	def validate_no_of_property user_id,property_id
		
	end

	def validate_property_relation user_id,property_id
		if (self.owners.where(user_id: user_id).length > 0)
			return "owner"
		elsif	self.tenants.where(user_id: user_id)
			return 'tenant'
		else
			return nil
		end
	end

	# def create_first_bill
	# 	bill = self.bills.create(amount: self.total_amount, 
	# 							category_id: Bills::Category.where(name: "RENT + MAINTENANCE").first.id,
	# 							user_id: self.added_by)
	# end

	def set_initials
		self.total_amount = self.rent.to_i + self.maintenance.to_i
		self.status_id = ::Properties::Status.where(name: 'NEW').first.id
	end

	def self.set_user_type is_owner,property_id,user_id
		user = User.find(user_id)
		if (is_owner.downcase == 'true')
			property_owner = ::Property.find(property_id).owners.first_or_create(email: user.email)
			if property_owner.new_record?
				property_owner.name = user.name
				property_owner.mobile = user.mobile
			end
			property_owner.save!
			return property_owner
		else
			nil
		# else
		# 	property_tenant = ::Property.find(property_id).tenants.first_or_create(email: user.email)
		# 	if property_tenant.new_record?
		# 		property_tenant.name = user.name
		# 		property_tenant.mobile = user.mobile
		# 		# property_tenant.staying_since = Time.parse(params[:staying_since])
		# 		# property_tenant.invited_by = params[:user_id]
		# 	end
		# 	property_tenant.save!
		# 	return property_tenant
		end
	end

	# Cron job Functions

	def self.generate_recurring_notifications
		date = (Time.now + 5.day).day
		::Property.all.where(status_id: Properties::Status.where(name: "VERIFIED").first.id).each do |property|
			if (property.rent_date == date)
				notification = ::Notification.new(user_id: property.active_tenants.first.id)
				notification.message = ""
				# bill.category = 'RENT + MAINTENANCE',
				# bill.amount = property.total_amount,
				# bill.property_id = property.property_id,
				notification.save!
			end
		end
	end

	# verified property email trigger
	def send_verified_email
		if self.status_id
			if self.status_id == Properties::Status.where(name: "VERIFIED").first.id
				PropertyJob.perform_later(self.id, 'activated')
				# send_verify_email
			end
		end
	end
end
