class EbProvider < ApplicationRecord
	validates :name, presence: true
	belongs_to :status, class_name: "::Status", optional: true
	belongs_to :state, class_name: "::State"
end
