class Properties::Owner < ApplicationRecord
	belongs_to 	:property, 	class_name: "::Property"
	belongs_to 	:user, 		class_name: "::User", optional: true
	belongs_to 	:status, 	class_name: "::Status", optional: true	
	
	has_many 	:bank_details, as: :account, class_name: "::Users::BankDetail"
	
	before_create :set_initials
	before_validation :prep_email

	after_create :check_if_user_exist

	after_create :check_if_user_exist

	def set_initials
		self.status_id = ::Status.where(name: "ACTIVE").first.id
	end
	
	def check_if_user_exist
		if !self.user_id
			# users = User.where(email: self.email)
			# if users.length > 0

			# 	# send email if user doesn't exist
			# else
				invite = Users::Invite.new(
							user_id: self.invited_by,
							name: self.name,
							email: self.email,
							mobile: self.mobile,
							referral_code: User.find(self.invited_by).profile.referral_code,
							invite_type: 'Owner',
							property_id: self.property_id
							)
				invite.save!
				# Send invite email for the property
			# end
		end
	end

	def self.update_status status,user_id
		self.update_all(status_id: ::Status::where(name: status.upcase).first.id)
		if status == 'inactive'
			self.update_all(end_date: Date.today, removed_by: user_id)
		end
	end

	private
	def prep_email
		self.email = self.email.strip.downcase if self.email
	end	
end
