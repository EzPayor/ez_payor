class Properties::Society < ApplicationRecord
	belongs_to :property, class_name: "::Property"
	belongs_to :society, class_name: "::Society"
end
