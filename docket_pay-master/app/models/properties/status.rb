class Properties::Status < ApplicationRecord
	has_many :properties, class_name: "::Property"
end
