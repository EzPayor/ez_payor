class Properties::Tenant < ApplicationRecord
	belongs_to 	:property, 	class_name: "::Property"
	belongs_to 	:user, 		class_name: "::User", optional: true
	belongs_to 	:status, 	class_name: "::Status",optional: true	
	
	has_many 	:bank_detail, as: :account, class_name: "::Users::BankDetail"
	
	before_validation :prep_email
	before_create :set_initials

	after_create :check_if_user_exist
	
	def set_initials
		self.status_id = ::Status.where(name: "ACTIVE").first.id if self.status_id.nil?
	end

	def check_if_user_exist
		if !self.user_id
			users = User.where(email: self.email)
			# if users.length > 0
			# 	# send email if user doesn't exist
			# else
				invite = Users::Invite.new( 
							email: self.email,
							name: self.name,
							user_id: self.invited_by,
							mobile: self.mobile,
							referral_code: User.find(self.invited_by).profile.referral_code,
							invite_type: 'Tenant',
							property_id: self.property_id
							)
						invite.save!
				# Send invite email for the property
			# end
		end
	end

	def self.send_sms msg,mobile
		uri = URI(ENV['sms_trxn_url'])
		 params = { #	user: ENV['sms_username'], 
					# pwd: ENV['sms_password'],
					to: mobile,
					workingkey: ENV["sms_key"],
					sender: ENV['sms_sender_id'], 
					message: msg#, 
					# fl: 0,
					# gwid: 2
				}
		uri.query = URI.encode_www_form(params)
		res = Net::HTTP.get_response(uri)
		puts "sms sent #{mobile}"
	end

	def self.update_status status,user_id
		self.update_all(status_id: ::Status::where(name: status.upcase).first.id)
		if status == 'inactive'
			self.update_all(end_date: Date.today, removed_by: user_id)
		end
	end

	private
	def prep_email
		self.email = self.email.strip.downcase if self.email
	end
end
