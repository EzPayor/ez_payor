class Properties::Payment < ApplicationRecord
	validates 	:amount, presence: true

	belongs_to 	:status, 	class_name: "::Bills::Status", 	optional: true
	belongs_to	:property, 	class_name: "::Property"
	belongs_to	:bill, 	class_name: "::Bill"

	belongs_to 	:bank_detail, class_name: "::Users::BankDetail",optional: true
	has_many 	:transactions, as: :payment, class_name: "::Properties::Transaction"
	
	before_create :set_initials
	after_create :check_bank_account

	def set_initials
		self.status_id = ::Bills::Status.where(name: "PENDING").first.id if self.status_id.nil?
	end

	def check_bank_account
		if self.bank_detail_id.nil?
			# trigger error for bank not found
			# elsif self.bank_detail.status == 
		end
	end
end
 