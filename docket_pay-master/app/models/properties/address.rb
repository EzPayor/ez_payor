class Properties::Address < ApplicationRecord
	belongs_to :property, class_name: "::Property", optional: true
	belongs_to :state, class_name: "::State"
end
