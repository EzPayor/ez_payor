class Banks::Emi < ApplicationRecord
	belongs_to :status, class_name: "::Status"
	belongs_to :bank, class_name: "::Bank"
end
