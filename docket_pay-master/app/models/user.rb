class User < ApplicationRecord
	# before_create 		:create_and_update_password
	# before_update 		:update_password
	before_validation 	:prep_email
	before_create 		:generate_authcode
	after_create 		:send_email
	belongs_to 			:profile, polymorphic: true, dependent: :destroy, optional: true
	belongs_to 			:status, class_name: "::Users::Status", optional: true

	has_many :owners, 		 	class_name: "::Properties::Owner"
	has_many :tenants, 		 	class_name: "::Properties::Tenant"
	has_many :invites, 		 	class_name: "::Users::Invite"
	has_many :queries, 		 	class_name: "::Users::Query"
	has_many :notifications, 	class_name: "::Notification"
	has_many :bills, 		 	class_name: "::Bill"
	has_many :properties, 		class_name: "::Property", foreign_key: :added_by
	has_many :authentications, 	class_name: "::Users::Authentication"

	validates :name, 	presence: true
	validates :mobile, 	presence: true, uniqueness: true
	validates :email, 	presence: true, uniqueness: true #, format: { with: /^[\w.+-]+@([\w]+.)+\w+$/ }

	#### On create user send email ####
	def send_email
		self.authentications.create(authentication_type: 'Activation') if !self.email_verified
		# UserJob.perform_later(self.id,'signup')
	end

	def self.authenticate(data, password ,type='email')
		if type == 'email'
			@user = User.where('email' => data)
		else
			@user = User.where('mobile' => data)
		end
		if @user.count == 1 and Password::check(password, @user[0].password)
		  @user[0]
		else
		  nil
		end
	end

	def check_dashboard_access
		case self.dashboard_list
		when ['tenant']
			puts ""
		when ['tenant', 'landlord']
			puts ""

		when ['society']
			puts ""

		when ['business::tenant']
			puts ""

		when ['business::landlord']
			puts ""

		end
	end

	def self.check_user mobile,email
		users = self.where("mobile='#{mobile}' or email='#{email}'")
	end

	private
	def prep_email
		self.email = self.email.strip.downcase if self.email
	end
	
	def generate_authcode
		o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
		string = ''
		3.times do 
			string += (0...8).map { o[rand(o.length)] }.join + '-'
		end
		self.authcode = string
		self.status_id = Status.where(name: "ACTIVE").first.id
	end

	protected 
	####### Update password of user #######
	def update_password
		self.password = Password::update(self.password) if self.password_changed?
	end
	####### hash password of users after create ######
	def create_and_update_password
		self.status_id = Users::Status.where(name: 'NEW').first.id
		self.password = SecureRandom.base64(6).gsub(/[$=+\/]/,65.+(rand(25)).chr) if self.password.nil?
		self.password = Password.update(self.password)
	end
end
