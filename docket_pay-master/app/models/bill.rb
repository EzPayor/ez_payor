class Bill < ApplicationRecord
	belongs_to 	:user, 		class_name: "::User", 			optional: true
	belongs_to 	:status, 	class_name: "::Bills::Status", 	optional: true
	belongs_to 	:category, 	class_name: "::Bills::Category",optional: true
	belongs_to	:property, 	class_name: "::Property"
	
	has_many 	:transactions, as: :payment, class_name: "::Properties::Transaction"
	has_many 	:payments, class_name: "::Properties::Payment"
	validates 	:amount, presence: true

	before_create :set_initials
	after_create :generate_invoice_no

	def set_initials
		self.status_id = Bills::Status.where(name: "PENDING").first.id
		self.service_charge = 	ENV['service_charge']
		self.pg_charge = 		ENV['pg_charge']
		self.convenience = 		ENV['convenience']
		result = Bill.bill_calculation(self.property_id,self.amount,self.category.name)
		self.final_amount = result['total_amount']
		self.actual_amount = result['actual_amount']
	end
	def generate_invoice_no
		self.invoice_no = Date.today.strftime("%d%m%y") + self.id.to_s
		self.save!
	end

	def check_for_security_paid bill_id
		bill = Bill.find(bill_id)
		tenant = Properties::Tenant.where(user_id: bill.user_id).first
		if tenant
			tenant.security_amount_paid = true
			tenant.save!
		end
	end

	def self.create_payment_list bill_id
		bill = Bill.find(bill_id)
		payment = Properties::Payment.new(bill_id: bill.id)
		case bill.category.name
		when "RENT"
			payment.bank_detail_id = bill.property.active_owners_details.first.bank_details.first.id rescue nil
			payment.property_id = bill.property_id
			payment.payment_type = bill.category.name
			payment.amount = bill.amount
		when "SECURITY DEPOSIT"
			payment.bank_detail_id = bill.property.active_owners_details.first.bank_details.first.id rescue nil
			payment.property_id = bill.property_id
			payment.payment_type = bill.category.name
			payment.amount = bill.amount
			check_for_security_paid bill_id
		when "MAINTENANCE"
			case bill.property.maintenance_paid_to.downcase
			when 'society'
				bank_id = bill.property.society.bank_details.first.id rescue nil
			when 'landlord'
				bank_id = bill.property.active_owners_details.first.bank_details.first.id rescue nil
			else
				bank_id = bill.property.active_owners_details.first.bank_details.first.id rescue nil
			end
			payment.bank_detail_id = bank_id
			payment.property_id = bill.property_id
			payment.payment_type = bill.category.name
			payment.amount = bill.amount
		when "RENT + MAINTENANCE"
			case bill.property.maintenance_paid_to.downcase
			when 'society'
				society_bank_id = bill.property.society.bank_details.first.id rescue nil
				
				bank_id = bill.property.active_owners_details.first.bank_details.first.id rescue nil
				amount = bill.amount.to_i - bill.property.maintenance.to_i
				society_payment = Properties::Payment.new(bill_id: bill.id)
				society_payment.bank_detail_id = society_bank_id
				society_payment.property_id = bill.property_id
				society_payment.payment_type = bill.category.name
				society_payment.amount = bill.property.maintenance
				society_payment.save!
			when 'landlord'
				bank_id = bill.property.active_owners_details.first.bank_details.first.id rescue nil
				amount = bill.amount
			else
				bank_id = bill.property.active_owners_details.first.bank_details.first.id rescue nil
				bill.amount
			end
			payment.bank_detail_id = bank_id
			payment.property_id = bill.property_id
			payment.payment_type = bill.category.name
			payment.amount = amount
		when "CORPUS FUNDS"
			if (bill.property.maintenance_paid_to.downcase == 'society')
				bank_id = bill.property.society.bank_details.first.id
			else
				bank_id = nil
			end
			payment.bank_detail_id = bank_id
			payment.property_id = bill.property_id
			payment.payment_type = bill.category.name
			payment.amount = bill.amount
		else
			puts 'no bank found'
			# trigger for bank not found
		end
		if payment.save!
			if bill.amount != bill.payments.sum(:amount)
				# email trigger for 
			end
		end
	end
	# def self.compact_detail user_id
	# 	joins(:property)
	# 		.where(user_id: user_id)
	# 		.select 
	# end

	# def get_bank_details
	# 	case self.category.name
	# 	when 'RENT'
	# 		self.property.owners.where()
	# end

	def self.update_status invoice_no,status
		bill = self.where(invoice_no: invoice_no).first
		if status == 'success'
			bill.date_paid = Time.now
			bill.status_id = ::Bills::Status.where(name: 'PAID').first.id
			BillJob.perform_later(bill.id)
        elsif status == 'failure'
			bill.status_id = ::Bills::Status.where(name: 'FAILED').first.id
		else
			bill.status_id = ::Bills::Status.where(name: 'TRANSACTION DECLINED').first.id
        end
		bill.save!
	end

	def self.generate_bill_payment_url bill_id
		# bill = Bill.find(bill_id)
		options = ::Properties::Transaction.icici_payment(bill_id)
		# @bill = Bill.find(bill_id)
		# url = ENV['ICICI_SERVER']
		# return "https://#{@options.host}#{@options.request_uri}"
		return options
	end

	def self.bill_calculation property_id,amount,category
		property = Property.find(property_id)
		result = Hash.new
		case category
		when "RENT"
			actual_amount = property.rent.to_i
			validation = (actual_amount >= amount) ? true : false
		when "SECURITY DEPOSIT"
			actual_amount = property.security_deposit.to_i
			validation = (actual_amount >= amount) ? true : false
		when "MAINTENANCE"
			actual_amount = property.maintenance.to_i
			validation = (actual_amount >= amount) ? true : false
		when "RENT + MAINTENANCE"
			actual_amount = (property.rent.to_i + property.maintenance.to_i)
			validation = (actual_amount >= amount) ? true : false
		when "CORPUS FUNDS"
			actual_amount = amount
			validation = true
		else
			validation = false
			actual_amount = amount
		end
		if validation
			result['convenience_fee'] = (amount.to_f * ENV['convenience'].to_f * 0.01).round(2)
			result['pg_charge'] = (amount.to_i * ENV['pg_charge'].to_f * 0.01).round(2)
			result['service_tax'] = (result['pg_charge'].to_f * ENV['service_charge'].to_f * 0.01).round(2) 
			result['total_amount'] = (amount.to_f + result['pg_charge'].to_f + result['service_tax'].to_f + result['convenience_fee'].to_f).round(2)
			result['actual_amount'] = actual_amount
		else
			result = nil
		end
		return result
	end

	def self.invoice_calculation bill_id
		bill = Bill.find(bill_id)
		result = Hash.new
		case bill.category.name
		when "RENT"
			actual_amount = bill.property.rent.to_i
			validation = (actual_amount >= bill.amount) ? true : false
		when "SECURITY DEPOSIT"
			actual_amount = bill.property.security_deposit.to_i
			validation = (actual_amount >= bill.amount) ? true : false
		when "MAINTENANCE"
			actual_amount = bill.property.maintenance.to_i
			validation = (actual_amount >= bill.amount) ? true : false
		when "RENT + MAINTENANCE"
			actual_amount = (bill.property.rent.to_i + bill.property.maintenance.to_i)
			validation = (actual_amount >= bill.amount) ? true : false
		when "CORPUS FUNDS"
			actual_amount = bill.amount
			validation = true
		else
			validation = false
			actual_amount = bill.amount
		end
		if validation
			result['convenience_fee'] = (bill.amount.to_f * bill.convenience.to_f * 0.01).round(2)
			result['pg_charge'] = (bill.amount.to_i * bill.pg_charge.to_f * 0.01).round(2)
			result['service_tax'] = (result['pg_charge'].to_f * bill.service_charge.to_f * 0.01).round(2) 
			result['total_amount'] = (bill.amount.to_f + result['pg_charge'].to_f + result['service_tax'].to_f + result['convenience_fee'].to_f).round(2)
			result['actual_amount'] = actual_amount
		else
			result = nil
		end
		return result
	end
end
