class Notification < ApplicationRecord
	validates :message, presence: true
	belongs_to :user, class_name: "::User"

	def self.get_user_notifications user_id
		self.where(user_id: user_id)
			.order('id desc')
			.map{|notify| { id: notify.id, read: notify.read,
					notification_type: notify.notification_type, 
					message: notify.message,
					url: notify.url,
					created_at: notify.created_at
					}
				}
	end
end
