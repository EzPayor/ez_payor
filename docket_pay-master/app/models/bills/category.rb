class Bills::Category < ApplicationRecord
	has_many :bills, class_name: "::Bill"
end
