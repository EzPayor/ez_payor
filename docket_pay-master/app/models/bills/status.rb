class Bills::Status < ApplicationRecord
	has_many :bills, class_name: "::Bill"
	has_many :payments, class_name: "::Properties::Payment"
end
