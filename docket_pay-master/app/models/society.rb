class Society < ApplicationRecord
	before_validation 	:prep_email
	validates :email, 	presence: true#, uniqueness: true

	has_many :properties_societies, class_name: "::Properties::Society"
	has_many :properties, through: :properties_societies #, :join_table => "properties_societies"
	has_many :bank_details, as: :account, class_name: "::Users::BankDetail"
	belongs_to :creator, class_name: "::User", foreign_key: :added_by, optional:true
	
	after_create :check_if_user_exist
	def check_if_user_exist
		if !self.added_by
			users = User.where(email: self.email)
			invite = Users::Invite.new(
						email: self.email,
						name: self.name,
						user_id: self.added_by,
						mobile: self.contact,
						invite_type: 'Society',
						property_id: (self.properties.first.id rescue nil)
						)
			invite.save!
		end
	end

	def self.predictions keyword
		self.where("lower(name) like ?", "%#{keyword.to_s.downcase}%")
	end
	
	private
	def prep_email
		self.email = self.email.strip.downcase if self.email
	end
end
