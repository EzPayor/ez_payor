class Status < ApplicationRecord
	has_many :states, class_name: "::State"
	has_many :authentication, class_name: "::Users::Authentication"
end
