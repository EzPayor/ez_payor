class Users::Invites::Status < ApplicationRecord
	has_many :invites, class_name: "::Users::Invite"
end
