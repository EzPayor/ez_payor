class Users::BankDetail < ApplicationRecord
	validates :account_holder_name, presence: true
	validates :bank_name, presence: true
	validates :ifsc, presence: true
	validates :acc_number, presence: true
	validates :acc_type, presence: true
	# validates :user_id, presence: true
	has_many :payments, class_name: "::Properties::Payment"
	
	belongs_to :account, polymorphic: true
end