class Users::Authentication < ApplicationRecord
	after_create 	:create_code
	before_create 	:set_status
	
	belongs_to 		:user
	belongs_to 		:status, class_name: "::Status", optional: true
	
	def self.check_valid code,type
		auths = self.where(status_id: ::Status.where(name: "ACTIVE").first.id).where(code: code, authentication_type: type.capitalize).where('users_authentications.validity >= ?', Time.now)
		if auths.length > 0
			auths.first.status_id = ::Status.where(name: 'INACTIVE').first.id
			auths.first.save!
			user_id = auths.first.user_id
		else
			user_id = nil
		end
		return user_id
	end

	protected
	def generate_token
      token = ::SecureRandom.urlsafe_base64
	end

	def generate_otp
		otp = ((0..9).to_a.shuffle[0,6]).shuffle.join
	end

	def set_status
		self.status_id = ::Status.where(name: 'ACTIVE').first.id
	end
	
	def create_code
		if self.authentication_type == 'Activation'
			self.code = generate_token
			self.validity = (Time.now + 2.day)
			activation_link = ENV['base_url'] + '/#!/dashboard/verify/activation/' + self.code
			UserJob.perform_later(self.user_id,'email_verify',activation_link)
		elsif self.authentication_type == 'Reset'
			self.code = generate_token
			self.validity = (Time.now + 2.hour)
			activation_link = ENV['base_url'] + '/#!/dashboard/verify/reset/' + self.code
			UserJob.perform_later(self.user_id,'reset',activation_link)
		elsif self.authentication_type == 'Otp'
			self.code = generate_otp
			self.validity = (Time.now + 30.minute)
			SmsJob.perform_later(self.user_id,self.code,'otp',nil)
			# UserJob.perform_now(self.user_id,'otp',self.code)
		elsif self.authentication_type == 'Login'
			self.code = generate_otp
			self.validity = (Time.now + 30.minute)
			SmsJob.perform_now(self.user_id,self.code,'otp',nil)
		elsif self.authentication_type == 'Property'
			self.code = generate_otp
			self.validity = (Time.now + 30.minute)
			SmsJob.perform_now(self.user_id,self.code,'otp',nil)
		elsif self.authentication_type == 'Signup'
			self.code = generate_otp
			self.validity = (Time.now + 30.minute)
			SmsJob.perform_now(self.user_id,self.code,'otp',nil)
			# UserJob.perform_now(self.user_id,'otp',self.code)
		elsif self.authentication_type == 'Activate'
			self.code = generate_token
			self.validity = (Time.now + 2.day)
			activation_link = ENV['base_url'] + '/#!/dashboard/verify/activation/' + self.code
			UserJob.perform_later(self.user_id,'email_verify',activation_link)
		end
		set_status
		self.save!
	end
end
