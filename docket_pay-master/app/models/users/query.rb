class Users::Query < ApplicationRecord
	validates :email, 	presence: true
	
	belongs_to :user, class_name: "::User", optional: true
	belongs_to :status, class_name: "::Status", optional: true
	belongs_to :property, class_name: "::Property", optional: true
	belongs_to :employee, class_name: "::Employee", foreign_key: :assigned_to, optional: true
	before_create :set_initials
	
	def set_initials
		self.status_id = ::Status.where(name: "NEW").first.id if self.status_id.nil?
	end
	
	def self.categories
		category_list = {
			"General": ['Documents', 'Invite', 'Others'],
			"Property": ['Tenant', 'Landlord', 'Account Details'],
			"Payment": ['Security Deposit', 'Maintenance', 'Society', 'Refund', 'Payment Gateway'],
			"Sales": ['Business Development', 'Society'],
			"Information": ['Society Contact Information', 'Landlord Account Details', 'Society Account Details'],
			"Offers": ['Deals & Coupons', 'Partner with us', 'Points & Redemption']
		}
		return category_list.stringify_keys
	end
end
