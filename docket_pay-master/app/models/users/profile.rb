class Users::Profile < ApplicationRecord
	has_one :user, as: :profile
	before_create  :generate_referral_code

	protected
	def generate_referral_code
		code = generate_code(8)
		while !(check_ref_code(code))
			code = generate_code(8)
		end
		self.referral_code = code
	end

	def check_ref_code code
		if Users::Profile.where(referral_code: code).length > 0
			false
		else 
			true
		end
	end
	def generate_code num=8
		o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
		code = (0...num).map { o[rand(o.length)] }.join
		return code
	end
end
