class Users::Invite < ApplicationRecord
	validates :email, presence: true
	belongs_to :user, class_name: "::User", optional: true
	belongs_to :status, class_name: "::Users::Invites::Status", optional: true
	
	before_create 	:set_initials
	after_create 	:create_code
	
	before_validation 	:prep_email
	# invite_code is referral code of other person inviting, used at signup
	# referral_code is for inviting other people
	def set_initials
		self.status_id = ::Users::Invites::Status.where(name: "INVITED").first.id
	end

	def check_if_exist
		users = User.where(email: self.email)
		if (users.length >= 0) && (self.invite_type.downcase == 'invite')
			return false
		else
			# send email invite to user code here
			return true
		end
	end

	def self.check_invite_status code,email
		invites = self.where(email: email)
		if invites.length > 0
			invite = invites.where(code: code).first
			if invite
				invite.status_id = ::Users::Invites::Status.where(name: "SIGNUP").first.id
				invite.save!
			else
				# check for recourse
			end
		else
		end
	end

	def self.verify_property_invite type,code,property_id
		self.where(invite_type: type.capitalize, code: code, status_id: Users::Invites::Status.where(name: 'INVITED'), property_id: property_id)
	end

	protected
	def generate_token 
      token = ::SecureRandom.urlsafe_base64
	end

	# def generate_otp
	# 	otp = ((0..9).to_a.shuffle[0,6]).shuffle.join
	# end

	# def set_status
	# 	self.status_id = ::Status.where(name: 'ACTIVE').first.id
	# end
	
	def create_code
		if check_if_exist
			if self.invite_type.downcase == 'tenant'
	      		self.code = generate_token
				token = ((self.code) + ('|' + Base64.encode64(self.property_id.to_s)))
				activation_link = ENV['base_url'] + '/#!/dashboard/verify/tenant/' + URI::encode(token)
				InviteJob.perform_later(self.id,self.invite_type.downcase,activation_link,property_id)
			elsif self.invite_type.downcase == 'owner'
				self.code = generate_token
				token = ((self.code) + ('|' + Base64.encode64(self.property_id.to_s)))
				activation_link = ENV['base_url'] + '/#!/dashboard/verify/owner/' + URI::encode(token)
				InviteJob.perform_later(self.id,self.invite_type.downcase,activation_link,property_id)
			elsif self.invite_type.downcase == 'society'
				self.code = generate_token
				token = ((self.code) + ('|' + Base64.encode64(self.property_id.to_s)))
				activation_link = ENV['base_url'] + '/#!/dashboard/verify/society/' + URI::encode(token)
				InviteJob.perform_later(self.id,self.invite_type.downcase,activation_link,property_id)
			elsif self.invite_type.downcase == 'invite'
				activation_link = ENV['base_url'] + '/#!/dashboard/verify/invite/' + self.referral_code
				InviteJob.perform_later(self.id, self.invite_type.downcase, activation_link)
			end
			self.save!
		end
	end

	private
	def prep_email
		self.email = self.email.strip.downcase if self.email
		self.invite_type = self.invite_type.strip.capitalize if self.invite_type
	end
end
