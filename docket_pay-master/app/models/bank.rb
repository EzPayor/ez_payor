class Bank < ApplicationRecord
	validates :name, presence: true
	has_many :emis, class_name: "::Banks::Emi"
	before_create :set_status
	belongs_to :status, class_name: "::Status", optional: true

	def set_status
		self.status_id = ::Status.where(name: "ACTIVE").first.id
	end

	def self.get_emi_list
		self.includes(:emis).joins(:emis)
			.where("banks_emis.status_id = ?", Status.where(name:"ACTIVE").first.id)
			.distinct.map{|bank|{
				name: bank.name,
				emis: bank.emis.map{|bank| {tenure: bank.tenure, roi: bank.roi}}
			}}
	end
end
