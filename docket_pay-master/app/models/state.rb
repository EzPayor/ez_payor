class State < ApplicationRecord
	has_many :address, class_name: '::Properties::Address'
	has_many :eb_providers, class_name: '::EbProvider'
end
