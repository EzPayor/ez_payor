module Api
  module ApiStatusList 
    OK = { 'status' => 'OK', 'message' => 'success'}
    UNKNOWN_ERROR = { 'status' => 'UNKNOWN_ERROR', 'message' => 'An error occurred. Please retry.'}
    ZERO_RESULTS = { 'status' => 'ZERO_RESULTS', 'message' => 'No matching results were found'}
    OVER_QUERY_LIMIT = { 'status' => 'OVER_QUERY_LIMIT', 'message' => 'You have exhausted your api quota'}
    REQUEST_DENIED = { 'status' => 'REQUEST_DENIED', 'message' => 'You are not authorised to make this request.'}
    INVALID_REQUEST = { 'status' => 'INVALID_REQUEST', 'message' => 'The request you sent is invalid'}
    NOT_FOUND = { 'status' => 'NOT_FOUND', 'message' => 'The requested resource could not be found.'}
    INVALID_CAPTCHA = { 'status' => 'INVALID_CAPTCHA', 'message' => 'Captcha validation failed.'}
    INVALID_API_KEY = { 'status' => 'INVALID_API_KEY', 'message' => 'Your API key is invalid.'}
    USER_NOT_FOUND = { 'status' => 'USER_NOT_FOUND', 'message' => 'Your EMAIL or mobile is invalid.'}
    MOBILE_EXIST = { 'status' => 'MOBILE ALREADY EXIST', 'message' => 'Your mobile is already registered with other user.'}
    USER_ALREADY_EXIST = { 'status' => 'USER ALREADY EXIST', 'message' => 'Your email/mobile is already registered with other user.'}
    ALREADY_REGISTERED = { 'status' => 'YOUR EMAIL/MOBILE ALREADY REGISTERED', 'message' => 'Your email/mobile is already registered with us.'}
    NOT_ACTIVE = { 'status' => 'NOT ACTIVE', 'message' => 'Your account is not active with us'}
    INCORRECT_PASSWORD = {'status' => 'INCORRECT PASSWORD', 'message' => 'Incorrect password'}
    INCORRECT_OTP = {'status' => 'INCORRECT OTP', 'message' => 'Incorrect otp'}
    INVALID_INVITE = {'status' => 'INVALID_INVITE', 'message' => 'The invite is invalid!'}
    CODE_EXPIRED = {'status' => 'CODE EXPIRED', 'message' => 'The link has expired try again!'}
    CODE_INVALID = {'status' => 'CODE INVALID', 'message' => 'The link is invalid try again!'}
    INVALID_MOBILE = {'status' => 'MOBILE NOT FOUND', 'message' => 'The mobile number does not exist.'}
    INVALID_EMAIL = {'status' => 'EMAIL NOT FOUND', 'message' => 'The email does not exist.'}
    PROPERTY_EXIST = {'status' => 'Property EXISTS', 'message' => 'This property with given Electricity account already exists.'}
    SOCIETY_EXIST = {'status' => 'SOCIETY EXISTS', 'message' => 'This society with given registration number already exists.'}
    TENANT_EXIST = {'status' => 'TENANT EXISTS', 'message' => 'This tenant with given account details already exists.'}
    OWNER_EXIST = {'status' => 'OWNER EXISTS', 'message' => 'This owner with given account details already exists.'}
  end
end