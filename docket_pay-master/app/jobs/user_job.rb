class UserJob < ApplicationJob
	queue_as :users
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end
  
	def perform(*args)
		user_id,type,code = args[0], args[1], args[2]
		if type == 'signup'	
			UserMailer.welcome_email(user_id).deliver_later
		elsif type == 'reset'
			UserMailer.password_reset(user_id,code).deliver_later
		elsif type == 'activated'
			UserMailer.activation_email(user_id).deliver_later
		elsif type == 'email_verify'
			UserMailer.activate_email(user_id,code).deliver_later
		elsif type == 'verify_account'
			UserMailer.verify_account(user_id,code).deliver_later
		elsif type == 'otp'
			UserMailer.verify_otp(user_id,code).deliver_later
		end
	end
	protected
	def notify_manager
		puts 'User sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception + 'users email'
	end
end
