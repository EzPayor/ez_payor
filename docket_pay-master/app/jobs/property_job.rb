class PropertyJob < ApplicationJob
 	queue_as :property
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end
  
	def perform(*args)
		property_id,type = args[0],args[1]
		if type == 'activated'
			PropertyMailer.activated(property_id).deliver_later
			SmsJob.perform_later('property_verified')
		else
			'nothing found to do'
		end
	end
	protected
	def notify_manager
		puts 'property email sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception + 'Proeprty email'
	end
end
