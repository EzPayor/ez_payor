class BillJob < ApplicationJob
	queue_as :bills
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end
  
	def perform(*args)
		bill_id = args[0]
		paths = ::InvoicePdf.new(bill_id).to_pdf
		invoice_url = upload("#{Rails.root}/tmp/#{paths[:invoice]}", paths[:invoice])
		receipt_url = upload("#{Rails.root}/tmp/#{paths[:receipt]}", paths[:receipt])
		bill = Bill.find(bill_id)
		bill.receipt_url = receipt_url
		bill.invoice_url = invoice_url
		bill.save!
		Bill.create_payment_list(bill.id)
		BillMailer.bill_paid(bill_id).deliver_later
	end

	def upload path,file_name
		s3 = AWS::S3.new
		key = file_name
		obj = s3.buckets[ENV['S3_BUCKET']].objects[key].write(:file => path)
		FileUtils.rm(path)
		return obj.public_url.to_s
	end
	protected
	def notify_manager
		puts 'Bills sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception.to_s + 'bills email'
	end
end
