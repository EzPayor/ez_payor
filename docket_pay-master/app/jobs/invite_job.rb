class InviteJob < ApplicationJob
	queue_as :invite
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end
  
	def perform(*args)
		id,type,url,property_id = args[0], args[1], args[2],args[3]
		if type == 'tenant'
			InviteMailer.property_tenant_invite(id,url,property_id).deliver_later
		elsif type == 'owner'
			InviteMailer.property_owner_invite(id,url,property_id).deliver_later
		elsif type == 'society'
			InviteMailer.property_society_invite(id,url).deliver_later
		elsif type == 'invite'
			InviteMailer.invite(id,url).deliver_later
		end
	end

	protected
	
	def notify_manager
		puts 'Invite sent'
	end
	
	private
	def notify_failed_job_to_manager(exception)
		puts exception.to_s + 'Invite email'
	end

end
