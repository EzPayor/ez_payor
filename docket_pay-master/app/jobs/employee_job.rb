class EmployeeJob < ApplicationJob
  queue_as :default
  after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end

  def perform(*args)
	bill_id, property_id, tenant_id, payment_id, type = args[0], args[1], args[2], args[3]
	if type == 'payment_pending'
	  ErrorMailer.payment_pending(bill_id).perform_later
	elsif 
	  type == 'acc_not_found'
	  ErrorMailer.acc_not_found(payment_id).perform_later
	elsif 
	   type == 'property_not_verified'
	   ErrorMailer.property_not_verified(property_id).perform_later
	elsif 
	   type == 'bill_mismatch'
	   ErrorMailer.bill_mismatch(payment_id).perform_later
	elsif 
	   type == 'verify_new_tenant'
	   ErrorMailer.verify_new_tenant(tenant_id).perform_later
   	end  	
  end

  protected
	def notify_manager
		puts 'Bills sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception + 'bills email'
	end

end
