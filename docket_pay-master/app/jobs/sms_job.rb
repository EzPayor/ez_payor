class SmsJob < ApplicationJob
	queue_as :sms
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end
  
	def perform(*args)
		user_id,code,type,mobile,bill_id = args[0], args[1], args[2], args[3], args[4]
		user = User.find(user_id) if !user_id.nil?
		# bill_id = 2
		@bill = Bill.find(bill_id) if !bill_id.nil?
		@date = Date.today
		date = @date.strftime("%B")
		# url = nil
		# @property = nil
		msg1 = 'Welcome to DocketPay, Your complete rental solution.'
		# code
		msg2 = "OTP for DocketPay is #{code} . Please do not share it with anyone."
		if !bill_id.nil?
			# month rent_date
			msg3 = "Dear Customer, Your rent for the month of is due on #{@bill.property.rent_date}. Login to DocketPay and pay your rent online"
			# rent_amount month address
		 	msg4 = "Dear Customer, You made a payment of Rs. #{@bill.amount} towards rent of #{date} for #{@bill.property.address.building}"
			# rent_amount address
			msg5 = "Dear Customer, Thank you for the payment. We have received Rs. #{@bill.amount} towards rent for #{@bill.property.address.building}"
			# security_deposit address
			msg6 = "Dear Customer, Your security deposit of #{@bill.amount} is due for #{@bill.property.address.building}. Login to DocketPay and pay your deposit amount online"
	        # security_deposit address
			msg7 = "Dear Customer, You made a payment of Rs. #{@bill.amount} towards security deposit for #{@bill.property.address.building}"
			# security_deposit address
			msg8 = "Dear Customer, Thank you for the payment of Rs. #{@bill.amount} towards security deposit for #{@bill.property.address.building}"
			# maintenance month date
			msg9 = "Dear Customer, Your maintenance rent for the month of #{date} is due on #{@bill.property.rent_date}. Login to DocketPay and pay your maintenance rent online"
			# maintenance month address
			msg10 = "Dear Customer, You made a payment of Rs. #{@bill.amount} towards maintenance of #{date} for #{@bill.property.address.building}"
			# maintenance address
			msg11 = "Dear Customer, Thank you for the payment. We have received Rs. #{@bill.amount} towards maintenance for #{@bill.property.address.building}"
	        #corpus_amount date
			msg12 = "Dear Customer, Your corpus fund rent for the month of #{date} is due on #{@bill.property.rent_date}. Login to DocketPay and pay corpus fund rent online"
			#corpus_amount address
			msg13 = "Dear Customer, You made a payment of Rs. #{@bill.amount} towards corpus fund for #{@bill.property.address.building}"
			#corpus_amount address
			msg14 = "Dear Customer, Thank you for the payment. We have received Rs. #{@bill.amount} towards corpus fund for #{@bill.property.address.building}"
			#corpus_amount address
			msg15 = "Welcome to DocketPay. Your property #{@bill.property.uid} has been successfully added. Pay your rent, maintenance, security deposit, corpus fund online"
			#property_uid
			msg16 = "Your property id #{@bill.property.uid} has been verified. Please login to DocketPay and make payments online with multiple methods"
		end
		#code
		msg17 = "Here is your invite #{code} for DocketPay. Pay rent, maintenance, security deposit, corpus fund online with credit/debit cards and Net Banking. Login to www.docketpay.com."
		
		mobile_no = user ? user.mobile : mobile
		case type
		when 'welcome'
			msg = msg1
			send_sms(msg, mobile_no)
		when 'otp'
			msg = msg2
			send_sms(msg, mobile_no)
		when 'rent'
			msg = msg3
			send_sms(msg, mobile_no)
		when 'rent_paid'
			msg = msg4
			send_sms(msg, mobile_no)
		when 'rent_received'
			msg = msg5
			send_sms(msg, mobile_no)
		when 'security'
			msg = msg6
			send_sms(msg, mobile_no)
		when 'security_paid'
			msg = msg7
			send_sms(msg, mobile_no)
		when 'security_received'
			msg = msg8
			send_sms(msg, mobile_no)
		when 'maintenance'
			msg = msg9
			send_sms(msg, mobile_no)
		when 'maintenance_paid'
			msg = msg10
			send_sms(msg, mobile_no)
		when 'maintenance_received'
			msg = msg11
			send_sms(msg, mobile_no)
		when 'corpus'
			msg = msg12
			send_sms(msg, mobile_no)
		when 'corpus_paid'
			msg = msg13
			send_sms(msg, mobile_no)
		when 'corpus_received'
			msg = msg14
			send_sms(msg, mobile_no)
		when 'property_add'
			msg = msg15
			send_sms(msg, mobile_no)
		when 'property_verified'
			msg = msg16
			send_sms(msg, mobile_no)
		when 'invite'
			msg = msg17
			send_sms(msg, mobile_no)
		else
			puts 'no msg found'
		end
	end

	def send_sms msg,mobile
		uri = URI(ENV['sms_trxn_url'])
		 params = { #	user: ENV['sms_username'], 
					# pwd: ENV['sms_password'],
					to: mobile,
					workingkey: ENV["sms_key"],
					sender: ENV['sms_sender_id'], 
					message: msg 
					# fl: 0,
					# gwid: 2
				}
		uri.query = URI.encode_www_form(params)
		res = Net::HTTP.get_response(uri)
		puts "sms sent #{mobile}"
	end

	protected
	def notify_manager
		puts 'sms sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception.to_s + 'sms email'
	end
end
