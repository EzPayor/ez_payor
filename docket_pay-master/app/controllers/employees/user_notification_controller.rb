class Employees::UserNotificationController < ApplicationController
	layout 'employee'
	
	def index
		@notification = Notification.new
	end

	def send_notification
		case type
		when 'user'
			user = User.find(params[:user_id])
			notification = Notification.create(user_id: user.id, 
												message: params[:message], url: params[:url], 
												notification_type: params[:notification_type])
		when 'all'
			User.all.each do |user|
				notification = Notification.create(user_id: user.id, 
												message: params[:message], url: params[:url], 
												notification_type: params[:notification_type])
			end
		else
			puts 'No proper command given'
		end
	end

end
