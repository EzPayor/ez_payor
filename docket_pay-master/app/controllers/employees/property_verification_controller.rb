class Employees::PropertyVerificationController < ApplicationController
	layout 'employee'
	
	def index
		@property = Property.all.includes(:address, tenants: [:bank_detail], owners: [:bank_details])
								.where(status_id: Properties::Status.where(name: ['NEW','ACTIVE']).ids).sample
		if @property 
			@active_tenants = @property.tenants.where(status_id: Properties::Status.where(name: 'ACTIVE').first.id)
			@owners = @property.owners.where(status_id: Properties::Status.where(name: 'ACTIVE').first.id)
		end
	end

	def verify_property
		if params[:property]
			@property = Property.find(params[:property])
			if @property
				@property.status_id = Properties::Status.where(name: 'VERIFIED').first.id
				@property.save!
				@property.send_verified_email
				redirect_to employees_property_verification_index_path, notice: 'Profile Verified' 
			else
				@property.status_id = Properties::Status.where(name: 'ACTIVE').first.id
				redirect_to employees_property_verification_index_path, :alert => 'Profile Not found'
			end
		end
	end

	def report_issue
		if params[:property] && !params[:message].empty?
			@property = Property.find(params[:property])
			if @property
				@property.report_issue = params[:message]
				@property.save!
				redirect_to employees_property_verification_index_path, notice: 'Profile Verified' 
			else
				redirect_to employees_property_verification_index_path, :alert => 'Profile Not found'
			end
		else
			redirect_to employees_property_verification_index_path, :alert => 'Enter valid Issues'
		end
	end

	def search
     
  	end

	def search_by 
		# if params[:type] == 'tag'
			# @users = Property.where("lower(tag) like ?", "%#{params[:query].to_s.downcase}%").order('id')
		# els
		if params[:type] == 'user_id' 
			@users = Property.where(email: params[:query]).order('id')
		elsif params[:type] == 'id'
			@users = Property.where(id: params[:query])
		end
		render 'users', layout: false
	end
end
