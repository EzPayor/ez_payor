class Employees::SearchController < ApplicationController
	layout 'employee'

	def check_permission
  	  return true
    end

    def property_list
    	@properties = Property.all
  	end

	def filter_property
	end
 
	def search_by_property 
	   if params[:type] == 'uid'
	      @properties = Property.where(uid: params[:query].strip).order('id')
	   elsif params[:type] == 'owner_email' 
	     properties_ids = Properties::Owner.joins(:property).where(email: params[:query].strip.strip).pluck(:property_id)
	     @properties = 	Property.where(id: properties_ids)
	   elsif params[:type] == 'owner_phone'
	   	properties_ids = Properties::Owner.joins(:property).where(mobile: params[:query].strip).pluck(:property_id)
	     @properties = 	Property.where(id: properties_ids)
	    elsif params[:type] == 'tenant_email'
	      properties_ids = Properties::Tenant.joins(:property).where(email: params[:query].strip).pluck(:property_id)
	     @properties = 	Property.where(id: properties_ids)
	    elsif params[:type] == 'tenant_phone'
	     properties_ids = Properties::Tenant.joins(:property).where(mobile: params[:query].strip).pluck(:property_id)
	     @properties = 	Property.where(id: properties_ids)
	    end
	    if @properties
		 render 'property_list', layout: false
		end
	end
 
	def society_list
		@societies = Society.all
	end

	def society_search
	   if params[:type] == 'email'
	   	@societies = Society.where(email: params[:query].strip).order('id')
	   elsif params[:type] == 'name'
	      @societies = Society.where("lower(name) like ?", "%#{params[:query].strip.to_s.downcase}%").order('id')
	   end
	   if @societies
		 render 'society_list', layout: false
		end
	end

end
