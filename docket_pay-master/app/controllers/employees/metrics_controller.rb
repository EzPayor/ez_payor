class Employees::MetricsController < ApplicationController
	layout 'employee'

	def check_permission
    	true
	end

	def index
	    @incomplete_bills_count    	=    Bill.where(status_id: Bills::Status.where(name: 'DUE').first.id).count
	    @inprogress_bills_count    	=    Bill.where(status_id: Bills::Status.where(name: 'PENDING').first.id).count
	    @completed_bills_count     	=    Bill.where(status_id: Bills::Status.where(name: 'PAID').first.id).count
	  	@properties_count 			=    Property.all.where(status_id: Properties::Status.where(name: 'VERIFIED').first.id).count
	  	@unverified_properties_count=    Property.all.where(status_id: Properties::Status.where(name: 'NEW').first.id).count
	  	@tenants_count 	        	=    Properties::Tenant.all.count
	  	@landlords_count 	        =    Properties::Owner.all.count
	    @successfull_payments_count =    Properties::Transaction.all.where(status: 'success').count
	  	@failed_payments_count      =    Properties::Transaction.all.where(status: 'fail').count
	  	@employees_count 			=    Employee.all.count
	  	@user_count				    =    User.all.count
	  	@contact_request_count		=    Users::Query.all.count
	end
	
	def compact_data
		
	end
end
