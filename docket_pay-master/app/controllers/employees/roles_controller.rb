class Employees::RolesController < ApplicationController
  before_action :set_employees_role, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /employees/roles
  # GET /employees/roles.json
  def index
    @employees_roles = Employees::Role.all
  end

  # GET /employees/roles/1
  # GET /employees/roles/1.json
  def show
  end

  # GET /employees/roles/new
  def new
    @employees_role = Employees::Role.new
  end

  # GET /employees/roles/1/edit
  def edit
  end

  # POST /employees/roles
  # POST /employees/roles.json
  def create
    @employees_role = Employees::Role.new(employees_role_params)

    respond_to do |format|
      if @employees_role.save
        format.html { redirect_to @employees_role, notice: 'Role was successfully created.' }
        format.json { render :show, status: :created, location: @employees_role }
      else
        format.html { render :new }
        format.json { render json: @employees_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/roles/1
  # PATCH/PUT /employees/roles/1.json 
  def update 
    respond_to do |format|
      if @employees_role.update(employees_role_params)
        format.html { redirect_to @employees_role, notice: 'Role was successfully updated.' }
        format.json { render :show, status: :ok, location: @employees_role }
      else
        format.html { render :edit }
        format.json { render json: @employees_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/roles/1
  # DELETE /employees/roles/1.json
  def destroy
    @employees_role.destroy
    respond_to do |format|
      format.html { redirect_to employees_roles_url, notice: 'Role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employees_role
      @employees_role = Employees::Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employees_role_params
      params.require(:employees_role).permit(:role_id, :employee_id)
    end
end
