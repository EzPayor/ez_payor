class Employees::TenantVerificationController < ApplicationController
	layout 'employee'
	
	def index
		@tenant = Properties::Tenant.all.includes(:bank_detail, property: [:address, owners: :bank_details])
								.where(status_id: Status.where(name: 'NEW').ids).sample
	end

	def verify_tenant
		if params[:tenant]
			@tenant = Properties::Tenant.find(params[:tenant])
			if @tenant
				@tenant.status_id = Status.where(name: 'ACTIVE').first.id
				@tenant.save!
				redirect_to employees_tenant_verification_index_path, notice: 'Tenant Activated' 
			else
				@tenant.status_id = Status.where(name: 'NEW').first.id
				redirect_to employees_tenant_verification_index_path, :alert => 'Tenant not found'
			end
		end
	end

	def report_issue
		if params[:tenant] && params[:message]
			@tenant = Properties::Tenant.find(params[:tenant])
			if @tenant
				@tenant.report_issue = params[:message]
				@tenant.save!
				redirect_to employees_tenant_verification_index_path, notice: 'Profile Verified' 
			else
				redirect_to employees_tenant_verification_index_path, :alert => 'Profile Not found'
			end
		end
	end

end
