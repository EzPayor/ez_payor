class Employees::PaymentVerificationController < ApplicationController
	layout 'employee'
	
	def index
		random_ids = Bill.where(status_id: Bills::Status.where(name: 'PAID').first.id).pluck(:id)
		@bill = Bill.includes(:user,:status,:category,:property,:payments)
						.where(id: (random_ids[Random.rand(random_ids.length) - 1 ])).first
	end

	def verify_bill
		@bill = Bill.find(params[:bill_id])
		if params[:verfied] == 'true' && (@bill.status_id == Bill::Status.where(name: 'PAID').first.id)
			@bill.status_id = Bill::Status.where(name: 'VERIFIED').first.id
			@bill.save!
		else
			# raise error in verifying the bill
			# fire an email to concerned authority for verification
		end
		redirect_to index
	end

	def search
     
  	end

	def search_by 
		if params[:type] == 'name'
			@users = Properties::Transaction.where("lower(name) like ?", "%#{params[:query].to_s.downcase}%").order('id')
		elsif params[:type] == 'email' 
			@users = Properties::Transaction.where(email: params[:query]).order('id')
		elsif params[:type] == 'phone'
			@users = Properties::Transaction.where(phone: params[:query]).order('id')
		end
		render 'users', layout: false
	end

end
