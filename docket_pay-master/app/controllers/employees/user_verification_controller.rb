class Employees::UserVerificationController < ApplicationController
	layout 'employee'

	def index
		random_ids = User.where(mobile_verified: true, email_verified: true, verified: false).pluck(:id)
		if random_ids.length > 0
			@users = User.includes(:profile, :tenants, :owners, :bills)
						.where(id: (random_ids[Random.rand(random_ids.length) - 1 ]))
		else
			@users = nil
		end
	end

	def verify_user
		@user = User.find(params[:user_id])
		if params[:verfied] == 'true'
			if (user.mobile_verified == true) && (user.email_verified == true)
				@user.status_id = Users::Status.where(name: 'VERIFIED').first.id
				@user.save!
			end
		else
			@user.status_id = Users::Status.where(name: 'INVALID').first.id
		end
		@user.save!
		redirect_to index
	end

	def search
     
  	end

	def search_by 
		if params[:type] == 'name'
			@users = User.where("lower(name) like ?", "%#{params[:query].strip.to_s.downcase}%").order('id')
		elsif params[:type] == 'email' 
			@users = User.where(email: params[:query].strip).order('id')
		elsif params[:type] == 'phone'
			@users = User.where(mobile: params[:query].strip).order('id')
		end
		render 'users', layout: false
	end
end
