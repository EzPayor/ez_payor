module Api
module V0
	class UserApiController < ApiController
	  
		def check_permission
			return false
		end

		# User Signup
		def signup
			response = Hash.new
			result = Hash.new
			if params[:email] && params[:name] && params[:mobile] ## && params[:password]
				users = User.check_user(params[:mobile],params[:email])
				if users.length == 0
					user = User.new(mobile:params[:mobile], email: params[:email], name: params[:name])
						# , password: params[:password])
 					if user.save!
 						profile = ::Users::Profile.new(first_name: params[:name].split(' ').first, last_name: params[:name].split(' ').last)
 						profile.referral_code = params[:referral_code]
 						profile.save!
 						user.profile_type = "::Users::Profile"
 						user.profile_id = profile.id
 						user.save!
						# result = get_user_details user.id
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::UNKNOWN_ERROR
						status = 500
					end
				else
					response.merge! ApiStatusList::USER_ALREADY_EXIST
					status = 400
				end
				response['result'] = result
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			render :json => response, status: (status || 200)
		end

		# User login
		def login
			response = Hash.new
			result = Hash.new
			if params[:otp] && params[:type]
				if params[:email] && (User.where(email: params[:email]).length > 0)
					user = User.where(email: params[:email]).first
					user_id = user.authentications.check_valid(params[:otp], params[:type].capitalize)
			    elsif params[:mobile] && (User.where(mobile: params[:mobile]).length > 0)
			    	user = User.where(mobile: params[:mobile]).first
					user_id = user.authentications.check_valid(params[:otp], params[:type].capitalize)
			    else
			    	response.merge! ApiStatusList::USER_NOT_FOUND
			    end
				if user_id
					result = get_user_details user_id
					result['authcode'] = User.find(user_id).authcode
					response.merge! ApiStatusList::OK
					# result['states'] = State.all.select(:id,:name).as_json
					response['result'] = result
				else
		    		response.merge! ApiStatusList::INCORRECT_OTP
		    		status = 400
		    	end
		    else
	    		response.merge! ApiStatusList::INVALID_REQUEST
	    		status = 400
	    	end
	    	render :json => response, status: (status || 200)

		end

		# Get User details
		def user_details
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				result = get_user_details params[:user_id]
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['data'] = result
			render :json => response, status: (status || 200)
		end

		# Update user profile
		def update_profile
			response = Hash.new
			result = Hash.new
			if !params[:user_id].nil? && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				if user && user.profile
					if (params[:type].downcase == 'details')
						if !params[:name].blank?
							user.name = params[:name] 
							user.profile.first_name = params[:name].split(' ').first
							user.profile.first_name = params[:name].split(' ').last
						end
						user.profile.profile_img = params[:profile_img] if !params[:profile_img].blank?
						user.profile.gender = params[:gender] if !params[:gender].blank?
						user.profile.dob = Time.parse(params[:birthday]) if !params[:birthday].blank?
						user.profile.pancard_url = params[:pancard_url] if !params[:pancard_url].blank?
						user.profile.id_proof_url = params[:id_proof_url] if !params[:id_proof_url].blank?
						user.profile.city = params[:city] if !params[:city].blank?
						user.profile.save!
						user.save!
						response.merge! ApiStatusList::OK
					elsif (params[:type].downcase == 'mobile') && params[:new_mobile]
						users = User.where(mobile: params[:new_mobile])
						if users.length > 0
							response.merge! ApiStatusList::ALREADY_EXIST
							status = 400
						else
							user.mobile = params[:new_mobile]
							user.mobile_verified = false
							user.save!
							response.merge! ApiStatusList::OK
						end
					elsif (params[:type].downcase == 'password') && params[:old_password]
						person = User.authenticate(user.email, params[:old_password])
						if person
							person.password = params[:new_password]
							person.save!
							response.merge! ApiStatusList::OK
						else
							response.merge! ApiStatusList::INCORRECT_PASSWORD
							status = 400
						end
					end
				else
					response.merge! ApiStatusList::NOT_FOUND
					status = 400
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			result = get_user_details user.id
			response['data'] = result
			render :json => response, status: (status || 200)
		end

		# VERIFY OTP
		def verify_otp
			response = Hash.new
			result = Hash.new
			if !params[:user_id].nil? && User.exists?(params[:user_id])
				user_id = Users::Authentication.all.check_valid(params[:code], (params[:type].capitalize))
				if user_id
					user = User.find(user_id)
					if (params[:type].downcase == 'signup')
						user.mobile_verified = true 
						user.verified = true 
						user.save!
						UserJob.perform_later(user.id,'signup')
					end
					result = get_user_details(user.id)
					result['authcode'] = User.find(user_id).authcode
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INCORRECT_OTP
					status = 400
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['result'] = result
			render :json => response, status: (status || 200)
		end

		def get_user_details id
			user = User.includes(:profile).where(id: id).first
			result = {
				id: user.id, name: user.name, email: user.email, mobile: user.mobile,
				profile_img: user.profile.profile_img || '',
				gender: user.profile.gender || '',
				birthday: user.profile.dob ? user.profile.dob : '',
				id_proof_url: user.profile.id_proof_url || '',
				pancard_url: user.profile.pancard_url || '',
				invite_code: user.profile.invite_code || '',
				city: user.profile.city || '',
				mobile_verified: user.mobile_verified,
				email_verified: user.email_verified,
				dashboard_list: user.dashboard_list || []
				}
		end

		# CHANGE MOBILE NUMBER
		def change_mobile
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				users = User.where(mobile: params[:new_mobile])
				if users.length > 0
					response.merge! ApiStatusList::ALREADY_EXIST
					status = 400
				else
					user.mobile = params[:new_mobile]
					user.save!
					response.merge! ApiStatusList::OK
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['data'] = result
			render :json => response, status: (status || 200)
		end

		# Change Password
		def change_password
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				user = User.authenticate(params[:email], params[:password])
				if user
					user.password = params[:new_password]
					user.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INCORRECT_PASSWORD
					status = 400
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['data'] = result
			render :json => response, status: (status || 200)
		end

		# Forgot Password
		def forgot_password
			response = Hash.new
			result = Hash.new
			if params[:email]
				user = User.where(email: params[:email], profile_type: '::Users::Profile')
				if user.length == 1
					user.first.authentications.create(authentication_type: 'Reset')
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INVALID_EMAIL
				end
			elsif params[:mobile]
				user = User.where(phone: params[:mobile],profile_type: '::Users::Profile')
				if user.length == 1
					user.first.authentications.create(authentication_type: 'Reset')
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INVALID_MOBILE
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		# Reset Password
		def reset_password
			response = Hash.new
			result = Hash.new
			if !params[:code].nil? && !params[:new_password].nil?
				user_id = Users::Authentication.all.check_valid(params[:code])
				if user_id
					user = User.find(user_id)
					user.password = params[:new_password]
					user.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::CODE_EXPIRED
					status = 400
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			render :json => response, status: (status || 200)
		end

		# Url Validation
		# Reset Password
		def validate_url
			response = Hash.new
			result = Hash.new
			if !params[:code].empty? && !params[:type].empty?
				if params[:type].downcase == 'activation'
					user_id = Users::Authentication.all.check_valid(params[:code], params[:type].capitalize)
					if user_id
						# Generate OTP here
						user = User.find(user_id)
						user.email_verified = true
						user.save!
						user.authentications.create(authentication_type: 'Signup')
						user = User.find(user_id)
						result['user_id'] = user_id
						result['name'] = user.name
						result['mobile'] = user.mobile
						response.merge! ApiStatusList::OK
						status = 200
					else
						auth = Users::Authentication.all.where(code: params[:code])
						Users::Authentication.create(authentication_type: 'Activation',user_id: auth.first.user_id) if (auth.length > 0)
						response.merge! ApiStatusList::CODE_EXPIRED
						status = 400
					end
				elsif params[:type].downcase == 'tenant'
					property_id = Base64.decode64(params[:code].split('|').last)
					invites = Users::Invite.verify_property_invite(params[:type],params[:code].split('|').first,property_id)
					if invites.length > 0
						users = User.where(email: invites.first.email)
						result['user_exist'] = (users.length == 0) ? false : true
						Users::Authentication.create(authentication_type: 'Property',user_id: users.first.id) if (users.length > 0)
						response.merge! ApiStatusList::OK
						status = 200
					else
						response.merge! ApiStatusList::INVALID_INVITE
						status = 400
					end
				elsif params[:type].downcase == 'owner'
					property_id = Base64.decode64(params[:code].split('|').last)
					invites = Users::Invite.verify_property_invite(params[:type],params[:code].split('|').first,property_id)
					if invites.length > 0
						users = User.where(email: invites.first.email)
						result['user_exist'] = (users.length == 0) ? false : true
						Users::Authentication.create(authentication_type: 'Property',user_id: users.first.id) if (users.length > 0)
						response.merge! ApiStatusList::OK
						status = 200
					else
						response.merge! ApiStatusList::INVALID_INVITE
						status = 400
					end
				elsif params[:type].downcase == 'society'
					property_id = Base64.decode64(params[:code].split('|').last)
					invites = Users::Invite.verify_property_invite(params[:type],params[:code].split('|').first,property_id)
					if invites.length > 0
						users = User.where(email: invites.first.email)
						result['user_exist'] = (users.length == 0) ? false : true
						Users::Authentication.create(authentication_type: 'Society',user_id: users.first.id) if (users.length > 0)
						response.merge! ApiStatusList::OK
						status = 200
					else
						response.merge! ApiStatusList::INVALID_INVITE
						status = 400
					end
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['result'] = result
			render :json => response, status: (status || 200)
		end

		####### verify mobile #####
		def generate_otp
			response = Hash.new
			result = Hash.new
			if params[:mobile]
				user = User.where(mobile: params[:mobile])
			else
				user = User.where(email: params[:email])
			end
			if user && (user.length == 1)
				if user.first.email_verified && user.first.mobile_verified
					verify = user.first.authentications.create(authentication_type: params[:type].capitalize)
					verify.save!
					response.merge! ApiStatusList::OK
				elsif !user.first.email_verified
					result['email_verified'] = false
					user.first.authentications.create(authentication_type: 'Activation')
					response.merge! ApiStatusList::OK
				elsif !user.first.mobile_verified
					result['mobile_verified'] = false
					result['user_id'] = user.first.id
					verify = user.first.authentications.create(authentication_type: 'Signup')
					response.merge! ApiStatusList::OK
				end
				# status = 200
			else
				response.merge! ApiStatusList::USER_NOT_FOUND
				status = 400
			end
			response['result'] = result
			render :json => response, status: (status || 200)
		end

		####### verify email #####
		def generate_email
			response = Hash.new
			if params[:email]
				user = User.where(email: params[:email])
			end
			if user && (user.length == 1)
				verify = user.first.authentications.create(authentication_type: params[:type].capitalize)
				verify.save!
				response.merge! ApiStatusList::OK
				# status = 200
			else
				response.merge! ApiStatusList::USER_NOT_FOUND
				status = 400
			end
			render :json => response, status: (status || 200)
		end

		def static_data
			response = Hash.new
			result = Hash.new
			result['states'] = State.includes(:eb_providers).all.map{|state|{id: state.id, name: state.name,
								electricity_provider: state.eb_providers.map{|eb| {name: eb.name,id: eb.id}}
								}}
			result['bill_statuses'] = Bills::Status.all.select(:id,:name).as_json
			result['bill_categories'] = Bills::Category.all.select(:id,:name).as_json
			result['bank_list'] = Bank.all.select(:id,:name).as_json
			result["emi_bank_list"] = Bank.get_emi_list
			result['query_category'] = Users::Query.categories
			result['property_statuses'] = Properties::Status.all.select(:name, :id).as_json
			response[:data] = result
			render :json => response, status: (status || 200)
		end
	end
end
end