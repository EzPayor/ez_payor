module Api
module V0
	class BillApiController < ApiController
	  
		def check_permission
			return false
		end

		def create_bill
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && (User.exists? params[:user_id])
				property = Property.find(params[:property_id])
				if property #&& (property.owners.where(user_id: params[:user_id]).length > 0)
					bill = ::Bill.new(user_id: params[:user_id], property_id: params[:property_id])
					bill.category_id = Bills::Category.where(name: params[:category]).first.id
					bill.amount = params[:amount]
					bill.remarks = params[:remarks],
					# bill.actual_amount = params[:actual_amount],
					if bill.save!
						result['bill'] = bill.as_json
						result['payment_url'] = create_payment_details bill.id,params['user_id']
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::UNKNOWN_ERROR
					end
				else
					response.merge! ApiStatusList::REQUEST_DENIED
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['data'] = result
			render :json => response, status: (status || 200)
		end


		def create_payment_details id,user_id
			result = Hash.new
			bill = Bill.where(id: id, user_id: user_id)
			if bill.length == 1
				result = {
					options: Bill.generate_bill_payment_url(bill.first.id),
					url: ENV['ICICI_SERVER']
				}
			end
			return result
		end

		def bill_calculation
			response = Hash.new
			result = Hash.new
			if params[:user_id]
				if params[:amount] && params[:category] && params[:property_id] && Property.exists?(params[:property_id])
					result = Bill.bill_calculation params[:property_id],params[:amount].to_i,params[:category].upcase
					result['category'] = params[:category] if result
					result['amount'] = params[:amount] if result
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response
		end

		##### Not Required #######
		def update_bill
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && (User.exists? params[:user_id])
				bill = Bill.where(user_id: params[:user_id], id: params[:bill_id])
				if bill.length > 0
					bill = bill.first
					bill.title = params[:title]
					bill.start_date = Time.parse(params[:start_date])
					bill.end_date = Time.parse(params[:end_date])
					bill.description =params[:description]
					bill.location =params[:location].capitalize
					bill.category_id = ::Bill::Category.where(name: params[:type].upcase).first.id
					bill.duration =params[:duration]
					result['bill'] = bill.as_json
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response
		end

		def delete_bill
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && (User.exists? params[:user_id])
				bill = Bill.where(user_id: params[:user_id], id: params[:bill_id] )
				if bill.length > 0
					bill.destroy_all
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response
		end

		def user_bills
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && (User.exists? params[:user_id])
				bills = Bill.where(user_id: params[:user_id])
				# result['bill_statuses'] = Bills::Status.all.select(:id,:name).as_json
				# result['bill_categories'] = Bills::Category.all.select(:id,:name).as_json
				result['bills'] = get_bill_details(Bill.where(user_id: params[:user_id]).pluck(:id))
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response
		end

		def search_bill

		end

		def bill_details
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				bills = Bill.where(invoice_no: params[:invoice_no])
				if bills.length > 0
					bill = bills.first
					result['bill'] = get_bill_details([bill.id])
					# result['bill_statuses'] = Bills::Status.all.select(:id,:name).as_json
					# result['bill_categories'] = Bills::Category.all.select(:id,:name).as_json
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end
		
		def get_bill_details bill_ids
			bills = Bill.where(id: bill_ids)
			if bills.length > 0
				result = bills.map{|bill|{
					id: bill.id,
					date_paid: bill.date_paid ? bill.date_paid.strftime('%d/%m/%Y') : '',
					category: bill.category.name || '',
					amount: bill.amount,
					status: bill.status.name,
					user_id: bill.user.id,
					user_name: bill.user.name,
					invoice_no: bill.invoice_no,
					receipt_url: bill.invoice_no,
					property_id: bill.property_id,
					created_at: bill.created_at.strftime('%d:%m:%Y-%H:%M:%S'),
					remarks: bill.remarks,
					actual_amount: bill.actual_amount,
					final_amount: bill.final_amount,
					service_charge: bill.service_charge,
					pg_charge: bill.pg_charge,
					convenience: bill.convenience,
					transactions: bill.transactions.map{|payment|{
						bank_ref_num: payment.bank_ref_num,
						banktxnid: payment.ipgTransactionId,
						cardnum: payment.cardnum,
						ccbrand: payment.ccbrand,
						status: payment.status,
						txndatetime: payment.txndatetime,
						amount: payment.amount,
						respmsg: payment.respmsg,
						response_code: payment.response_code
						}},
					property: get_properties(bill.property_id),
				}}
			else
				result = []
			end
		end

		def payment_url
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && (User.exists? params[:user_id])
				bill = Bill.where(user_id: params[:user_id], invoice_no: params[:invoice_no] )
				if bill.length == 1
					result['bill'] = {
						options: Bill.generate_bill_payment_url(bill.first.id),
						url: ENV['ICICI_SERVER']
					}
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def rent_receipt
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				bills = Bill.where(invoice_no: params[:invoice_no])
				if bills.length > 0
					result['bill'] = bills.map{|bill|{
						id: bill.id,
						date_paid: bill.date_paid ? bill.date_paid.strftime('%d/%m/%Y') : '',
						category: bill.category.name || '',
						amount: bill.amount,
						status: bill.status.name,
						user_id: bill.user.id,
						user_name: bill.user.name,
						invoice_no: bill.invoice_no,
						receipt_url: bill.receipt_url,
						property: get_properties(bill.property_id),
						created_at: bill.created_at.strftime('%d/%m/%Y')
					}}
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end	
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def get_properties property_ids
			properties = Property.includes(:owners, :tenants,:user, :bills, :address).where(id: property_ids)
			if properties.length > 0
				result = properties.map{|property|{
					id: property.id,
					property_type: property.property_type,
					rent_date: property.rent_date,
					owners: property.owners.map{ |owner|{
						id: owner.id,
						name: owner.name,
						email: owner.email,
						mobile: owner.mobile,
						account: owner.bank_details.first.as_json
					}},
					address: property.address.as_json,
					society_available: property.society_available,
					rent: property.rent,
					maintenance: property.maintenance,
					society: { 	name: property.society ? property.society.name : nil ,
								pancard: property.society ?  property.society.pancard : '',
								account: property.society ? property.society.bank_details.first.as_json : nil
							}
				}}
			else
				result = []
			end
		end
	end
end
end