module Api
module V0
	class PropertyApiController < ApiController
	  
		def check_permission
			return false
		end

		def create_property
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id]) && params[:eb_account]
				#  handle for unique property eb account and provider
				if (Property.where(eb_provider: params[:eb_provider], eb_account: params[:eb_account]).length == 0)
					property = ::Property.new(name: params[:name], added_by: params[:user_id].to_i,
											property_type: params[:property_type].capitalize,
											eb_account: params[:eb_account],
											eb_provider: params[:eb_provider],
											society_available: params[:society_available],
											rent: params[:rent],
											maintenance: params[:maintenance],
											rent_date: params[:rent_date]
										)
					puts property.as_json
					property.maintenance_paid_to = params[:maintenance_paid_to]
					property.security_deposit = params[:security_amount]
					if property.save!
						property_address = ::Properties::Address.find_or_initialize_by(property_id: property.id)
						property_address.flat_no = params[:flat_no]
						property_address.floor 	 = params[:floor]
						property_address.building = params[:building]
						property_address.locality = params[:locality]
						property_address.city = params[:city]
						property_address.state_id = params[:state_id]
						property_address.pincode = params[:pincode]
						property_address.save!

						property_owner = ::Property.find(property.id).owners.find_or_initialize_by(email: params[:owner_email])
						if property_owner.new_record?
							property_owner.name = params[:owner_name]
							property_owner.mobile = params[:owner_mobile]
							property_owner.pancard = params[:owner_pancard]
							property_owner.invited_by = params[:user_id] if (params[:is_owner].downcase != 'true')
							property_owner.user_id = params[:user_id] if (params[:is_owner].downcase == 'true')
							property_owner.save!
						end
						if params[:tenant_email] && !params[:tenant_email].empty?
							property_tenant = ::Property.find(property.id).tenants.find_or_initialize_by(email: params[:tenant_email])
							property_tenant.name = params[:tenant_name]
							property_tenant.mobile = params[:tenant_mobile]
							property_tenant.rent_agreement_url = params[:rent_agreement_url]
							property_tenant.staying_since = Time.parse(params[:staying_since]) if (params[:staying_since] && !params[:staying_since].empty?)
							property_tenant.security_amount = params[:security_amount]
							property_tenant.security_amount_paid = params[:security_amount_paid]
							property_tenant.user_id = params[:user_id] if (params[:is_owner].downcase != 'true')
							property_tenant.invited_by = params[:user_id] if (params[:is_owner].downcase == 'true')
							property_tenant.save!
						end
						if params[:type]
							user = User.find(params[:user_id])
							if !(user.dashboard_list.include? params[:type])
								user.dashboard_list << params[:type]
								user.save!
							end
						end
						if params[:account_holder_name] && !params[:account_holder_name].empty?
							bank_detail = ::Users::BankDetail.find_or_initialize_by(account_type: "Properties::Owner", account_id: property_owner.id)
							bank_detail.account_holder_name = params[:account_holder_name]
							bank_detail.bank_name = params[:bank_name]
							bank_detail.ifsc = params[:ifsc]
							bank_detail.acc_number = params[:acc_number]
							bank_detail.acc_type = params[:acc_type]
							bank_detail.save!
						end
						if params[:society_id] && !params[:society_id].empty? && Society.exists?(params[:society_id])
							property_society = ::Properties::Society.find_or_initialize_by(property_id: property.id)
							property_society.society_id = params[:society_id]
							property_society.save!
						elsif params[:society] && !params[:society].empty?
							society_data = JSON.parse(params[:society])
							# handle for already existing society unique email
							# if (Society.where(email: society_data['email']).length == 0)
								society = Society.new(name:  society_data["name"],
												email: society_data["email"],
												contact: society_data["contact"],
												registration_no: society_data["registration_no"], 
												address: society_data['address'], 
												pancard: society_data['pancard'], 
												state_id: params[:state_id], city: params[:city],
												added_by: params[:user_id]
											)
								if society.save!
									bank_detail = ::Users::BankDetail.find_or_initialize_by(account_type: "Society", account_id: society.id)
									bank_detail.account_holder_name = society_data['bank_detail']['account_holder_name']
									bank_detail.bank_name = society_data['bank_detail']['bank_name']
									bank_detail.ifsc = society_data['bank_detail']['ifsc']
									bank_detail.acc_number = society_data['bank_detail']['acc_number']
									bank_detail.acc_type = society_data['bank_detail']['acc_type']
									bank_detail.save!
									property_society = ::Properties::Society.find_or_initialize_by(property_id: property.id)
									property_society.society_id = society.id
									property_society.save!
								end
							# else
								# society_id = Society.where(email: society_data['email']).first.id
								# property_society = ::Properties::Society.find_or_initialize_by(property_id: property.id)
								# property_society.society_id = society_id
								# property_society.save!
							# end
						end
						result['property_statuses'] = Properties::Status.all.select(:name, :id).as_json
						result['property'] = get_properties([property.id],params[:user_id])
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::UNKNOWN_ERROR
						status = 500
					end
				else
					response.merge! ApiStatusList::PROPERTY_EXIST
					status = 400
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['data'] = result
			render :json => response, status: (status || 200)
		end

		def update_property
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				properties = Users::property.where(id: params[:property_id], user_id: params[:user_id])
				if properties.length > 0
					properties.first.update_attributes(
										property_type: params[:type], 
										eb_account: params[:eb_account],
										society_available: params[:society_available],
										rent: params[:rent],
										maintenance: params[:maintenance]
									)
					result = get_properties([properties.first.id],params[:user_id])
					response.merge! ApiStatusList::OK
					status = 200
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def delete_property
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				properties = Property.where(added_by: params[:user_id], id: params[:property_id])
				if properties.length > 0
					properties.update_all(status_id: Properties::Status.where(name: 'DELETE').first.id)
					response.merge! ApiStatusList::OK
					status = 200
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def all_properties
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				case (params[:type].capitalize rescue "Tenant")
				when "Tenant"
					property_ids = Properties::Tenant.where(user_id: params[:user_id], status_id: Status.where(name: ["ACTIVE"]).pluck(:id)).pluck(:property_id)
				when "Landlord"
					property_ids = Properties::Owner.where(user_id: params[:user_id], status_id: Status.where(name: ["ACTIVE"]).pluck(:id)).pluck(:property_id)
				when "Business::tenant"
					property_ids = Properties::Tenant.where(user_id: params[:user_id], status_id: Status.where(name: ["ACTIVE","NEW"]).pluck(:id)).pluck(:property_id)
				when "Business::landlord"
					property_ids = Properties::Owner.where(user_id: params[:user_id], status_id: Status.where(name: ["ACTIVE","NEW"]).pluck(:id)).pluck(:property_id)
				when "Society"
					puts "work to do"
				else
  					property_ids = nil
				end
				result['property'] = get_properties property_ids,params[:user_id]
				result['property_statuses'] = Properties::Status.all.select(:name, :id).as_json
				status = 200
				response.merge! ApiStatusList::OK
				# result['states'] = State.all.select(:id,:name).as_json
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def get_properties property_ids,user_id
			status_ids = Properties::Status.where(name: ["ACTIVE", "NEW", "VERIFIED"]).pluck(:id)
			properties = Property.includes(:user, :tenants, society: [:bank_details], address: [:state], owners: [:bank_details])
								.where(id: property_ids, status_id: status_ids)
			if properties.length > 0
				result = properties.map{|property|{
					id: property.id,
					uid: property.uid,
					name: property.name,
					added_by: property.added_by,
					status_id: property.status_id,
					property_type: property.property_type,
					eb_account: property.eb_account,
					rent_date: property.rent_date,
					rent_enable: property.last_paid_rent,
					maintenance_enable: property.last_paid_maintenance,
					address: property.address ?  {
						locality: property.address.locality,
						building: property.address.building,
						floor: property.address.floor,
						city: property.address.city,
						state_id: property.address.state_id,
						state: property.address.state.name,
						flat_no: property.address.flat_no,
						pincode: property.address.pincode
					} : '',
					society_available: property.society_available,
					rent: property.rent || 0,
					maintenance: property.maintenance || 0,
					maintenance_paid_to: property.maintenance_paid_to,
					total_amount: property.total_amount,
					security_deposit: property.security_deposit,
					owners: property.active_owners_details.map{ |owner|{
						id: owner.id,
						user_id: owner.user_id,
						name: owner.name,
						email: owner.email,
						mobile: owner.mobile,
						pancard: owner.pancard,
						account: owner.bank_details.first.as_json
					}},
					tenants: property.active_tenants_details.map{ |tenant|{
						id: tenant.id,
						user_id: tenant.user_id,
						name: tenant.name,
						email: tenant.email,
						mobile: tenant.mobile,
						staying_since: tenant.staying_since,
						security_amount: tenant.security_amount,
						security_amount_paid: tenant.security_amount_paid
					}},
					user: {
						id: property.user.id, 
						name: property.user.name, 
						profile_img: property.user.profile.profile_img || '',
					},
					deletable: ((property.added_by == params[:user_id].to_i) ? true : false),
					society: { 	name: property.society ? property.society.name : nil ,
								pancard: property.society ?  property.society.pancard : '',
								account: property.society ? property.society.bank_details.first.as_json : nil
							},
					user_type: property.validate_property_relation(user_id,property.id)
				}}
			else
				result = []
			end
		end

		def property_details
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				result['properties'] = get_properties(params[:property_id],params[:user_id])
				status = 200
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def create_address
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				property_address = ::Properties::Address.find_or_initialize_by(property_id: params[:property_id])
				property_address.flat_no = params[:flat_no]
				property_address.floor 	 = params[:floor]
				property_address.building = params[:building]
				property_address.locality = params[:locality]
				property_address.city = params[:city]
				property_address.state_id = params[:state_id]
				property_address.pincode = params[:pincode]
				if property_address.save!
					status = 200
					result = get_properties(params[:property_id],params[:user_id])
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def add_owner
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				property_owner = ::Property.find(params[:property_id]).owners(email: params[:email])
				property_owner.name = params[:name]
				property_owner.mobile = params[:mobile]
				if property_owner.save!
					status = 200
					result = get_properties(params[:property_id],params[:user_id])
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def remove_associate
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id]) && Property.exists?(params[:property_id])
				property = Property.find(params[:property_id])
				case property.validate_property_relation params[:user_id],params[:property_id]
				when 'tenant'
					if (property.added_by.to_i == params[:user_id].to_i)
						if params[:associate_type] == 'tenant'
							tenants = Properties::Tenant.where(id: params[:associate_ids].split(',')).update_status('inactive',params[:user_id])
						elsif params[:associate_type] == 'owner'
							owners = Properties::Owner.where(id: params[:associate_ids].split(',')).update_status('inactive',params[:user_id])
						end
					else
						# handle removed by in this case
						# handle for not authorized to perform this action
					end
					status = 200
					response.merge! ApiStatusList::OK
				when 'owner'
					if params[:associate_type] == 'tenant'
						tenants = Properties::Tenant.where(id: params[:associate_ids].split(',')).update_status('inactive',params[:user_id])
					elsif params[:associate_type] == 'owner'
						owners = Properties::Owner.where(id: params[:associate_ids].split(',')).update_status('inactive',params[:user_id])
					else
						# handle for undefined type
					end
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			# response['data'] = result
			render :json => response, status: (status || 400)
		end

		def add_tenant
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				property_tenant = ::Property.find(params[:property_id]).tenants.find_or_initialize_by(email: params[:email])
				property_tenant.name = params[:name]
				property_tenant.mobile = params[:mobile]
				property_tenant.amount = params[:rent]
				property_tenant.rent_agreement_url = params[:rent_agreement_url]
				property_tenant.staying_since = Time.parse(params[:staying_since]) if !params[:staying_since].empty?
				property_tenant.invited_by = params[:user_id]
				property_tenant.security_amount = params[:security_amount]
				property_tenant.security_amount_paid = params[:security_amount_paid]
				property_tenant.status_id = ::Status.where(name: "NEW").first.id
				if property_tenant.save!
					result = get_properties(params[:property_id],params[:user_id])
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def remove_tenant
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id]) && Property.exists?(property_id)
				tenants = Properties::Tenant.where(property_id: params[:property_id], id: params[:tenant_ids])
				if properties.length > 0
					status = 200
					response.merge! ApiStatusList::OK
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def property_status
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id]) && Property.exists?(params[:property_id])
				if params[:rent_status] && (params[:rent_status].strip.downcase == 'no')
					bill = ::Bill.new(user_id: params[:user_id])
					bill.category = 'RENT + MAINTENANCE',
					bill.amount = self.property.total_amount,
					bill.property_id = self.property.id,
					if bill.save!
						result['bill'] = bill.as_json
					end
				end
				if params[:maintenance_status] && params[:maintenance_status].strip.downcase == 'no'
					bill = ::Bill.new(user_id: params[:user_id])
					bill.category = 'SECURITY DEPOSIT',
					bill.amount = self.property.security_amount,
					bill.property_id = self.property.id,
					if bill.save!
						result['bill'] = bill.as_json
					end
				end
				if params[:security_status] && params[:security_status].strip.downcase == 'no'
				end
				if create_bill
					status = 200
					response.merge! ApiStatusList::OK
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def activate_property_invite_user
			response = Hash.new
			result = Hash.new
			if params[:code] && params[:invite_type] && params[:otp]
				property_id = Base64.decode64(params[:code].split('|').last)
				invites = Users::Invite.verify_property_invite(params[:invite_type],params[:code].split('|').first,property_id)
				if invites.length > 0
					users = User.where(email: invites.first.email)
					if (users.length > 0)
						# validate otp
						user_id = users.first.authentications.check_valid(params[:otp], 'Property')
						if user_id
							case params[:invite_type].downcase
							when 'tenant'
								tenant = Properties::Tenant.where(email: invites.first.email, property_id: property_id).first
								tenant.user_id = user_id
								tenant.save!
							when 'owner'
								owner = Properties::Owner.where(email: invites.first.email, property_id: property_id).first
								owner.user_id = user_id
								owner.save!
							when 'society'
								society = Society.where(email: invites.first.email)
								society.user_id = user_id
								society.save!
							else
								puts 'no user found'
							end
							invite = invites.first
							invite.status_id = ::Users::Invites::Status.where(name: "SIGNUP").first.id
							invite.save!
							result = get_user_details(user_id)
							user = User.find(user_id)
							user.dashboard_list = [params[:invite_type]]
							user.save!
							result['authcode'] = user.authcode
							response.merge! ApiStatusList::OK
							status = 200
						else
							response.merge! ApiStatusList::INCORRECT_OTP
						end
					else
						response.merge! ApiStatusList::UNKNOWN_ERROR
					end
				else
					response.merge! ApiStatusList::CODE_EXPIRED
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response, status: (status || 400)
		end

		def create_property_invite_user
			response = Hash.new
			result = Hash.new
			if params[:code] && params[:invite_type]
				property_id = Base64.decode64(params[:code].split('|').last)
				invites = Users::Invite.verify_property_invite(params[:invite_type],params[:code].split('|').first,property_id)
				if invites.length > 0
					users = User.where(email: invites.first.email)
					if (users.length == 0)
						# create users
						user = User.new(mobile: invites.first.mobile, email: invites.first.email, name: invites.first.name)
						if user.save!
	 						profile = ::Users::Profile.new(first_name: user.name.split(' ').first, last_name: user.name.split(' ').last)
	 						profile.save!
	 						user.profile_type = "::Users::Profile"
	 						user.profile_id = profile.id
	 						user.email_verified = true
	 						user.save!
	 					end
	 					Users::Authentication.create(authentication_type: 'Property',user_id: user.id)
 					else
 						Users::Authentication.create(authentication_type: 'Property',user_id: users.first.id) if (users.length > 0)
					end
					result['otp_type'] = 'Property'
					response.merge! ApiStatusList::OK
					status = 200
				else
					response.merge! ApiStatusList::CODE_EXPIRED
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response, status: (status || 400)
		end

		def get_user_details id
			user = User.includes(:profile).where(id: id).first
			result = {
				id: user.id, name: user.name, email: user.email, mobile: user.mobile,
				profile_img: user.profile.profile_img || '',
				gender: user.profile.gender || '',
				birthday: user.profile.dob ? user.profile.dob : '',
				id_proof_url: user.profile.id_proof_url || '',
				pancard_url: user.profile.pancard_url || '',
				invite_code: user.profile.invite_code || '',
				city: user.profile.city || '',
				mobile_verified: user.mobile_verified,
				email_verified: user.email_verified,
				dashboard_list: user.dashboard_list || []
				}
		end

		def send_notification
			response = Hash.new
			result = Hash.new
			if params[:property_id] && Property.exists?(params[:property_id])
				property = Property.find(params[:property_id])
				relation = property.validate_property_relation(params[:user_id],params[:property_id])
				tenants = property.active_tenants_details
				if relation == 'owner'
					tenants.each do |tenant|
						notification = Notification.new(
							user_id: tenant.user_id,
							notification_type: 'Alert'
							)
						notification.save!
					end
				end
				response.merge! ApiStatusList::OK
				status = 200
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response, status: (status || 400)
		end

	end
end
end
