module Api
module V0
	class NotificationApiController < ApiController
	  
		def check_permission
			return false
		end

		def notifications
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				user = User.find params[:user_id]	
				if user
					result['notification'] = Notification.get_user_notifications(params[:user_id]).as_json
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response, status: (status || 400)
		end

		def read_notification
			response = Hash.new
			result = Hash.new
			if params[:user_id]	&& params[:notification_id]
				user = User.find params[:user_id]	
				notification = Notification.where(id: params[:notification_id].strip.split(','), user_id: params[:user_id])
				if user && notification
					notification.update_all(read: true)
					# notification.save!
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response, status: (status || 400)
		end

		# def dummy_notification
		# 	response = Hash.new
		# 	fcm = ::FCM.new(ENV['fcm_key'])
		# 	if params[:registration_id] && params[:message]
		# 		registration_id = [params[:registration_id]]
		# 		options = {
		# 			'notification' => {
		# 				'message' => params[:message],
		# 				'title' => (params[:title] || 'Hello there'),
		# 				'text' => (params[:text] || 'This is a test message!')#,
		# 				# 'image' => params[:image_url] || ''
		# 		  	},
		# 			'collapse_key' => 'updated_state'
		# 		}
		# 		send = fcm.send_notification(registration_id, options)
		# 		if send
		# 			response.merge! ApiStatusList::OK
		# 		else
		# 			response.merge! ApiStatusList::UNKNOWN_ERROR
		# 		end
		# 	else
		# 		response.merge! ApiStatusList::INVALID_REQUEST
		# 	end
		# 	render json: response
		# end
	end
end
end