module Api
module V0
	class QueryApiController < ApiController
	  
		def check_permission
			return false
		end

		def query
			response = Hash.new
			result = Hash.new
			if !params[:email].blank?
				query = Users::Query.create(name: params[:name],
								user_id: params[:user_id],
								email: params[:email],
								mobile: params[:mobile],
								category: params[:category],
								category_type: params[:category_type],
								query: params[:query]
								)
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			render :json => response, status: (status || 200)
		end

		def queries
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				result['queries'] = Users::Query.where(user_id: params[:user_id])
												.select([:id, :name, :category, :category_type, :query, :created_at, :status_id])
												.as_json
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				status = 400
			end
			response['data'] = result
			render :json => response, status: (status || 200)
		end
	end
end
end