module Api
module V0
	class InviteApiController < ApiController
	  
		def check_permission
			return false
		end
		
		####  INVITE OWNERS & TENANT  ####
		def invite
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && (User.exists? params[:user_id])
				if !params[:email].empty?
					user = User.find(params[:user_id])
					invite = Users::Invite.new(
						user_id: params[:user_id],
						name: params[:name],
						email: params[:email],
						mobile: params[:mobile],
						referral_code: user.profile.referral_code,
						invite_type: 'Invite'
						)
					invite.save!
					response.merge! ApiStatusList::OK
					status = 200
				else
					response.merge! ApiStatusList::INVALID_REQUEST
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		#### INVITE LIST ####
		def invite_list
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && (User.exists? params[:user_id])
				result['invites'] = Users::Invite.where(user_id: params[:user_id])
													.joins(:status)
													.select('users_invites.id, users_invites.name, users_invites.email, 
														users_invites.mobile, users_invites_statuses.id as status_id, 
														users_invites_statuses.name as status_name, users_invites.invite_type')
													.as_json

				status = 200
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end
	end
end
end