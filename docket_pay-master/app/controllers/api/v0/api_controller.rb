module Api
module V0
class ApiController < ApplicationController
  	skip_before_action :verify_authenticity_token
	private
	before_action do |controller|
		response = Hash.new
		if !['signup', 'login', 'static_data', 'validate_url', 'generate_otp', 'generate_email', 'query', 'verify_otp', 'create_property_invite_user', 'activate_property_invite_user'].include? params[:action]
			if !(User.where(authcode: request.headers.env['HTTP_AUTHCODE'], id: params[:user_id]).length == 1)
				response.merge!(ApiStatusList::REQUEST_DENIED)
				render :json => response, :callback => params[:callback], status: 401
			end
		end
		if Rails.env.production?
			if request.headers.env['HTTP_KEY'].nil?
				response.merge!(ApiStatusList::INVALID_API_KEY)
				render :json => response, :callback => params[:callback], status: 401
			else
				app = request.headers.env['HTTP_KEY']
				if (app == Figaro.env.api_key)
					true
				else
					response.merge!(ApiStatusList::INVALID_API_KEY)
					render :json => response, :callback => params[:callback], status: 401
				end
			end
		else
			if request.headers.env['HTTP_KEY'].nil?
				response.merge!(ApiStatusList::INVALID_API_KEY)
				render :json => response, :callback => params[:callback], status: 401
			else
				app = request.headers.env['HTTP_KEY']
				if (app == Figaro.env.api_key)
					true
				else
					response.merge!(ApiStatusList::INVALID_API_KEY)
					render :json => response, :callback => params[:callback], status: 401
				end
			end
		end
	end
end
end
end
