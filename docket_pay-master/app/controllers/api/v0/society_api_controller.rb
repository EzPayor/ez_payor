module Api
module V0
	class SocietyApiController < ApiController
	  
		def check_permission
			return false
		end

		def create_society
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				if !(Society.where(registration_no: params[:registration_no]).length > 0)
					society = Society.new(name:  params[:name],
									email: params[:email],
									contact: params[:contact],
									registration_no: params[:registration_no]
							)
					society.address = params[:address]
					society.locality = params[:locality]
					society.city = params[:city]
					society.state_id = params[:state_id]
					society.property_list_url = params[:property_list_url]
					society.pancard = params[:pancard]
					if society.save!
						bank_detail = ::Users::BankDetail.find_or_initialize_by(account_type: "Society", account_id: society.id)
						bank_detail.account_holder_name = params[:account_holder_name]
						bank_detail.bank_name = params[:bank_name]
						bank_detail.ifsc = params[:ifsc]
						bank_detail.acc_number = params[:acc_number]
						bank_detail.acc_type = params[:acc_type]
						bank_detail.save!
						# property_society = ::Properties::Society.new(property_id: params[:property_id], society_id: society.id)
						# property_society.save!
						if params[:type]
							user = User.find(params[:user_id])
							if !(user.dashboard_list.include? params[:type])
								user.dashboard_list << params[:type]
								user.save!
							end
						end
						status = 200
						result['society'] = society.as_json
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::UNKNOWN_ERROR
					end
				else
					response.merge! ApiStatusList::SOCIETY_EXIST
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def add_society
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				if Society.exists?(params[:society_id]) && Property.exists?(params[:property_id])
					property_society = ::Properties::Society.find_or_initialize_by(property_id: params[:property_id])
					property_society.society_id = params[:society_id]
					property_society.save!
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def search_society
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				society_ids = Society.where(state_id: params[:state_id]).predictions(params[:query]).pluck(:id)
				result['societies'] = get_society_details(society_ids)
				status = 200
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def get_society_details society_ids
			result = Society.includes(:bank_details).where(id: society_ids).map{|society|{
				id: society.id,
				name: society.name,
				email: society.email,
				pancard: society.pancard,
				address: society.address,
				contact: society.contact,
				registration_no: society.registration_no,
				city: society.city,
				state_id: society.state_id,
				locality: society.locality,
				account: society.bank_details.as_json
				}
			}
		end

		def add_bank_details
			response = Hash.new
			result = Hash.new
			if !params[:user_id].blank? && User.exists?(params[:user_id])
				if (params[:profile].downcase = 'owner') 
					type = "Properties::Owner"
				elsif (params[:profile].downcase = 'tenant')
					type = 'Properties::Tenant'
				elsif (params[:profile].downcase = 'society')
					type = 'Society'
				end
				bank_detail = ::Users::BankDetail.find_or_initialize_by(account_type: "#{type}", account_id: params[:owner_id])
				bank_detail.account_holder_name = params[:account_holder_name]
				bank_detail.bank_name = params[:bank_name]
				bank_detail.ifsc = params[:ifsc]
				bank_detail.acc_number = params[:acc_number]
				bank_detail.acc_type = params[:acc_type]
				if bank_detail.save!
					status = 200
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end
	end
end
end