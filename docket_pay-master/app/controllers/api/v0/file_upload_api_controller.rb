module Api
module V0
	class FileUploadApiController < ApiController
	  
		def check_permission
			return false
		end

		def presigned_s3_url
			response = Hash.new
			result = Hash.new
			if params[:user_id]
				bucket = ::AWS::S3::Bucket.new(ENV['S3_BUCKET'])
									.presigned_post( 
										policy: 'public_read_write',
										secure: true,
										expires: (Time.now + 15.minutes), 
										key: params[:object_key]
										)
				if bucket.url
					result = {fields: bucket.fields.map{|key,value| { "#{key.downcase}" => value } },
								url: bucket.url.host.sub('s3',"s3-#{ENV['S3_REGION']}"), port: bucket.url.port, scheme: bucket.url.scheme
								 }
					response.merge! ApiStatusList::OK
					status = 200
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['data'] = result
			render :json => response, status: (status || 400)
		end

		def get_invoice
			if params[:user_id] && User.exists?(params[:user_id])
				case params[:type] 
				when 'invoice'
					url = ::Bill.where(invoice_no: params[:object_id]).first.invoice_url
				when 'receipt'
					url = ::Bill.where(invoice_no: params[:object_id]).first.receipt_url
				when 'agreement'
					url = ::Properties::Tenant.where(id: params[:object_id]).first.rent_agreement_url
				else
					"No file found"
				end
				mime_type = url.split('.').last
				data = open(url) 
	  			send_data(data.read, filename: "#{params[:type]}.#{mime_type}", type: "application/#{mime_type}", disposition: params[:request_type], stream: 'true', buffer_size: '4096')
			else
				response.merge! ApiStatusList::INVALID_REQUEST
				render :json => response, status: (status || 400)
			end
		end

		def download_all
		end

	end
end
end