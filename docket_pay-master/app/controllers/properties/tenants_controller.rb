class Properties::TenantsController < ApplicationController
  before_action :set_properties_tenant, only: [:show, :edit, :update, :destroy, :property_detail]
  layout 'employee'

  # GET /properties/tenants
  # GET /properties/tenants.json
  def index
    @properties_tenants = Properties::Tenant.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /properties/tenants/1
  # GET /properties/tenants/1.json
  def show
  end

  # GET /properties/tenants/new
  def new
    @properties_tenant = Properties::Tenant.new
  end

  # GET /properties/tenants/1/edit
  def edit 
  end

  # POST /properties/tenants
  # POST /properties/tenants.json
  def create
    @properties_tenant = Properties::Tenant.new(properties_tenant_params)

    respond_to do |format|
      if @properties_tenant.save
        format.html { redirect_to @properties_tenant, notice: 'Tenant was successfully created.' }
        format.json { render :show, status: :created, location: @properties_tenant }
      else
        format.html { render :new }
        format.json { render json: @properties_tenant.errors, status: :unprocessable_entity }
      end
    end
  end
 
  # PATCH/PUT /properties/tenants/1
  # PATCH/PUT /properties/tenants/1.json
  def update
    respond_to do |format|
      if @properties_tenant.update(properties_tenant_params)
        format.html { redirect_to @properties_tenant, notice: 'Tenant was successfully updated.' }
        format.json { render :show, status: :ok, location: @properties_tenant }
      else
        format.html { render :edit }
        format.json { render json: @properties_tenant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/tenants/1
  # DELETE /properties/tenants/1.json
  def destroy
    @properties_tenant.destroy
    respond_to do |format|
      format.html { redirect_to properties_tenants_url, notice: 'Tenant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def property_detail
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_properties_tenant
      @properties_tenant = Properties::Tenant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def properties_tenant_params
      params.require(:properties_tenant).permit(:property_id, :amount, :user_id, :staying_since, :end_date, :invited_by, :removed_by, :rent,
          :rent_agreement_url, :name, :mobile, :email, :status_id, :security_amount, :security_amount_paid, :report_issue)
    end
end
