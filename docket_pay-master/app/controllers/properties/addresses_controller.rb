class Properties::AddressesController < ApplicationController
  before_action :set_properties_address, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /properties/addresses
  # GET /properties/addresses.json
  def index
    @properties_addresses = Properties::Address.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /properties/addresses/1
  # GET /properties/addresses/1.json
  def show
  end

  # GET /properties/addresses/new
  def new
    @properties_address = Properties::Address.new
  end

  # GET /properties/addresses/1/edit 
  def edit
  end

  # POST /properties/addresses
  # POST /properties/addresses.json
  def create
    @properties_address = Properties::Address.new(properties_address_params) 

    respond_to do |format|
      if @properties_address.save
        format.html { redirect_to @properties_address, notice: 'Address was successfully created.' }
        format.json { render :show, status: :created, location: @properties_address }
      else
        format.html { render :new }
        format.json { render json: @properties_address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /properties/addresses/1 
  # PATCH/PUT /properties/addresses/1.json
  def update
    respond_to do |format|
      if @properties_address.update(properties_address_params)
        format.html { redirect_to @properties_address, notice: 'Address was successfully updated.' }
        format.json { render :show, status: :ok, location: @properties_address }
      else
        format.html { render :edit }
        format.json { render json: @properties_address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/addresses/1
  # DELETE /properties/addresses/1.json
  def destroy
    @properties_address.destroy
    respond_to do |format|
      format.html { redirect_to properties_addresses_url, notice: 'Address was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_properties_address
      @properties_address = Properties::Address.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def properties_address_params
      params.require(:properties_address).permit(:property_id, :locality, :building, :floor, :city, :state_id, :flat_no, :pincode)
    end
end
