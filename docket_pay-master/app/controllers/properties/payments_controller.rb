class Properties::PaymentsController < ApplicationController
  before_action :set_properties_payment, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  
  def check_permission
    true
  end
  # GET /properties/payments
  # GET /properties/payments.json
  def index
    @properties_payments = Properties::Payment.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /properties/payments/1
  # GET /properties/payments/1.json
  def show
  end

  # GET /properties/payments/new
  def new
    @properties_payment = Properties::Payment.new
  end
 
  # GET /properties/payments/1/edit
  def edit
  end

  # POST /properties/payments
  # POST /properties/payments.json
  def create
    @properties_payment = Properties::Payment.new(properties_payment_params)

    respond_to do |format|
      if @properties_payment.save
        format.html { redirect_to @properties_payment, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: @properties_payment }
      else
        format.html { render :new }
        format.json { render json: @properties_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /properties/payments/1
  # PATCH/PUT /properties/payments/1.json
  def update
    respond_to do |format|
      if @properties_payment.update(properties_payment_params)
        format.html { redirect_to @properties_payment, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @properties_payment }
      else
        format.html { render :edit }
        format.json { render json: @properties_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/payments/1
  # DELETE /properties/payments/1.json
  def destroy
    @properties_payment.destroy
    respond_to do |format|
      format.html { redirect_to properties_payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def download_excel

  end 

  def excel
    all_payments = Properties::Payment.all.joins(:bank_detail, :bill)
    if params[:selected_date]
      selected_date = Date.parse(params[:selected_date])
      @payments = all_payments.where(:created_at => selected_date.beginning_of_day..selected_date.end_of_day)
    end
    respond_to do |format|
      format.html
      format.csv { send_data @payments.to_csv }
      format.xls 
    end
  end

  def update_transaction
    if params[:payment_id]
      transaction = Properties::Transaction.find_or_initialize_by(payment_id: params[:payment_id].to_i )
      transaction.amount = params[:amount]
      transaction.bill_id = params[:bill_id].to_i
      transaction.bank_ref_num = params[:bank_ref_num]
      transaction.bankcode = params[:bankcode]
      transaction.banktxnid = params[:banktxnid]
      transaction.bname = params[:bname]
      transaction.payment_type = params[:payment_type].to_s.capitalize
      if transaction.save!
        render json: ::Api::ApiStatusList::OK
      else
        render json: ::Api::ApiStatusList::UNKOWN_ERROR
      end
    else
      render json: ::Api::ApiStatusList::INVALID_REQUEST
    end
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_properties_payment
      @properties_payment = Properties::Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def properties_payment_params
      params.require(:properties_payment).permit(:bank_detail_id, :bill_id, :property_id, :payment_type, :status_id, :employee_id, :amount)
    end
end
