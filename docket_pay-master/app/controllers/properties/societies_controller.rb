class Properties::SocietiesController < ApplicationController
  before_action :set_properties_society, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /properties/societies
  # GET /properties/societies.json
  def index
    @properties_societies = Properties::Society.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /properties/societies/1
  # GET /properties/societies/1.json
  def show 
  end

  # GET /properties/societies/new
  def new
    @properties_society = Properties::Society.new
  end
 
  # GET /properties/societies/1/edit
  def edit
  end

  # POST /properties/societies
  # POST /properties/societies.json
  def create
    @properties_society = Properties::Society.new(properties_society_params)

    respond_to do |format|
      if @properties_society.save
        format.html { redirect_to @properties_society, notice: 'Society was successfully created.' }
        format.json { render :show, status: :created, location: @properties_society }
      else
        format.html { render :new }
        format.json { render json: @properties_society.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /properties/societies/1
  # PATCH/PUT /properties/societies/1.json
  def update
    respond_to do |format|
      if @properties_society.update(properties_society_params)
        format.html { redirect_to @properties_society, notice: 'Society was successfully updated.' }
        format.json { render :show, status: :ok, location: @properties_society }
      else
        format.html { render :edit }
        format.json { render json: @properties_society.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/societies/1
  # DELETE /properties/societies/1.json
  def destroy
    @properties_society.destroy
    respond_to do |format|
      format.html { redirect_to properties_societies_url, notice: 'Society was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_properties_society
      @properties_society = Properties::Society.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def properties_society_params
      params.require(:properties_society).permit(:property_id, :society_id, :amount)
    end
end
