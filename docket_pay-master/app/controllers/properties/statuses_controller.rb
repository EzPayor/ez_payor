class Properties::StatusesController < ApplicationController
  before_action :set_properties_status, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /properties/statuses
  # GET /properties/statuses.json
  def index
    @properties_statuses = Properties::Status.all
  end

  # GET /properties/statuses/1
  # GET /properties/statuses/1.json
  def show
  end

  # GET /properties/statuses/new
  def new
    @properties_status = Properties::Status.new
  end

  # GET /properties/statuses/1/edit
  def edit
  end
 
  # POST /properties/statuses 
  # POST /properties/statuses.json
  def create
    @properties_status = Properties::Status.new(properties_status_params)

    respond_to do |format|
      if @properties_status.save
        format.html { redirect_to @properties_status, notice: 'Status was successfully created.' }
        format.json { render :show, status: :created, location: @properties_status }
      else
        format.html { render :new }
        format.json { render json: @properties_status.errors, status: :unprocessable_entity }
      end
    end
  end
 
  # PATCH/PUT /properties/statuses/1
  # PATCH/PUT /properties/statuses/1.json
  def update
    respond_to do |format|
      if @properties_status.update(properties_status_params)
        format.html { redirect_to @properties_status, notice: 'Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @properties_status }
      else
        format.html { render :edit }
        format.json { render json: @properties_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/statuses/1
  # DELETE /properties/statuses/1.json
  def destroy
    @properties_status.destroy
    respond_to do |format|
      format.html { redirect_to properties_statuses_url, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_properties_status
      @properties_status = Properties::Status.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def properties_status_params
      params.require(:properties_status).permit(:name)
    end
end
