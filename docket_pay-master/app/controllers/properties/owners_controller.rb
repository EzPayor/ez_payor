class Properties::OwnersController < ApplicationController
  before_action :set_properties_owner, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /properties/owners
  # GET /properties/owners.json
  def index
    @properties_owners = Properties::Owner.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /properties/owners/1
  # GET /properties/owners/1.json
  def show
  end

  # GET /properties/owners/new
  def new
    @properties_owner = Properties::Owner.new
  end

  # GET /properties/owners/1/edit
  def edit
  end
 
  # POST /properties/owners
  # POST /properties/owners.json 
  def create
    @properties_owner = Properties::Owner.new(properties_owner_params)

    respond_to do |format|
      if @properties_owner.save
        format.html { redirect_to @properties_owner, notice: 'Owner was successfully created.' }
        format.json { render :show, status: :created, location: @properties_owner }
      else
        format.html { render :new }
        format.json { render json: @properties_owner.errors, status: :unprocessable_entity }
      end
    end 
  end

  # PATCH/PUT /properties/owners/1
  # PATCH/PUT /properties/owners/1.json
  def update
    respond_to do |format|
      if @properties_owner.update(properties_owner_params)
        format.html { redirect_to @properties_owner, notice: 'Owner was successfully updated.' }
        format.json { render :show, status: :ok, location: @properties_owner }
      else
        format.html { render :edit }
        format.json { render json: @properties_owner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/owners/1
  # DELETE /properties/owners/1.json
  def destroy
    @properties_owner.destroy
    respond_to do |format|
      format.html { redirect_to properties_owners_url, notice: 'Owner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_properties_owner
      @properties_owner = Properties::Owner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def properties_owner_params
      params.require(:properties_owner).permit(:property_id, :user_id, :amount, :name, :email, :mobile, :status_id, :pancard, :invited_by)
    end
end
