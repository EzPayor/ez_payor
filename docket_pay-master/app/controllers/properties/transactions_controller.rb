class Properties::TransactionsController < ApplicationController
  layout 'employee'
  
  before_action :set_properties_transaction, only: [:show, :edit, :update, :destroy]
  protect_from_forgery :except => [:icici_payments]
  before_action :check_permission, :except => [:icici_payments]
  # GET /properties/transactions
  # GET /properties/transactions.json
  def check_permission
    true
  end
  
  def index
    @properties_transactions = Properties::Transaction.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /properties/transactions/1
  # GET /properties/transactions/1.json
  def show
  end

  # GET /properties/transactions/new
  def new
    @properties_transaction = Properties::Transaction.new
  end

  # GET /properties/transactions/1/edit
  def edit
  end 

  # POST /properties/transactions
  # POST /properties/transactions.json
  def create
    @properties_transaction = Properties::Transaction.new(properties_transaction_params)

    respond_to do |format|
      if @properties_transaction.save
        format.html { redirect_to @properties_transaction, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: @properties_transaction }
      else
        format.html { render :new }
        format.json { render json: @properties_transaction.errors, status: :unprocessable_entity }
      end
    end 
  end

  # ICICI payment gateway 
  def icici_payments
    response = Hash.new
    result = Hash.new
    icici_response = Properties::Transaction.new(icici_params)
    icici_response.response_dump = request.params.to_s
    icici_response.save!
    bill = Bill.where(invoice_no: params[:invoice_no]).first
    if bill
      transaction = Properties::Transaction.new(txndate_processed: params[:txndate_processed],
                                  ccbin: params[:ccbin],timezone: params[:timezone],
                                  oid: params[:oid],cccountry: params[:cccountry],
                                  expmonth: params[:expmonth],secret_hash: params[:hash_algorithm],
                                  bank_ref_num: params[:endpointTransactionId],
                                  currency: params[:currency],
                                  response_code: params[:processor_response_code],
                                  amount: params[:chargetotal],terminal_id: params[:terminal_id],
                                  txnid: params[:txnid],approval_code: params[:approval_code],
                                  expyear: params[:expyear],response_hash: params[:response_hash],
                                  response_code_3dsecure: params[:response_code_3dsecure],
                                  authenticity_token: params[:authenticity_token],
                                  tdate: params[:tdate],bname: params[:bname],
                                  ccbrand: params[:ccbrand],txntype: params[:txntype],
                                  mode: params[:paymentMethod],txndatetime: params[:txndatetime],
                                  cardnum: params[:cardnumber],ipgTransactionId: params[:ipgTransactionId],
                                  status: params[:status], response_dump: request.params.to_s
                                )
      if params[:processor_response_code] == '00' && params[:status] == 'success'
        transaction.status_id = Bills::Status.where(name: 'SUCCESS').first.id
        transaction.save!
        Bill.update_status(params[:txnid],params[:status]='success')
        redirect_to thankyou_path(txnid: bill.txnid), notice: 'Payment Successfully done!'
      else
        transaction.status_id = Bills::Status.where(name: 'FAILED').first.id
        transaction.save!
        redirect_to failure_path(txnid: bill.txnid), alert: 'Payment Error. We are resolving It'
      end
    else
        redirect_to failure_path(txnid: bill.txnid), alert: 'Bill Not Found'
    end
  end

  # PATCH/PUT /properties/transactions/1
  # PATCH/PUT /properties/transactions/1.json
  def update
    respond_to do |format|
      if @properties_transaction.update(properties_transaction_params)
        format.html { redirect_to @properties_transaction, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @properties_transaction }
      else
        format.html { render :edit }
        format.json { render json: @properties_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/transactions/1
  # DELETE /properties/transactions/1.json
  def destroy
    @properties_transaction.destroy
    respond_to do |format|
      format.html { redirect_to properties_transactions_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_properties_transaction
      @properties_transaction = Properties::Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def properties_transaction_params
      params.require(:properties_transaction).permit(:amount, :approval_code, :bank_ref_num, :bankcode, :banktxnid, 
        :bname, :cardnum, :ccbin, :ccbrand, :cccountry, :currency, :discount, :email, :expmonth, :expyear, 
        :first_name, :last_name, :terminal_id, :ipgTransactionId, :mode, :name_on_card, :oid, :order_id, :bill_id, 
        :pg_type, :phone, :respmsg, :response_code, :response_code_3dsecure, :response_dump, :response_hash,
        :response_hash_validation, :responseHash_validationFrom, :secret_hash, :status, :tdate, 
        :timezone, :transactionNotificationURL, :txndate_processed, :txndatetime, :txnid, :txntype, :payment_type, :payment_id)
    end
end
