class Banks::EmisController < ApplicationController
  before_action :set_banks_emi, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  
  def check_permission
    true
  end
  # GET /banks/emis
  # GET /banks/emis.json
  def index
    @banks_emis = Banks::Emi.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /banks/emis/1 
  # GET /banks/emis/1.json
  def show
  end 

  # GET /banks/emis/new
  def new
    @banks_emi = Banks::Emi.new
  end

  # GET /banks/emis/1/edit
  def edit
  end

  # POST /banks/emis
  # POST /banks/emis.json
  def create
    @banks_emi = Banks::Emi.new(banks_emi_params)

    respond_to do |format|
      if @banks_emi.save
        format.html { redirect_to @banks_emi, notice: 'Emi was successfully created.' }
        format.json { render :show, status: :created, location: @banks_emi }
      else
        format.html { render :new }
        format.json { render json: @banks_emi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /banks/emis/1
  # PATCH/PUT /banks/emis/1.json
  def update
    respond_to do |format|
      if @banks_emi.update(banks_emi_params)
        format.html { redirect_to @banks_emi, notice: 'Emi was successfully updated.' }
        format.json { render :show, status: :ok, location: @banks_emi }
      else
        format.html { render :edit }
        format.json { render json: @banks_emi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /banks/emis/1
  # DELETE /banks/emis/1.json
  def destroy
    @banks_emi.destroy
    respond_to do |format|
      format.html { redirect_to banks_emis_url, notice: 'Emi was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_banks_emi
      @banks_emi = Banks::Emi.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def banks_emi_params
      params.require(:banks_emi).permit(:bank_id, :status_id, :tenure, :roi)
    end
end
