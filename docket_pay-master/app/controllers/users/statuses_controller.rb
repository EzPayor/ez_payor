class Users::StatusesController < ApplicationController
  before_action :set_users_status, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  # GET /users/statuses
  # GET /users/statuses.json
  def index
    @users_statuses = Users::Status.all
  end

  # GET /users/statuses/1
  # GET /users/statuses/1.json
  def show
  end

  # GET /users/statuses/new
  def new
    @users_status = Users::Status.new
  end

  # GET /users/statuses/1/edit
  def edit
  end
 
  # POST /users/statuses
  # POST /users/statuses.json
  def create
    @users_status = Users::Status.new(users_status_params)

    respond_to do |format|
      if @users_status.save
        format.html { redirect_to @users_status, notice: 'Status was successfully created.' }
        format.json { render :show, status: :created, location: @users_status }
      else
        format.html { render :new }
        format.json { render json: @users_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/statuses/1
  # PATCH/PUT /users/statuses/1.json 
  def update
    respond_to do |format|
      if @users_status.update(users_status_params)
        format.html { redirect_to @users_status, notice: 'Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_status }
      else
        format.html { render :edit }
        format.json { render json: @users_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/statuses/1
  # DELETE /users/statuses/1.json
  def destroy
    @users_status.destroy
    respond_to do |format|
      format.html { redirect_to users_statuses_url, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_status
      @users_status = Users::Status.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_status_params
      params.require(:users_status).permit(:name)
    end
end
