class Users::Invites::StatusesController < ApplicationController
  before_action :set_users_invites_status, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  
  def check_permission
    true
  end
  # GET /users/invites/statuses
  # GET /users/invites/statuses.json
  def index
    @users_invites_statuses = Users::Invites::Status.all
  end

  # GET /users/invites/statuses/1
  # GET /users/invites/statuses/1.json
  def show
  end
 
  # GET /users/invites/statuses/new
  def new
    @users_invites_status = Users::Invites::Status.new
  end
 
  # GET /users/invites/statuses/1/edit
  def edit
  end

  # POST /users/invites/statuses
  # POST /users/invites/statuses.json
  def create
    @users_invites_status = Users::Invites::Status.new(users_invites_status_params)

    respond_to do |format|
      if @users_invites_status.save
        format.html { redirect_to @users_invites_status, notice: 'Status was successfully created.' }
        format.json { render :show, status: :created, location: @users_invites_status }
      else
        format.html { render :new }
        format.json { render json: @users_invites_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/invites/statuses/1
  # PATCH/PUT /users/invites/statuses/1.json
  def update
    respond_to do |format|
      if @users_invites_status.update(users_invites_status_params)
        format.html { redirect_to @users_invites_status, notice: 'Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_invites_status }
      else
        format.html { render :edit }
        format.json { render json: @users_invites_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/invites/statuses/1
  # DELETE /users/invites/statuses/1.json
  def destroy
    @users_invites_status.destroy
    respond_to do |format|
      format.html { redirect_to users_invites_statuses_url, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_invites_status
      @users_invites_status = Users::Invites::Status.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_invites_status_params
      params.require(:users_invites_status).permit(:name)
    end
end
