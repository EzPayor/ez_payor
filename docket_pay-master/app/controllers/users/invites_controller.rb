class Users::InvitesController < ApplicationController
  before_action :set_users_invite, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /users/invites
  # GET /users/invites.json
  def index
    @users_invites = Users::Invite.all
  end

  # GET /users/invites/1
  # GET /users/invites/1.json
  def show
  end

  # GET /users/invites/new
  def new
    @users_invite = Users::Invite.new
  end

  # GET /users/invites/1/edit
  def edit
  end

  # POST /users/invites
  # POST /users/invites.json
  def create
    @users_invite = Users::Invite.new(users_invite_params)
 
    respond_to do |format|
      if @users_invite.save
        format.html { redirect_to @users_invite, notice: 'Invite was successfully created.' }
        format.json { render :show, status: :created, location: @users_invite }
      else
        format.html { render :new }
        format.json { render json: @users_invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/invites/1
  # PATCH/PUT /users/invites/1.json
  def update 
    respond_to do |format|
      if @users_invite.update(users_invite_params)
        format.html { redirect_to @users_invite, notice: 'Invite was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_invite }
      else
        format.html { render :edit }
        format.json { render json: @users_invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/invites/1
  # DELETE /users/invites/1.json
  def destroy
    @users_invite.destroy
    respond_to do |format|
      format.html { redirect_to users_invites_url, notice: 'Invite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_invite
      @users_invite = Users::Invite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_invite_params
      params.require(:users_invite).permit(:name, :email, :mobile, :referral_code, :user_id, :status_id, :invite_type, :code, :property_id)
    end
end
