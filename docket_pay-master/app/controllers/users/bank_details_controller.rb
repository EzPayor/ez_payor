class Users::BankDetailsController < ApplicationController
  before_action :set_users_bank_detail, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /users/bank_details
  # GET /users/bank_details.json
  def index
    @users_bank_details = Users::BankDetail.all.paginate(:page => params[:page], :per_page => 30)
  end 



  # GET /users/bank_details/1
  # GET /users/bank_details/1.json
  def show
  end

  # GET /users/bank_details/new

  def new
    @users_bank_detail = Users::BankDetail.new
  end 

  # GET /users/bank_details/1/edit
  def edit
  end
 
  # POST /users/bank_details 
  # POST /users/bank_details.json
  def create
    @users_bank_detail = Users::BankDetail.new(users_bank_detail_params)

    respond_to do |format|
      if @users_bank_detail.save
        format.html { redirect_to @users_bank_detail, notice: 'Bank detail was successfully created.' }
        format.json { render :show, status: :created, location: @users_bank_detail }
      else
        format.html { render :new }
        format.json { render json: @users_bank_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/bank_details/1
  # PATCH/PUT /users/bank_details/1.json
  def update
    respond_to do |format|
      if @users_bank_detail.update(users_bank_detail_params)
        format.html { redirect_to @users_bank_detail, notice: 'Bank detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_bank_detail }
      else
        format.html { render :edit }
        format.json { render json: @users_bank_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/bank_details/1
  # DELETE /users/bank_details/1.json
  def destroy
    @users_bank_detail.destroy
    respond_to do |format|
      format.html { redirect_to users_bank_details_url, notice: 'Bank detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_bank_detail
      @users_bank_detail = Users::BankDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_bank_detail_params
      params.require(:users_bank_detail).permit(:ifsc, :acc_number, :account_type, :account_id, :acc_type, :account_holder_name, :bank_name, :user_id)
    end
end
