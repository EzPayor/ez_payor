class Users::AuthenticationsController < ApplicationController
  before_action :set_users_authentication, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /users/authentications
  # GET /users/authentications.json
  def index
    @users_authentications = Users::Authentication.all
  end

  # GET /users/authentications/1
  # GET /users/authentications/1.json
  def show
  end

  # GET /users/authentications/new 
  def new
    @users_authentication = Users::Authentication.new
  end

  # GET /users/authentications/1/edit
  def edit
  end

  # POST /users/authentications 
  # POST /users/authentications.json
  def create
    @users_authentication = Users::Authentication.new(users_authentication_params)

    respond_to do |format|
      if @users_authentication.save
        format.html { redirect_to @users_authentication, notice: 'Authentication was successfully created.' }
        format.json { render :show, status: :created, location: @users_authentication }
      else
        format.html { render :new }
        format.json { render json: @users_authentication.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/authentications/1
  # PATCH/PUT /users/authentications/1.json
  def update
    respond_to do |format|
      if @users_authentication.update(users_authentication_params)
        format.html { redirect_to @users_authentication, notice: 'Authentication was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_authentication }
      else
        format.html { render :edit }
        format.json { render json: @users_authentication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/authentications/1
  # DELETE /users/authentications/1.json
  def destroy
    @users_authentication.destroy
    respond_to do |format|
      format.html { redirect_to users_authentications_url, notice: 'Authentication was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_authentication
      @users_authentication = Users::Authentication.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_authentication_params
      params.require(:users_authentication).permit(:code, :authentication_type, :user_id, :status_id, :validity, :email_status, :sms_status)
    end
end
