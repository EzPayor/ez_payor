class Users::QueriesController < ApplicationController
  before_action :set_users_query, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /users/queries
  # GET /users/queries.json
  def index
    @users_queries = Users::Query.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /users/queries/1
  # GET /users/queries/1.json
  def show
  end

  # GET /users/queries/new
  def new
    @users_query = Users::Query.new
  end

  # GET /users/queries/1/edit
  def edit 
  end

  # POST /users/queries 
  # POST /users/queries.json
  def create
    @users_query = Users::Query.new(users_query_params)

    respond_to do |format|
      if @users_query.save
        format.html { redirect_to @users_query, notice: 'Query was successfully created.' }
        format.json { render :show, status: :created, location: @users_query }
      else
        format.html { render :new }
        format.json { render json: @users_query.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/queries/1
  # PATCH/PUT /users/queries/1.json
  def update
    respond_to do |format|
      if @users_query.update(users_query_params)
        format.html { redirect_to @users_query, notice: 'Query was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_query }
      else
        format.html { render :edit }
        format.json { render json: @users_query.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/queries/1
  # DELETE /users/queries/1.json
  def destroy
    @users_query.destroy
    respond_to do |format|
      format.html { redirect_to users_queries_url, notice: 'Query was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_query
      @users_query = Users::Query.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_query_params
      params.require(:users_query).permit(:name, :user_id, :subject, :query, :notes, :status_id, :email, :mobile, :assigned_to, :category, :category_type, :property_id)
    end
end
