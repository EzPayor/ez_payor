class EbProvidersController < ApplicationController
  before_action :set_eb_provider, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  
  # GET /eb_providers
  # GET /eb_providers.json
  def index
    @eb_providers = EbProvider.all
  end

  # GET /eb_providers/1
  # GET /eb_providers/1.json
  def show
  end

  # GET /eb_providers/new
  def new
    @eb_provider = EbProvider.new 
  end

  # GET /eb_providers/1/edit
  def edit
  end

  # POST /eb_providers
  # POST /eb_providers.json 
  def create
    @eb_provider = EbProvider.new(eb_provider_params)

    respond_to do |format|
      if @eb_provider.save
        format.html { redirect_to @eb_provider, notice: 'Eb provider was successfully created.' }
        format.json { render :show, status: :created, location: @eb_provider }
      else
        format.html { render :new }
        format.json { render json: @eb_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /eb_providers/1
  # PATCH/PUT /eb_providers/1.json
  def update
    respond_to do |format|
      if @eb_provider.update(eb_provider_params)
        format.html { redirect_to @eb_provider, notice: 'Eb provider was successfully updated.' }
        format.json { render :show, status: :ok, location: @eb_provider }
      else
        format.html { render :edit }
        format.json { render json: @eb_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eb_providers/1
  # DELETE /eb_providers/1.json
  def destroy
    @eb_provider.destroy
    respond_to do |format|
      format.html { redirect_to eb_providers_url, notice: 'Eb provider was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_eb_provider
      @eb_provider = EbProvider.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def eb_provider_params
      params.require(:eb_provider).permit(:state_id, :name, :status_id)
    end
end
