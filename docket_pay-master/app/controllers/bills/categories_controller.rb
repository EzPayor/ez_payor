class Bills::CategoriesController < ApplicationController
  before_action :set_bills_category, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  
  def check_permission
    true
  end
  # GET /bills/categories
  # GET /bills/categories.json
  def index
    @bills_categories = Bills::Category.all
  end

  # GET /bills/categories/1
  # GET /bills/categories/1.json 
  def show
  end
 
  # GET /bills/categories/new
  def new
    @bills_category = Bills::Category.new
  end

  # GET /bills/categories/1/edit
  def edit
  end

  # POST /bills/categories
  # POST /bills/categories.json
  def create
    @bills_category = Bills::Category.new(bills_category_params)

    respond_to do |format|
      if @bills_category.save
        format.html { redirect_to @bills_category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @bills_category }
      else
        format.html { render :new }
        format.json { render json: @bills_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bills/categories/1
  # PATCH/PUT /bills/categories/1.json
  def update
    respond_to do |format|
      if @bills_category.update(bills_category_params)
        format.html { redirect_to @bills_category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @bills_category }
      else
        format.html { render :edit }
        format.json { render json: @bills_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bills/categories/1
  # DELETE /bills/categories/1.json
  def destroy
    @bills_category.destroy
    respond_to do |format|
      format.html { redirect_to bills_categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bills_category
      @bills_category = Bills::Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bills_category_params
      params.require(:bills_category).permit(:name)
    end
end
