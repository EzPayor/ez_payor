class Bills::StatusesController < ApplicationController
  before_action :set_bills_status, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  
  def check_permission
    true
  end

  # GET /bills/statuses
  # GET /bills/statuses.json
  def index
    @bills_statuses = Bills::Status.all
  end

  # GET /bills/statuses/1
  # GET /bills/statuses/1.json 
  def show
  end 

  # GET /bills/statuses/new
  def new
    @bills_status = Bills::Status.new
  end

  # GET /bills/statuses/1/edit
  def edit
  end

  # POST /bills/statuses
  # POST /bills/statuses.json
  def create
    @bills_status = Bills::Status.new(bills_status_params)

    respond_to do |format|
      if @bills_status.save
        format.html { redirect_to @bills_status, notice: 'Status was successfully created.' }
        format.json { render :show, status: :created, location: @bills_status }
      else
        format.html { render :new }
        format.json { render json: @bills_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bills/statuses/1
  # PATCH/PUT /bills/statuses/1.json
  def update
    respond_to do |format|
      if @bills_status.update(bills_status_params)
        format.html { redirect_to @bills_status, notice: 'Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @bills_status }
      else
        format.html { render :edit }
        format.json { render json: @bills_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bills/statuses/1
  # DELETE /bills/statuses/1.json
  def destroy
    @bills_status.destroy
    respond_to do |format|
      format.html { redirect_to bills_statuses_url, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bills_status
      @bills_status = Bills::Status.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bills_status_params
      params.require(:bills_status).permit(:name)
    end
end
