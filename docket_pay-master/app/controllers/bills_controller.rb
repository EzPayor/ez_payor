class BillsController < ApplicationController
  layout 'employee'

  protect_from_forgery :except => [:icici_payment]
  
  before_action :set_bill, only: [:show, :edit, :update, :destroy]
  before_action :check_permission, :except => [:icici_payment]
  # GET /bills
  # GET /bills.json

  def check_permission
    true
  end

  def index
    @bills = Bill.all.includes(:status).paginate(:page => params[:page], :per_page => 30)
  end

  # GET /bills/1
  # GET /bills/1.json
  def show 
  end

  # GET /bills/new
  def new
    @bill = Bill.new
  end

  # GET /bills/1/edit
  def edit 
  end

  # POST /bills
  # POST /bills.json
  def create
    @bill = Bill.new(bill_params)

    respond_to do |format|
      if @bill.save
        format.html { redirect_to @bill, notice: 'Bill was successfully created.' }
        format.json { render :show, status: :created, location: @bill }
      else
        format.html { render :new }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bills/1
  # PATCH/PUT /bills/1.json
  def update
    respond_to do |format|
      if @bill.update(bill_params)
        format.html { redirect_to @bill, notice: 'Bill was successfully updated.' }
        format.json { render :show, status: :ok, location: @bill }
      else
        format.html { render :edit }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # ICICI payment gateway 
  def icici_payment
    response = Hash.new
    result = Hash.new
    # icici_response = Properties::Transaction.new(icici_params)
    # icici_response.response_dump = request.params.to_s
    # icici_response.save!
    bill = Bill.where(invoice_no: params[:txnid]).first
    if bill
      transaction = Properties::Transaction.new(txndate_processed: params[:txndate_processed],
                                  ccbin: params[:ccbin],timezone: params[:timezone], discount: params[:discount],
                                  oid: params[:oid],cccountry: params[:cccountry],
                                  expmonth: params[:expmonth],secret_hash: params[:hash_algorithm],
                                  bank_ref_num: params[:endpointTransactionId],
                                  currency: params[:currency],respmsg: params[:fail_reason],
                                  response_code: params[:processor_response_code],
                                  amount: params[:chargetotal],terminal_id: params[:terminal_id],
                                  txnid: params[:txnid],approval_code: params[:approval_code],
                                  expyear: params[:expyear],response_hash: params[:response_hash],
                                  response_code_3dsecure: params[:response_code_3dsecure],
                                  tdate: params[:tdate],bname: params[:bname],
                                  ccbrand: params[:ccbrand],txntype: params[:txntype],
                                  mode: params[:paymentMethod],txndatetime: params[:txndatetime],
                                  cardnum: params[:cardnumber],ipgTransactionId: params[:ipgTransactionId],
                                  status: params[:status], response_dump: request.params.to_s,
                                  bill_id: bill.id, first_name: params[:name], last_name: params[:first_name],
                                  payment_id: bill.id, payment_type: "Bill"
                                )
      transaction.save!
      if params[:processor_response_code] == '00' && params[:status] == 'success'
        # payment.status_id = Bills::Status.where(name: 'PAID').first.id
        Bill.update_status(params[:txnid],params[:status]='success')
        redirect_to "#{ENV['base_url']}/#!/dashboard/payment/success/?invoice_no=#{bill.invoice_no}"#, invoice_no: bill.invoice_no, notice: 'Payment Successfully done!'
      else
        Bill.update_status(params[:txnid],params[:status]='failure')
        # payment.status_id = Bills::Status.where(name: 'FAILED').first.id
        # payment.save!
        redirect_to  "#{ENV['base_url']}/#!/dashboard/payment/fail/?invoice_no=#{bill.invoice_no}"#,invoice_no: bill.invoice_no, alert: 'Payment Error. We are resolving It'
      end
    else

      redirect_to  "#{ENV['base_url']}/#!/dashboard/payment/error/?invoice_no=#{bill.invoice_no}"#,invoice_no: bill.invoice_no, alert: 'Bill Not Found'
    end
  end

  # DELETE /bills/1
  # DELETE /bills/1.json
  def destroy
    @bill.destroy
    respond_to do |format|
      format.html { redirect_to bills_url, notice: 'Bill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bill
      @bill = Bill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bill_params
      params.require(:bill).permit(:date_paid, :category_id, :amount, :status_id, :user_id, :invoice_no, :verified_by, :property_id, :receipt_url, :remarks, :actual_amount, :final_amount, :service_charge, :pg_charge, :convenience, :owner_payment_status, :society_payment_status, :invoice_url)
    end
end
