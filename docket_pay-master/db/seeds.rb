# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
data = ['NEW', "ACTIVE", "INACTIVE", "DELETE"]
data.each do |c|
	status = ::Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

users_status_data = ['NEW', "VERIFIED", "INVALID"]
users_status_data.each do |c|
	status = Users::Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

property_data = ['NEW', "ACTIVE", "INACTIVE", "DELETE", 'VERIFIED']
property_data.each do |c|
	status = ::Properties::Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

invite_data = ['INVITED', "SIGNUP"]
invite_data.each do |c|
	status = ::Users::Invites::Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end


bill_data = ['PENDING', "PAID", 'DUE', 'VERIFIED', 'TRANSACTION DECLINED', 'FAILED']
bill_data.each do |c|
	status = ::Bills::Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

bill_category_data = ["RENT", "SPLIT RENT", "SECURITY DEPOSIT",	"LEASE DEPOSIT", "MAINTENANCE", "CORPUS FUNDS", "RENT + MAINTENANCE"]
bill_category_data.each do |c|
	status = ::Bills::Category.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

Dir.glob("#{Rails.root}/db/seeds/*.rb").each { |f| require f }