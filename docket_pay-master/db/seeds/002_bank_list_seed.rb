excel2 = Roo::Spreadsheet.open("#{Dir[Rails.root].first}"+"/db/excels/banks_list.xlsx")
if !excel2.cell(1,2).empty?
	i = 2
	j = 2
	until excel2.cell(i,2).nil? do
		if !excel2.cell(i,1).to_s.empty? && !excel2.cell(i,2).to_s.empty?
			bank = Bank.find_or_initialize_by(name: excel2.cell(i,2).to_s.strip)
			if bank.new_record?
				bank.save!
				puts "#{bank.name} created"
			else
				puts "#{bank.name} exists"
			end
			bank.save!
			i +=1
			j +=1
		end
	end
end



banks_emi = [
	{
		name: "ICICI Bank",
		'roi': {
			3 => 13.00,
			6 => 13.00,
			9 => 13.00,
			12 => 13.00,
			18 => 15.00,
			24 => 15.00
		}
	},
	{
		name: "Standard Chartered Bank",
		'roi': {
			3 => 13.00,
			6 => 13.00,
			9 => 14.00,
			12 => 14.00,
			18 => 15.00,
			24 => 15.00
		}
	},
	{
		name: "Axis Bank",
		'roi': {
			3 => 12.00,
			6 => 12.00,
			9 => 13.00,
			12 => 13.00,
			18 => 15.00,
			24 => 15.00
		}
	},
	{
		name: "Kotak Bank",
		'roi': {
			3 => 12.00,
			6 => 12.00,
			9 => 14.00,
			12 => 14.00,
			18 => 15.00,
			24 => 15.00
		}
	},
	{
		name: "HSBC Bank",
		'roi': {
			3 => 12.50,
			6 => 12.50,
			9 => 13.50,
			12 => 13.50,
			18 => 13.50,
			24 => 15.00
		}
	},
	{
		name: "IndusInd Bank",
		'roi': {
			3 => 13.00,
			6 => 13.00,
			9 => 13.00,
			12 => 13.00,
			18 => 15.00,
			24 => 15.00
		}
	}]
banks_emi.each do |bank|
	bank_object = Bank.where(name: bank[:name]).first
	if bank_object
		bank[:roi].each do |key,value|
			emi = Banks::Emi.find_or_initialize_by(bank_id: bank_object.id, tenure: key)
			if emi
				emi.roi = value if emi.roi.nil?
				emi.status_id = Status.where(name: "ACTIVE").first.id if emi.status_id.nil?
				emi.save!
			end
		end
	end
end
