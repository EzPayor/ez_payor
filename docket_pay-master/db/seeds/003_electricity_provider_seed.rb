excel2 = Roo::Spreadsheet.open("#{Dir[Rails.root].first}"+"/db/excels/electricity_boards_list.xlsx")
if !excel2.cell(1,2).empty?
	i = 2
	j = 2
	until excel2.cell(i,2).nil? do
		if !excel2.cell(i,1).to_s.empty? && !excel2.cell(i,2).to_s.empty?
			eb_provider = EbProvider.find_or_initialize_by(name: excel2.cell(i,3).to_s.capitalize.strip, 
								state_id: State.where(name: excel2.cell(i,2).to_s.capitalize.strip).first.id)
			if eb_provider.new_record?
				eb_provider.save!
				puts "#{eb_provider.name} created"
			else
				puts "#{eb_provider.name} exists"
			end
			eb_provider.save!
			i +=1
			j +=1
		end
	end
end