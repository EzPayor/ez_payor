class AddSecurityAmountPropertiesTenants < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties_tenants, :security_amount, :integer
  	add_column :properties_tenants, :security_amount_paid, :boolean
  end
end
