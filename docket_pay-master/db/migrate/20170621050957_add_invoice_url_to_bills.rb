class AddInvoiceUrlToBills < ActiveRecord::Migration[5.0]
  def change
  	add_column :bills, :invoice_url, :string
  end
end
