class CreatePropertiesSocieties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties_societies do |t|
      t.integer :property_id
      t.integer :society_id
      t.integer :amount

      t.timestamps
    end
  end
end
