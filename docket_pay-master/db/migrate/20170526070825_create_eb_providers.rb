class CreateEbProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :eb_providers do |t|
      t.integer :state_id
      t.string :name
      t.integer :status_id

      t.timestamps
    end
  end
end
