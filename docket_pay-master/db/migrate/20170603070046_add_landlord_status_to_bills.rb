class AddLandlordStatusToBills < ActiveRecord::Migration[5.0]
  def change
  	add_column :bills, :owner_payment_status, :boolean, default: false
  	add_column :bills, :society_payment_status, :boolean, default: false
  end
end
