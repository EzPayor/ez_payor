class AddRentPaymentDateToProperties < ActiveRecord::Migration[5.0]
  def change
    add_column :properties, :rent_date, :integer
  end
end
