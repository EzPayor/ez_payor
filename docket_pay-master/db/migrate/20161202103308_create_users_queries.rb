class CreateUsersQueries < ActiveRecord::Migration[5.0]
  def change
    create_table :users_queries do |t|
      t.string  :name
      t.integer :user_id
      t.string  :subject
      t.text    :query
      t.text    :notes
      t.integer :status_id
      t.integer :assigned_to

      t.timestamps
    end
  end
end
