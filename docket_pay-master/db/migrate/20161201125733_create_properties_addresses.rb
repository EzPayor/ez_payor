class CreatePropertiesAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :properties_addresses do |t|
      t.integer :property_id
      t.string  :locality
      t.string  :building
      t.string  :floor
      t.string  :city
      t.integer :state_id
      t.string  :flat_no
      t.string  :pincode

      t.timestamps
    end
  end
end
