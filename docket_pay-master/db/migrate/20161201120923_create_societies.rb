class CreateSocieties < ActiveRecord::Migration[5.0]
  def change
    create_table :societies do |t|
      t.string :name
      t.string :email
      t.string :contact
      t.string :registration_no

      t.timestamps
    end
  end
end
