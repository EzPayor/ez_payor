class AddMissingIndexes < ActiveRecord::Migration[5.0]
	def change
  		add_index :properties, :added_by
        add_index :properties, :status_id
        add_index :properties_transactions, :bill_id
        add_index :properties_transactions, [:payment_id, :payment_type]
        add_index :properties_tenants, :property_id
        add_index :properties_tenants, :user_id
        add_index :properties_tenants, :status_id
        add_index :users_authentications, :user_id
        add_index :users_authentications, :status_id
        add_index :bills, :user_id
        add_index :bills, :status_id
        add_index :bills, :category_id
        add_index :bills, :property_id
        add_index :banks_emis, :status_id
        add_index :banks_emis, :bank_id
        add_index :banks, :status_id
        add_index :properties_payments, :status_id
        add_index :properties_payments, :property_id
        add_index :properties_payments, :bill_id
        add_index :properties_payments, :bank_detail_id
        add_index :properties_societies, :property_id
        add_index :properties_societies, :society_id
        add_index :properties_owners, :property_id
        add_index :properties_owners, :user_id
        add_index :properties_owners, :status_id
        add_index :properties_addresses, :property_id
        add_index :properties_addresses, :state_id
        add_index :notifications, :user_id
        add_index :employees, :parent_id
        add_index :eb_providers, :status_id
        add_index :eb_providers, :state_id
        add_index :users_queries, :user_id
        add_index :users_queries, :status_id
        add_index :users_queries, :property_id
        add_index :users_queries, :assigned_to
        add_index :users_invites, :user_id
        add_index :users_invites, :status_id
        add_index :users_bank_details, [:account_id, :account_type]
        add_index :users, [:profile_id, :profile_type]
        add_index :users, :status_id
        add_index :societies, :added_by
        add_index :roles, :parent_id
	end
end
