class AddPropertyId < ActiveRecord::Migration[5.0]
  def change
  	add_column :users_invites, :property_id, :integer
  	add_column :properties_owners, :invited_by, :integer
  end
end
