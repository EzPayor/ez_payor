class CreateUsersAuthentications < ActiveRecord::Migration[5.0]
  def change
    create_table :users_authentications do |t|
      t.string :code
      t.string :authentication_type
      t.integer :user_id
      t.integer :status_id
      t.datetime :validity

      t.timestamps
    end
  end
end
