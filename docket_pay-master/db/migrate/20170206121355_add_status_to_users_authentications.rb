class AddStatusToUsersAuthentications < ActiveRecord::Migration[5.0]
  def change
    add_column :users_authentications, :email_status, :string
    add_column :users_authentications, :sms_status, :string
  end
end
