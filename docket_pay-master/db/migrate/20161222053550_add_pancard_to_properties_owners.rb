class AddPancardToPropertiesOwners < ActiveRecord::Migration[5.0]
  def change
    add_column :properties_owners, :pancard, :string
  end
end
