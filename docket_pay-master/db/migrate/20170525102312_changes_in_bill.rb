class ChangesInBill < ActiveRecord::Migration[5.0]
  def change
  	add_column :bills, :remarks, :string
  	add_column :bills, :actual_amount, :integer
  	add_column :bills, :final_amount, :float, precision: 5, scale: 2
  	add_column :bills, :service_charge, :float
  	add_column :bills, :pg_charge, :float
  	add_column :bills, :convenience, :float
  end
end
