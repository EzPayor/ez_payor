class AddMaintenancePaidToProperty < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :maintenance_paid_to, :string
  	add_column :societies, :address, :string
  	add_column :societies, :city, :string
  	add_column :societies, :state_id, :integer
  	add_column :societies, :localty, :string
  	add_column :societies, :property_list_url, :string
  	add_column :societies, :pancard, :string
  end
end
