class CreatePropertiesPayments < ActiveRecord::Migration[5.0]
  def change
    create_table :properties_payments do |t|
      t.integer :bank_detail_id
      t.integer :bill_id
      t.integer :property_id
      t.string  :payment_type
      t.integer :status_id
      t.integer :employee_id
      t.integer :amount

      t.timestamps
    end
  end
end
