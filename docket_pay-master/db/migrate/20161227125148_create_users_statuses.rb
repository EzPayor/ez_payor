class CreateUsersStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :users_statuses do |t|
      t.string :name

      t.timestamps
    end
  end
end
