class AddEbProviderToProperties < ActiveRecord::Migration[5.0]
  def change
    add_column :properties, :eb_provider, :string
  end
end
