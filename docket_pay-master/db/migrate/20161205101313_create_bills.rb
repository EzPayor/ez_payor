class CreateBills < ActiveRecord::Migration[5.0]
  def change
    create_table :bills do |t|
      t.datetime  :date_paid
      t.integer   :category_id
      t.float     :amount, precision: 5, scale: 2
      t.integer   :status_id
      t.integer   :user_id
      t.string    :invoice_no
      t.integer   :verified_by

      t.timestamps
    end
  end
end
