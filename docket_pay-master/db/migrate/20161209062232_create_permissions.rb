class CreatePermissions < ActiveRecord::Migration[5.0]
	def up
	    create_table :permissions do |t|
	      t.integer :role_id
	      t.integer :resource_id
	    end
	    change_table :permissions do |t|
	      t.index [:role_id, :resource_id], :unique =>false
	    end
	    
	    execute <<-SQL
	      ALTER TABLE permissions
	        ADD CONSTRAINT fk_roles
	        FOREIGN KEY (role_id)
	        REFERENCES roles(id)
	    SQL
	    
	    execute <<-SQL
	      ALTER TABLE permissions
	        ADD CONSTRAINT fk_resources
	        FOREIGN KEY (resource_id)
	        REFERENCES resources(id)
	    SQL
	end

	def down
	  	execute <<-SQL
	      ALTER TABLE permissions
	        DROP CONSTRAINT fk_roles
	    SQL
	    
	    execute <<-SQL
	      ALTER TABLE permissions
	        DROP CONSTRAINT fk_resources
	    SQL
	    
	    drop_table :permissions
	end
end
