class AddReportMessageToProperties < ActiveRecord::Migration[5.0]
  def change
    add_column :properties, :report_message, :text
  end
end
