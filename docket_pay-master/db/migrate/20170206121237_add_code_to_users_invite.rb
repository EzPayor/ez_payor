class AddCodeToUsersInvite < ActiveRecord::Migration[5.0]
  def change
    add_column :users_invites, :code, :string
  end
end
