class CreatePropertiesOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :properties_owners do |t|
      t.integer :property_id
      t.integer :user_id
      t.integer :amount
      t.string	:name
      t.string	:email
      t.string	:mobile
      t.integer :status_id

      t.timestamps
    end
  end
end
