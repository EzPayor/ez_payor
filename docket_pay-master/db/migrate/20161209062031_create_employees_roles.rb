class CreateEmployeesRoles < ActiveRecord::Migration[5.0]
	def up
		create_table :employees_roles do |t|
			t.integer :role_id
			t.integer :employee_id

			t.timestamps
		end
  
		change_table :employees_roles do |t|
		  t.index [:role_id, :employee_id], :unique =>false
		end

		execute <<-SQL
		  ALTER TABLE employees_roles
			ADD CONSTRAINT fk_roles
			FOREIGN KEY (role_id)
			REFERENCES roles(id)
		SQL
		
		execute <<-SQL
		  ALTER TABLE employees_roles
			ADD CONSTRAINT fk_employees
			FOREIGN KEY (employee_id)
			REFERENCES employees(id)
		SQL
	end

	def down
		execute <<-SQL
		  ALTER TABLE employees_roles
			DROP CONSTRAINT fk_roles
		SQL
		
		execute <<-SQL
		  ALTER TABLE employees_roles
			DROP CONSTRAINT fk_employees
		SQL
		
		drop_table :employees_roles
	end
end
