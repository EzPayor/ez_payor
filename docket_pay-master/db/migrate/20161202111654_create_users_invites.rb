class CreateUsersInvites < ActiveRecord::Migration[5.0]
  def change
    create_table :users_invites do |t|
      t.string  :name
      t.string  :email
      t.string  :mobile
      t.string  :referral_code
      t.integer :user_id
      t.integer :status_id
      t.string  :invite_type

      t.timestamps
    end
  end
end
