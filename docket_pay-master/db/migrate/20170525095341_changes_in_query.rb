class ChangesInQuery < ActiveRecord::Migration[5.0]
  def change
  	remove_column :users_queries, :subject, :string
  	add_column :users_queries, :category, :string
  	add_column :users_queries, :category_type, :string
  end
end
