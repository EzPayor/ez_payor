class ChangesToPropertyAndTenantTables < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties_tenants, :report_issue, :text
  	remove_column :properties, :report_message, :text
  	add_column :properties, :report_issue, :text
  end
end
