class AddEmailToUsersQueries < ActiveRecord::Migration[5.0]
  def change
  	add_column :users_queries, :email, :string
  	add_column :users_queries, :mobile, :string
  end
end
