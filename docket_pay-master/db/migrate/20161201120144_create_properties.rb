class CreateProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties do |t|
      t.string  :name
      t.integer :added_by
      t.integer :status_id
      t.string  :property_type
      t.string  :eb_account
      t.integer :owner_id
      t.boolean :society_available, default: false
      t.integer :rent, default: 0
      t.integer :maintenance, default: 0
      t.integer :total_amount

      t.timestamps
    end
  end
end
