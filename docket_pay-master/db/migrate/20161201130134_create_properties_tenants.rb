class CreatePropertiesTenants < ActiveRecord::Migration[5.0]
  def change
    create_table :properties_tenants do |t|
      t.integer :property_id
      t.integer :amount
      t.integer :user_id
      t.date    :staying_since
      t.date    :end_date
      t.integer :invited_by
      t.integer :removed_by
      t.string  :rent_agreement_url
      t.string  :name
      t.string  :mobile
      t.string  :email
      t.integer :status_id

      t.timestamps
    end
  end
end
