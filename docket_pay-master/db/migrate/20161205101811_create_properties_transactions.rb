class CreatePropertiesTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :properties_transactions do |t|
      t.string :amount
      t.string :approval_code
      t.string :bank_ref_num
      t.string :bankcode
      t.string :banktxnid
      t.string :bname
      t.string :cardnum
      t.string :ccbin
      t.string :ccbrand
      t.string :cccountry
      t.string :currency
      t.string :discount
      t.string :email
      t.string :expmonth
      t.string :expyear
      t.string :first_name
      t.string :last_name
      t.string :terminal_id
      t.string :ipgTransactionId
      t.string :mode
      t.string :name_on_card
      t.string :oid
      t.string :bill_id
      t.string :pg_type
      t.string :phone
      t.string :respmsg
      t.string :response_code
      t.string :response_code_3dsecure
      t.string :response_dump
      t.string :response_hash
      t.string :secret_hash
      t.string :status
      t.string :tdate
      t.string :timezone
      t.string :transactionNotificationURL
      t.string :txndate_processed
      t.string :txndatetime
      t.string :txnid
      t.string :txntype
      t.string :payment_type
      t.integer :payment_id

      t.timestamps
    end
  end
end
