class CreateBanksEmis < ActiveRecord::Migration[5.0]
  def change
    create_table :banks_emis do |t|
      t.integer :bank_id
      t.integer :status_id
      t.integer :tenure
      t.float :roi

      t.timestamps
    end
  end
end
