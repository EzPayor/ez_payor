class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string  :name
      t.string  :email
      t.string  :mobile
      t.integer :status_id
      t.string  :profile_type
      t.integer :profile_id
      t.boolean :mobile_verified, default: false
      t.boolean :email_verified, default: false
      t.boolean :verified, default: false
      t.string  :password
      t.string  :authcode

      t.timestamps
    end
  end
end
