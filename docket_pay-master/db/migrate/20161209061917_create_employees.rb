class CreateEmployees < ActiveRecord::Migration[5.0]
  def up
    create_table :employees do |t|
      t.string :name
      t.string :email
      t.string :mobile
      t.string :password
      t.integer :parent_id
      t.string :city

      t.timestamps
    end
    change_table :employees do |t|
       t.index [:email, :mobile], :unique => true
    end
    
    execute <<-SQL
      ALTER TABLE employees
        ADD CONSTRAINT fk_employees_parent
        FOREIGN KEY (parent_id)
        REFERENCES employees(id)
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE employees
        DROP CONSTRAINT fk_employees_parent
    SQL
    drop_table :employees
  end
end
