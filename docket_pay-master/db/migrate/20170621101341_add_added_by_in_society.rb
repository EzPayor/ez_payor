class AddAddedByInSociety < ActiveRecord::Migration[5.0]
  def change
  	add_column :societies, :added_by, :integer
  end
end
