class AddSecurityToProperties < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :security_deposit, :integer
  end
end
