class CreateUsersBankDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :users_bank_details do |t|
      t.string  :ifsc
      t.string  :acc_number
      t.string  :account_type
      t.integer :account_id
      t.string  :acc_type
      t.string  :account_holder_name
      t.string  :bank_name
      t.integer :user_id

      t.timestamps
    end
  end
end
