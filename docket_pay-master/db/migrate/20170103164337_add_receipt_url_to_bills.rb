class AddReceiptUrlToBills < ActiveRecord::Migration[5.0]
  def change
    add_column :bills, :receipt_url, :string
  end
end
