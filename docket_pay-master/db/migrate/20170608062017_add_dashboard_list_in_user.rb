class AddDashboardListInUser < ActiveRecord::Migration[5.0]
  def change
  	remove_column :societies, 	:localty, :string
  	add_column :societies, 		:locality, :string
  	add_column :users_queries, 	:property_id, :integer
  	add_column :users, 			:dashboard_list, :string, array: true, default: []
  end
end
