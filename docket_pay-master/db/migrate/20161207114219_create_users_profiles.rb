class CreateUsersProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :users_profiles do |t|
      t.string    :first_name
      t.string    :last_name
      t.string    :id_proof_url
      t.string    :pancard_url
      t.string    :invite_code
      t.string    :referred_by
      t.string    :referral_code
      t.string    :gender
      t.date      :dob
      t.string    :profile_img
      t.string    :city
      t.integer   :state_id
      t.datetime  :last_login
      t.string    :last_login_ip
      t.date      :current_login
      t.string    :current_login_ip
      t.string    :last_pwd
      t.datetime  :last_pwd_update
      t.string    :last_pwd_update_ip

      t.timestamps
    end
  end
end
