module.exports = function(grunt) {
    var path = require('path');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    console.log(path.resolve(__dirname));
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        dirs: {
            input: 'src',
            output: 'build',
            base: path.resolve(__dirname)
        },
        jshint:{
            files:['Gruntfile.js'],
            options:{
                globals:{
                    jQuery:true
                }
            }
        },
        watch: {
          files: ['<%= jshint.files %>'],
          tasks: ['jshint']
        },
        cssmin: {
          build: {
            files: [{
              expand: true,
              src: ['<%= dirs.input %>/**/*.css', '!*.min.css'],
              dest: '<%= dirs.output %>',
              ext: '.css'
            }]
          }
        },
        htmlmin: {                                     // Task
          build: {                                      // Target
            options: {                                 // Target options
              removeComments: true,
              collapseWhitespace: true
            },
            files: [{
              expand: true,
              src: ['<%= dirs.input %>/**/*.html'],
              dest: '<%= dirs.output %>'
            }]
          }
        },
        uglify: {
          build: {
            files: [{
                expand: true,
                src: '<%= dirs.input %>/app/*.js',
                dest: '<%= dirs.output %>/js/app/',
            },
            {
                expand: false,
                src: '<%= dirs.input %>/app/components/**/*.js',
                dest: '<%= dirs.output %>/js/docketscript.js',
            }],
            options:{
                mangle:false
            }
          }
        },
        copy: {
          main: {
            expand: true,
            cwd: '<%= dirs.output %>/src',
            src: '**',
            dest: '<%= dirs.base %>/',
          },
          libs:{
            expand: true,
            cwd: '<%= dirs.input %>/libs',
            src: '**',
            dest: '<%= dirs.base %>/libs/',
          },
          jsapp:{
            expand:true,
            cwd:'<%= dirs.output %>/js/app/src/app',
            src:'**',
            dest:'<%= dirs.base %>/js/app/'
          },
          jscomponents:{
            expand:true,
            cwd:'<%= dirs.output %>/js',
            src:'*.js',
            dest:'<%= dirs.base %>/js/'
          },
          assets:{
            expand:true,
            cwd:'<%= dirs.input %>/assets',
            src:'**',
            dest:'<%= dirs.base %>/assets/'
          }
        },
    });
 
    grunt.registerTask('default', ['jshint','cssmin','htmlmin','uglify','copy'] );
};